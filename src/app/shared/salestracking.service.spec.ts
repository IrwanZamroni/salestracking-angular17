import { TestBed } from '@angular/core/testing';

import { SalestrackingService } from './salestracking.service';

describe('SalestrackingService', () => {
  let service: SalestrackingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SalestrackingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
