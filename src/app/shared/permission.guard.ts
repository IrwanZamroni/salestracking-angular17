// permission.guard.ts
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { PermissionService } from './permission.service';

interface RouteDataWithPermission {
  permission: string;
}

@Injectable({
  providedIn: 'root'
})
export class PermissionGuard implements CanActivate {
  constructor(private permissionService: PermissionService, private router: Router) {
    this.canActivate;
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    // Dapatkan izin dari properti data rute
    const testPermission: string | undefined = (route.data as RouteDataWithPermission).permission;

    // Periksa apakah testPermission tidak undefined sebelum menggunakannya
    const hasPermission = testPermission !== undefined ? this.permissionService.checkPermission(testPermission) : false;

    if (hasPermission) {
      
      return true;
    } else {
      // Redirect ke halaman lain jika tidak memiliki izin
      this.router.navigate(['**']);
      return false;
    }
  }
}
