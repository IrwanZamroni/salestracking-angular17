
import { HttpClient ,HttpHeaders,HttpParams} from '@angular/common/http';
import { Injectable, inject, signal } from '@angular/core';
import { firstValueFrom } from 'rxjs';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import {environment} from '../../environments/environment.development'
import * as moment from 'moment';
import Swal from 'sweetalert2';

import { test,InqueryLoginDataInfo,PermissionLoginDataInfo,LoginResponseInformation,CurentLoginDataInfo,LoginData, LoginResponse, RegistrationResponse, UserProfile, UserRegistrationData } from '../../app/components/authentication/users.interfaces';
import { Router } from '@angular/router';

interface userNamedata {
  // Tentukan properti yang akan dikembalikan oleh promise resendKP di sini
  // Misalnya:
  name: any;
  userName: any;
}
export interface IDocumentKPRInputDto {
    psCode: string;
    bookingHeaderID: number;
    documentType: string;
    documentFile: string;
    documentPicType: string;
    priorityPassId: number;
}
export class ResendEmailByBookCodeInputDto implements IResendEmailByBookCodeInputDto {
  bookCode: string;
  emailCustomer: string;
  customerName: string;
  memberCode: string;
  docCode: string;
  isSigned: boolean;

  constructor(data?: IResendEmailByBookCodeInputDto) {
      if (data) {
          for (var property in data) {
              if (data.hasOwnProperty(property))
                  (<any>this)[property] = (<any>data)[property];
          }
      }
  }

  init(data?: any) {
      if (data) {
          this.bookCode = data["bookCode"];
          this.emailCustomer = data["emailCustomer"];
          this.customerName = data["customerName"];
          this.memberCode = data["memberCode"];
          this.docCode = data["docCode"];
          this.isSigned = data["isSigned"];
      }
  }

  static fromJS(data: any): ResendEmailByBookCodeInputDto {
      let result = new ResendEmailByBookCodeInputDto();
      result.init(data);
      return result;
  }

  toJSON(data?: any) {
      data = typeof data === 'object' ? data : {};
      data["bookCode"] = this.bookCode;
      data["emailCustomer"] = this.emailCustomer;
      data["customerName"] = this.customerName;
      data["memberCode"] = this.memberCode;
      data["docCode"] = this.docCode;
      data["isSigned"] = this.isSigned;
      return data; 
  }
}

export interface IResendEmailByBookCodeInputDto {
  bookCode: string;
  emailCustomer: string;
  customerName: string;
  memberCode: string;
  docCode: string;
  isSigned: boolean;
}
export class ResendEmailLayoutPlanDto implements IResendEmailLayoutPlanDto {
  projectID: number;
  emailCustomer: string;
  customerName: string;
  memberCode: string;

  constructor(data?: IResendEmailLayoutPlanDto) {
      if (data) {
          for (var property in data) {
              if (data.hasOwnProperty(property))
                  (<any>this)[property] = (<any>data)[property];
          }
      }
  }

  init(data?: any) {
      if (data) {
          this.projectID = data["projectID"];
          this.emailCustomer = data["emailCustomer"];
          this.customerName = data["customerName"];
          this.memberCode = data["memberCode"];
      }
  }

  static fromJS(data: any): ResendEmailLayoutPlanDto {
      let result = new ResendEmailLayoutPlanDto();
      result.init(data);
      return result;
  }

  toJSON(data?: any) {
      data = typeof data === 'object' ? data : {};
      data["projectID"] = this.projectID;
      data["emailCustomer"] = this.emailCustomer;
      data["customerName"] = this.customerName;
      data["memberCode"] = this.memberCode;
      return data; 
  }
}

export interface IResendEmailLayoutPlanDto {
  projectID: number;
  emailCustomer: string;
  customerName: string;
  memberCode: string;
}
export class DocumentKPRFileResultDto implements IDocumentKPRFileResultDto {
  documentFile: string;
  documentType: string;

  constructor(data?: IDocumentKPRFileResultDto) {
      if (data) {
          for (var property in data) {
              if (data.hasOwnProperty(property))
                  (<any>this)[property] = (<any>data)[property];
          }
      }
  }

  init(data?: any) {
      if (data) {
          this.documentFile = data["documentFile"];
          this.documentType = data["documentType"];
      }
  }

  static fromJS(data: any): DocumentKPRFileResultDto {
      let result = new DocumentKPRFileResultDto();
      result.init(data);
      return result;
  }

  toJSON(data?: any) {
      data = typeof data === 'object' ? data : {};
      data["documentFile"] = this.documentFile;
      data["documentType"] = this.documentType;
      return data; 
  }
}

export interface IDocumentKPRFileResultDto {
  documentFile: string;
  documentType: string;
}
export class DocumentKPRListDto implements IDocumentKPRListDto {
  priorityPassId: number;
  kprtype: number;
  documentType: string;
  documentName: string;
  isMandatory: boolean;
  psCode: string;
  isUploaded: boolean;
  isVerified: boolean;
  creationTime: moment.Moment;
  lastModificationTime: moment.Moment;

  constructor(data?: IDocumentKPRListDto) {
      if (data) {
          for (var property in data) {
              if (data.hasOwnProperty(property))
                  (<any>this)[property] = (<any>data)[property];
          }
      }
  }

  init(data?: any) {
      if (data) {
          this.priorityPassId = data["priorityPassId"];
          this.kprtype = data["kprtype"];
          this.documentType = data["documentType"];
          this.documentName = data["documentName"];
          this.isMandatory = data["isMandatory"];
          this.psCode = data["psCode"];
          this.isUploaded = data["isUploaded"];
          this.isVerified = data["isVerified"];
          this.creationTime = data["creationTime"] ? moment(data["creationTime"].toString()) : <any>undefined;
          this.lastModificationTime = data["lastModificationTime"] ? moment(data["lastModificationTime"].toString()) : <any>undefined;
      }
  }

  static fromJS(data: any): DocumentKPRListDto {
      let result = new DocumentKPRListDto();
      result.init(data);
      return result;
  }

  toJSON(data?: any) {
      data = typeof data === 'object' ? data : {};
      data["priorityPassId"] = this.priorityPassId;
      data["kprtype"] = this.kprtype;
      data["documentType"] = this.documentType;
      data["documentName"] = this.documentName;
      data["isMandatory"] = this.isMandatory;
      data["psCode"] = this.psCode;
      data["isUploaded"] = this.isUploaded;
      data["isVerified"] = this.isVerified;
      data["creationTime"] = this.creationTime ? this.creationTime.toISOString() : <any>undefined;
      data["lastModificationTime"] = this.lastModificationTime ? this.lastModificationTime.toISOString() : <any>undefined;
      return data; 
  }
}

export interface IDocumentKPRListDto {
  priorityPassId: number;
  kprtype: number;
  documentType: string;
  documentName: string;
  isMandatory: boolean;
  psCode: string;
  isUploaded: boolean;
  isVerified: boolean;
  creationTime: moment.Moment;
  lastModificationTime: moment.Moment;
}

export class DocumentKPRInputDto implements IDocumentKPRInputDto {
  psCode: string;
  bookingHeaderID: number;
  documentType: string;
  documentFile: string;
  documentPicType: string;
  priorityPassId: number;
  userId: any;

  constructor(data?: IDocumentKPRInputDto) {
      if (data) {
          for (var property in data) {
              if (data.hasOwnProperty(property))
                  (<any>this)[property] = (<any>data)[property];
          }
      }
  }

  init(data?: any) {
      if (data) {
          this.psCode = data["psCode"];
          this.bookingHeaderID = data["bookingHeaderID"];
          this.documentType = data["documentType"];
          this.documentFile = data["documentFile"];
          this.documentPicType = data["documentPicType"];
          this.priorityPassId = data["priorityPassId"];
          this.userId = data["userId"];
      }
  }

  static fromJS(data: any): DocumentKPRInputDto {
      let result = new DocumentKPRInputDto();
      result.init(data);
      return result;
  }

  toJSON(data?: any) {
      data = typeof data === 'object' ? data : {};
      data["psCode"] = this.psCode;
      data["bookingHeaderID"] = this.bookingHeaderID;
      data["documentType"] = this.documentType;
      data["documentFile"] = this.documentFile;
      data["documentPicType"] = this.documentPicType;
      data["priorityPassId"] = this.priorityPassId;
      data["userId"] = this.userId;
      return data; 
  }
}

export interface IDocumentKPRInputDto {
  psCode: string;
  bookingHeaderID: number;
  documentType: string;
  documentFile: string;
  documentPicType: string;
  priorityPassId: number;
  userId:any;
}
export class SendPaymentInstructionsDto implements ISendPaymentInstructionsDto {
  bookcode: string;
  orderCode: string;
  paymentInstructions: string;
  emailCustomer: string;
  customerName: string;
  memberCode: string;
  amountPay: string;

  constructor(data?: ISendPaymentInstructionsDto) {
      if (data) {
          for (var property in data) {
              if (data.hasOwnProperty(property))
                  (<any>this)[property] = (<any>data)[property];
          }
      }
  }

  init(data?: any) {
      if (data) {
          this.bookcode = data["bookcode"];
          this.orderCode = data["orderCode"];
          this.paymentInstructions = data["paymentInstructions"];
          this.emailCustomer = data["emailCustomer"];
          this.customerName = data["customerName"];
          this.memberCode = data["memberCode"];
          this.amountPay = data["amountPay"];
      }
  }

  static fromJS(data: any): SendPaymentInstructionsDto {
      let result = new SendPaymentInstructionsDto();
      result.init(data);
      return result;
  }

  toJSON(data?: any) {
      data = typeof data === 'object' ? data : {};
      data["bookcode"] = this.bookcode;
      data["orderCode"] = this.orderCode;
      data["paymentInstructions"] = this.paymentInstructions;
      data["emailCustomer"] = this.emailCustomer;
      data["customerName"] = this.customerName;
      data["memberCode"] = this.memberCode;
      data["amountPay"] = this.amountPay;
      return data; 
  }
}
export interface ISendPaymentInstructionsDto {
  bookcode: string;
  orderCode: string;
  paymentInstructions: string;
  emailCustomer: string;
  customerName: string;
  memberCode: string;
  amountPay: string;
}
export class SendEmailDpDto implements ISendEmailDpDto {
  bookcode: string;
  emailCustomer: string;
  namaCustomer: string;

  constructor(data?: ISendEmailDpDto) {
      if (data) {
          for (var property in data) {
              if (data.hasOwnProperty(property))
                  (<any>this)[property] = (<any>data)[property];
          }
      }
  }

  init(data?: any) {
      if (data) {
          this.bookcode = data["bookcode"];
          this.emailCustomer = data["emailCustomer"];
          this.namaCustomer = data["namaCustomer"];
      }
  }

  static fromJS(data: any): SendEmailDpDto {
      let result = new SendEmailDpDto();
      result.init(data);
      return result;
  }

  toJSON(data?: any) {
      data = typeof data === 'object' ? data : {};
      data["bookcode"] = this.bookcode;
      data["emailCustomer"] = this.emailCustomer;
      data["namaCustomer"] = this.namaCustomer;
      return data; 
  }
}

export interface ISendEmailDpDto {
  bookcode: string;
  emailCustomer: string;
  namaCustomer: string;
}
@Injectable({ providedIn: 'root' })
export class AuthService {
  //private baseUrl = '';getCurrentLoginInformationsGET
   //private loginUrl = 'http://18.140.60.145:1225/api/TokenAuth/Authenticate';
  
  private baseUrlss=environment.baseUrl;
//'http://18.140.60.145:1225'
  private http = inject(HttpClient);
  private router = inject(Router)

  // Signals to represent user roles
  isFarmOwner = signal(false);
  isFarmManager = signal(false);
  isAsstFarmManager = signal(false);
  isFarmWorker = signal(false);

  /**
   * Registers a new user.
   * @param userData - User registration data.
   * @returns A welcome message.
   * @throws Error if registration fails.
   */
  async registerUser(userData: UserRegistrationData): Promise<string> {
    try {
      const response = await firstValueFrom(this.http.post<RegistrationResponse>(this.baseUrlss, userData));
      const welcomeMessage = `Welcome, ${response.username}! to Peter's FARMS`;
      return welcomeMessage;
    } catch (error) {
      throw error;
    }
  }

  /**
   * Logs in a user and stores the authentication token.
   * @param userLoginData - User login data.
   * @returns Authentication token.
   * @throws Error if login fails.
   */
  // async login(userLoginData: LoginData): Promise<string> {
  //   try {
  //     const response = await firstValueFrom(this.http.post<LoginResponse>(this.loginUrl, userLoginData));
  //     localStorage.setItem('Token', response.auth_token);
  //     return response.auth_token;
  //   } catch (error) {
  //     throw 'An error occurred during login. Please try again.';
  //   }
  // }
  // async login(userLoginData: LoginData): Promise<string> {
    
  //   const headers = new HttpHeaders({
  //     "Content-Type": "application/json",
  //     "Access-Control-Allow-Origin": "*",
  //   });
    
  //   try {
  //     const response = await firstValueFrom(this.http.post<LoginResponse>(this.loginUrl, userLoginData, {headers}));
      
  //     if (response && response.result && response.result.accessToken) {
  //       const accessToken = response.result.accessToken;
  //       localStorage.setItem('Token', accessToken);
  //       return accessToken;
  //     } else {
  //       throw 'Invalid response format during login.';
  //     }
  //   } catch (error) {
  //     throw 'An error occurred during login. Please try again.';
  //   }
  // }
  async login(userLoginData: LoginData): Promise<any> {

    const baseUrl= this.baseUrlss + `/api/services/app/CashierSystem/Login`;
    const headers = new HttpHeaders({
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*",
        });
      
       // try {
          const response = await firstValueFrom(this.http.post<any>(baseUrl, userLoginData, { headers }));
      
          if (response && response?.result[0]?.result && response?.result[0]?.result.accessToken) {
            const accessToken = response?.result[0]?.result.accessToken;
            const expireInSeconds = response?.result[0]?.result.expireInSeconds;
            const userId = response?.result[0]?.result.userId;
     
            localStorage.setItem('Token', accessToken);
   
            const expirationTime = Date.now() + expireInSeconds * 1000;

            localStorage.setItem('TokenExpiresAt', expirationTime.toString());
            localStorage.setItem('UserId', userId);
         
            this.router.navigate(['/salestracking']);
            return response;
          } if(response?.result[0]?.errorMessage!=null) {
          Swal.fire({
                  title: `${response?.result[0]?.error.message}`,
                  text: `${response?.result[0]?.error.details}`,
                  icon: 'error',
                  timer: 3000, 
                  
                  showConfirmButton: true, 
          timerProgressBar: true 
                })
          
          }
       
      }
     

     
  ////LoginResponseInformation
  async GetMappingOfferingLetterNobu(bookCode: any): Promise<any> {
    const baseUrl= this.baseUrlss + `/api/services/app/CashierSystem/GetMappingOfferingLetterNobu`;
    const headers = new HttpHeaders({
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
    });

    try {
        // Assuming you are sending curentLoginDataInfo as query parameters
        const response = await firstValueFrom(this.http.get<any>(baseUrl, {
            headers,
            params: {
                // Add query parameters here based on curentLoginDataInfo properties
                // For example: param1: curentLoginDataInfo.param1,
                //              param2: curentLoginDataInfo.param2,
                bookCode: bookCode
            },
        }));

        if (response) {
            // Set the expiration time in localStorage
            // localStorage.setItem('TokenExpiresAt', expirationTime.toString());
            localStorage.setItem('informations', JSON.stringify(response.result));
            return response;
        } else {
            console.error('Invalid response format during login:', response);
            throw new Error('Invalid response format during login.');
        }
    } catch (error) {
        console.error('An error occurred during login:', error);
        throw new Error('An error occurred during login. Please try again.');
    }
}
//-----------------------------------------------------------------
async GetPlafondBookingDocument(bookCode: any): Promise<any> {
  const baseUrl= this.baseUrlss + `/api/services/app/CashierSystem/GetPlafondBookingDocument`;
  const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
  });

  try {
      // Assuming you are sending curentLoginDataInfo as query parameters
      const response = await firstValueFrom(this.http.get<any>(baseUrl, {
          headers,
          params: {
              // Add query parameters here based on curentLoginDataInfo properties
              // For example: param1: curentLoginDataInfo.param1,
              //              param2: curentLoginDataInfo.param2,
              bookCode: bookCode
          },
      }));

      if (response) {
          // Set the expiration time in localStorage
          // localStorage.setItem('TokenExpiresAt', expirationTime.toString());
          localStorage.setItem('informations', JSON.stringify(response.result));
          return response;
      } else {
          console.error('Invalid response format during login:', response);
          throw new Error('Invalid response format during login.');
      }
  } catch (error) {
      console.error('An error occurred during login:', error);
      throw new Error('An error occurred during login. Please try again.');
  }
}
//------------------------------------------------------------------
async getCurrentLoginInformationsGET(curentLoginDataInfo: CurentLoginDataInfo): Promise<any> {
  const baseUrl= this.baseUrlss + `/api/services/app/CashierSystem/getCurrentLoginInformationsGET`;
  const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
  });

  try {
      // Assuming you are sending curentLoginDataInfo as query parameters
      const response = await firstValueFrom(this.http.get<any>(baseUrl, {
          headers,
          params: {
              // Add query parameters here based on curentLoginDataInfo properties
              // For example: param1: curentLoginDataInfo.param1,
              //              param2: curentLoginDataInfo.param2,
              accessToken: curentLoginDataInfo.accessToken,
              expireInSeconds:curentLoginDataInfo.expireInSeconds,
              userId:curentLoginDataInfo.userId
          },
      }));

      if (response) {
          // Set the expiration time in localStorage
          // localStorage.setItem('TokenExpiresAt', expirationTime.toString());
          localStorage.setItem('informations', JSON.stringify(response.result));
          return response;
      } else {
          console.error('Invalid response format during login:', response);
          throw new Error('Invalid response format during login.');
      }
  } catch (error) {
      console.error('An error occurred during login:', error);
      throw new Error('An error occurred during login. Please try again.');
  }
}
//------------------------------------------------------------------
async gatewayOfUserPermissions(permissionLoginDataInfo: PermissionLoginDataInfo): Promise<any> {
  const baseUrl= this.baseUrlss + `api/services/app/CashierSystem/GatewayOfUserPermissions`;
  const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
  });

  try {
      // Assuming you are sending curentLoginDataInfo as query parameters
      const response = await firstValueFrom(this.http.get<any>(baseUrl, {
          headers,
          params: {
              // Add query parameters here based on curentLoginDataInfo properties
              // For example: param1: curentLoginDataInfo.param1,
              //              param2: curentLoginDataInfo.param2,
              accessToken: permissionLoginDataInfo.accessToken,
       
             
          },
      }));

      if (response) {
          // Set the expiration time in localStorage
          // localStorage.setItem('TokenExpiresAt', expirationTime.toString());
          console.log(response)
          
          return response;
      } else {
          console.error('Invalid response format during login:', response);
          throw new Error('Invalid response format during login.');
      }
  } catch (error) {
      console.error('An error occurred during login:', error);
      throw new Error('An error occurred during login. Please try again.');
  }
}
///
async gateBanerFoto(): Promise<any> {
  const baseUrl= this.baseUrlss + `/api/services/app/Project/GetListAllProduct`;
  const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
  });

  try {
      // Assuming you are sending curentLoginDataInfo as query parameters
      const response = await firstValueFrom(this.http.get<any>(baseUrl, {
          headers,
          params: {
              // Add query parameters here based on curentLoginDataInfo properties
              // For example: param1: curentLoginDataInfo.param1,
              //              param2: curentLoginDataInfo.param2,
              memberCode: '',
              searchBox:''
       
             
          },
      }));

      if (response) {
          // Set the expiration time in localStorage
          // localStorage.setItem('TokenExpiresAt', expirationTime.toString());
          console.log(response)
          
          return response;
      } else {
          console.error('Invalid response format during login:', response);
          throw new Error('Invalid response format during login.');
      }
  } catch (error) {
      console.error('An error occurred during login:', error);
      throw new Error('An error occurred during login. Please try again.');
  }
}
  // Function to check if the token is expired
  isTokenExpired(): boolean {
    const tokenExpiresAt = localStorage.getItem('TokenExpiresAt');
  
    if (tokenExpiresAt) {
      const expirationTime = parseInt(tokenExpiresAt, 10);
      return expirationTime < Date.now();
    }
  
    // If expiration time is not available, consider it expired
    return true;
  }
  
  // Function to handle logout
  logout(): void {
    // Perform any additional cleanup tasks if needed
    // Remove the token and expiration time from local storage
    localStorage.removeItem('Token');
    localStorage.removeItem('TokenExpiresAt');
  
    // Redirect the user to the logout page or any desired location
    // Example: this.router.navigate(['/logout']);
  }
  
  // Function to refresh the token
  async refreshToken(): Promise<string> {
    if (this.isTokenExpired()) {
      // Logout the user when the token has expired
      console.log(this.isTokenExpired())
      this.logout();
      return ''; // or return some default value indicating no valid token
    } else {
      // If the token is not expired, simply return the current token
      return localStorage.getItem('Token') || '';
    }
  }
  // async login(userNameOrEmailAddress: string, password: string) {
  //   // Menambahkan header ke permintaan HTTP
  //   const headers = new HttpHeaders({
  //     "Content-Type": "application/json",
  //     "Access-Control-Allow-Origin": "*",
  //   });

  //   // Menggunakan opsi headers pada permintaan post
  //   return this.http.post<string>(this.loginUrl, { userNameOrEmailAddress, password }, { headers });
  // }
////: InqueryLoginDataInfo
async gateInquerySelesTracking(InqueryLoginDataInfo:any): Promise<any> {
  const baseUrl= this.baseUrlss + `/api/services/app/CashierSystem/InquiryDataSalesTracking`;
  const headers = new HttpHeaders({
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
      });
    
      try {
        const response = await firstValueFrom(this.http.post<LoginResponse>(baseUrl, InqueryLoginDataInfo, { headers }));
    
        if (response ) {
          // const accessToken = response.result.accessToken;
          // const expireInSeconds = response.result.expireInSeconds;
    
          // // Save token and expiration time in local storage
          // localStorage.setItem('Token', accessToken);
          // //localStorage.setItem('TokenExpiresAt', (Date.now() + expireInSeconds * 1000).toString());
          // //localStorage.setItem('TokenExpiresAt', (Date.now() + expireInSeconds * 1000).toString());
          // const expireInSecondss = 2 * 60;
          // const expirationTime = Date.now() + expireInSeconds * 1000;

          // // Set the expiration time in localStorage
          // localStorage.setItem('TokenExpiresAt', expirationTime.toString());
          console.log(response,'pppppppppppppppppppppppppppppppppppppppppppppppppppppp')
          return response;
        } else {
          console.error('Invalid response format during login:', response);
          throw new Error('Invalid response format during login.');
        }
      } catch (error) {
        console.error('An error occurred during login:', error);
        throw new Error('An error occurred during login. Please try again.');
      }
    }

    
/////

async postPrintOLN(InqueryLoginDataInfo:any): Promise<any> {
  const baseUrl= this.baseUrlss + `/api/services/app/CashierSystem/PrintOLN`;
  const headers = new HttpHeaders({
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
      });
    
      try {
        const response = await firstValueFrom(this.http.post<any>(baseUrl, InqueryLoginDataInfo, { headers }));
    
        if (response ) {
          // const accessToken = response.result.accessToken;
          // const expireInSeconds = response.result.expireInSeconds;
    
          // // Save token and expiration time in local storage
          // localStorage.setItem('Token', accessToken);
          // //localStorage.setItem('TokenExpiresAt', (Date.now() + expireInSeconds * 1000).toString());
          // //localStorage.setItem('TokenExpiresAt', (Date.now() + expireInSeconds * 1000).toString());
          // const expireInSecondss = 2 * 60;
          // const expirationTime = Date.now() + expireInSeconds * 1000;

          // // Set the expiration time in localStorage
          // localStorage.setItem('TokenExpiresAt', expirationTime.toString());
          console.log(response,'pppppppppppppppppppppppppppppppppppppppppppppppppppppp')
          return response;
        } else {
          console.error('Invalid response format during login:', response);
          throw new Error('Invalid response format during login.');
        }
      } catch (error) {
        console.error('An error occurred during login:', error);
        throw new Error('An error occurred during login. Please try again.');
      }
    }


    async OrderPaymentMidtrans(InqueryLoginDataInfo:any): Promise<any> {
      debugger;
      const baseUrl= this.baseUrlss + `/api/services/app/CashierSystem/OrderPaymentMidtrans`;
      const headers = new HttpHeaders({
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
          });
        
          try {
            const response = await firstValueFrom(this.http.post<any>(baseUrl, InqueryLoginDataInfo, { headers }));
        
      // if(response.success==false){
      
      // }
            if (response ) {
              // const accessToken = response.result.accessToken;
              // const expireInSeconds = response.result.expireInSeconds;
        
              // // Save token and expiration time in local storage
              // localStorage.setItem('Token', accessToken);
              // //localStorage.setItem('TokenExpiresAt', (Date.now() + expireInSeconds * 1000).toString());
              // //localStorage.setItem('TokenExpiresAt', (Date.now() + expireInSeconds * 1000).toString());
              // const expireInSecondss = 2 * 60;
              // const expirationTime = Date.now() + expireInSeconds * 1000;
    
              // // Set the expiration time in localStorage
              // localStorage.setItem('TokenExpiresAt', expirationTime.toString());
              console.log(response,'pppppppppppppppppppppppppppppppppppppppppppppppppppppp')
              return response;
            } else {
              console.error('Invalid response format during login:', response);
              throw new Error('Invalid response format during login.');
            }
          } catch (error) {
            // console.error('An error occurred during login:', error);
            // throw new Error('An error occurred during login. Please try again.');
            Swal.fire({
              title: `An internal error occurred during your request!`,
         
              icon: 'error',
              showConfirmButton: true, // Menampilkan tombol OK
            
            });
          }
        }
// async gateInquerySelesTracking(permissionLoginDataInfo:InqueryLoginDataInfo ): Promise<any> {
//   const headers = new HttpHeaders({
//       "Content-Type": "application/json",
//       "Access-Control-Allow-Origin": "*",
//   });

//   try {
//       // Assuming you are sending curentLoginDataInfo as query parameters
//       const response = await firstValueFrom(this.http.post<any>(this.inqueryurl, {
//           headers,
//           params: {
//               // Add query parameters here based on curentLoginDataInfo properties
//               // For example: param1: curentLoginDataInfo.param1,
//               //              param2: curentLoginDataInfo.param2,
//               //accessToken: permissionLoginDataInfo.projectID,
//               memberCode: permissionLoginDataInfo.memberCode,
//               projectID: permissionLoginDataInfo.projectID,
//               keyword: permissionLoginDataInfo.keyword,
//               bookCode: permissionLoginDataInfo.bookCode,
//               termID: permissionLoginDataInfo.termID,
//               maxResultCount: permissionLoginDataInfo.maxResultCount,
//               skipCount: permissionLoginDataInfo.skipCount

            
       
             
//           },
//       }));

//       if (response) {
//           // Set the expiration time in localStorage
//           // localStorage.setItem('TokenExpiresAt', expirationTime.toString());
//           console.log(response)
          
//           return response;
//       } else {
//           console.error('Invalid response format during login:', response);
//           throw new Error('Invalid response format during login.');
//       }
//   } catch (error) {
//       console.error('An error occurred during login:', error);
//       throw new Error('An error occurred during login. Please try again.');
//   }
// }

  /**
   * Logs out the current user, removes the authentication token, and resets user role signals.
   * @throws Error if logout fails.
   */
  // async logout(): Promise<void> {
  //   try {
  //     await firstValueFrom(this.http.post(this.logOutUrl, {}));
  //     localStorage.removeItem('Token');
  //     this.resetSignals();
  //     this.router.navigate(['/login']);
  //   } catch (error) {
  //     throw 'An error occurred during logout. Please try again.';
  //   }
  // }

  /**
   * Verifies the authentication token and sets user role signals based on the user profile.
   * @returns True if the token is valid, and the user is authenticated; otherwise, false.
   */
  // async verifyToken(): Promise<boolean> {
  //   try {
  //     const response = await firstValueFrom(this.http.get<UserProfile>(this.userMeUrl));
  //     if (response instanceof Object) {
  //       // Assuming there's a property like 'id' that indicates a valid user
  //       if (response.id) {
  //         this.setSignals(response); // Set signals based on user roles
  //         return true; // Token is valid, and user is authenticated
  //       } else {
  //         localStorage.removeItem('Token');
  //         this.resetSignals();
  //         return false; // Token is valid, but the user is not authenticated
  //       }
  //     } else {
  //       localStorage.removeItem('Token');
  //       this.resetSignals();
  //       return false; // Token is invalid or unauthorized
  //     }
  //   } catch (error) {
  //     this.resetSignals();
  //     return false; // Token is invalid or unauthorized
  //   }
  // }
  async verifyToken(): Promise<boolean> {
    try {
      const accessToken = localStorage.getItem('Token');
  
      if (accessToken) {
        // Token is present, no need to make an HTTP request
        // You might want to add logic here to validate the token (e.g., expiration check)
        //this.setSignals(); // Set signals based on user roles
        return true; // Token is present, and user is considered authenticated
      } else {
        localStorage.removeItem('Token');
        this.resetSignals();
        return false; // Token is not present in local storage
      }
    } catch (error) {
      this.resetSignals();
      return false; // Token is invalid or unauthorized
    }
  }
  

  /**
   * Sets user role signals based on the user profile.
   * @param userProfile - User profile containing role information.
   */
  private setSignals(userProfile: UserProfile): void {
    this.isFarmOwner.set(userProfile.is_farm_owner);
    this.isFarmManager.set(userProfile.is_farm_manager);
    this.isAsstFarmManager.set(userProfile.is_assistant_farm_manager);
    this.isFarmWorker.set(userProfile.is_farm_worker);
  }

  /**
   * Resets user role signals to false.
   */
  private resetSignals(): void {
    this.isFarmOwner.set(false);
    this.isFarmManager.set(false);
    this.isAsstFarmManager.set(false);
    this.isFarmWorker.set(false);
  }


  // getListDocumentKPR(psCode: any, bookingHeaderID: any, ppNo: any): Observable<DocumentKPRListDto[]> {
  //   let url_ = `${this.baseUrl}/api/services/app/SalesTracking/GetListDocumentKPR?`;

  //   if (psCode !== undefined) {
  //     url_ += `psCode=${encodeURIComponent(psCode)}&`;
  //   }
  //   if (bookingHeaderID !== undefined) {
  //     url_ += `bookingHeaderID=${encodeURIComponent(bookingHeaderID.toString())}&`;
  //   }
  //   if (ppNo !== undefined) {
  //     url_ += `ppNo=${encodeURIComponent(ppNo)}&`;
  //   }

  //   url_ = url_.replace(/[?&]$/, '');

  //   const options_ = {
  //     headers: new HttpHeaders({
  //       'Content-Type': 'application/json',
  //       'Accept': 'application/json'
  //     })
  //   };

  //   return this.http.get(url_, options_).pipe(
  //     map((response: any) => this.processGetListDocumentKPR(response)),
  //     catchError(this.handleError)
  //   );
  // }
  async getListDocumentKPR(psCode: any, bookingHeaderID: any, ppNo: any): Promise<any> {
    const baseUrl= this.baseUrlss + `/api/services/app/CashierSystem/GetListDocumentKPR`;
    const headers = new HttpHeaders({
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
    });
  
    try {
        // Assuming you are sending curentLoginDataInfo as query parameters
        const response = await firstValueFrom(this.http.get<any>(baseUrl, {
            headers,
            params: {
                // Add query parameters here based on curentLoginDataInfo properties
                // For example: param1: curentLoginDataInfo.param1,
                //              param2: curentLoginDataInfo.param2,
                
                psCode: psCode, 
                bookingHeaderID: bookingHeaderID,
                ppNo: ppNo
               
            },
        }));
  
        if (response) {
            // Set the expiration time in localStorage
            // localStorage.setItem('TokenExpiresAt', expirationTime.toString());
            console.log(response)
            
            return response;
        } else {
            console.error('Invalid response format during login:', response);
            throw new Error('Invalid response format during login.');
        }
    } catch (error) {
        console.error('An error occurred during login:', error);
        throw new Error('An error occurred during login. Please try again.');
    }
  }
  private processGetListDocumentKPR(response: any): DocumentKPRListDto[] {
    const status = response.status;

    let result200: DocumentKPRListDto[] = [];

    if (status === 200) {
      const resultData200 = response.data; // Sesuaikan dengan format respon yang sesuai dengan API Anda

      if (resultData200 && Array.isArray(resultData200)) {
        result200 = resultData200.map((item: any) => DocumentKPRListDto.fromJS(item));
      }
    } else if (status !== 200 && status !== 204) {
      throw new Error("An unexpected server error occurred.");
    }
    console.log(result200)
    return result200;
  }

  private handleError(error: any): Observable<never> {
    let errorMessage = 'An error occurred';
    if (error.error instanceof ErrorEvent) {
      // Client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.error(errorMessage);
    return throwError(errorMessage);
  }

  ///

  

//   getDocumentKPRFile(psCode: string, bookingHeaderID: number, documentType: string, priorityPassId: number): Observable<DocumentKPRFileResultDto> {
//     let url_ = this.baseUrl + "/api/services/app/SalesTracking/GetDocumentKPRFile?";
//     if (psCode !== undefined)
//         url_ += "psCode=" + encodeURIComponent("" + psCode) + "&"; 
//     if (bookingHeaderID !== undefined)
//         url_ += "bookingHeaderID=" + encodeURIComponent("" + bookingHeaderID) + "&"; 
//     if (documentType !== undefined)
//         url_ += "documentType=" + encodeURIComponent("" + documentType) + "&"; 
//     if (priorityPassId !== undefined)
//         url_ += "PriorityPassId=" + encodeURIComponent("" + priorityPassId) + "&"; 
//     url_ = url_.replace(/[?&]$/, "");

//     let options_ : any = {
//         method: "get",
//         headers: new Headers({
//             "Content-Type": "application/json", 
//             "Accept": "application/json"
//         })
//     };

//     return this.http.request(url_, options_).flatMap((response_ : any) => {
//         return this.processGetDocumentKPRFile(response_);
//     }).catch((response_: any) => {
//         if (response_ instanceof Response) {
//             try {
//                 return this.processGetDocumentKPRFile(response_);
//             } catch (e) {
//                 return <Observable<DocumentKPRFileResultDto>><any>Observable.throw(e);
//             }
//         } else
//             return <Observable<DocumentKPRFileResultDto>><any>Observable.throw(response_);
//     });
// }

async getDocumentKPRFile(psCode: string, bookingHeaderID: number, documentType: string, priorityPassId: number): Promise<any> {
  const baseUrl= this.baseUrlss + `/api/services/app/CashierSystem/GetDocumentKPRFile`;
  const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
  });

  try {
      // Assuming you are sending curentLoginDataInfo as query parameters
      const response = await firstValueFrom(this.http.get<any>(baseUrl, {
          headers,
          params: {
              // Add query parameters here based on curentLoginDataInfo properties
              // For example: param1: curentLoginDataInfo.param1,
              //              param2: curentLoginDataInfo.param2,
              
              psCode: psCode, 
              bookingHeaderID: bookingHeaderID,
              documentType: documentType,
              priorityPassId:priorityPassId
             
          },
      }));

      if (response) {
          // Set the expiration time in localStorage
          // localStorage.setItem('TokenExpiresAt', expirationTime.toString());
          console.log(response)
          
          return response;
      } else {
          console.error('Invalid response format during login:', response);
          throw new Error('Invalid response format during login.');
      }
  } catch (error) {
      console.error('An error occurred during login:', error);
      throw new Error('An error occurred during login. Please try again.');
  }
}



// uploadDocumentKPR(input: DocumentKPRInputDto): Observable<void> {
//   let url_ = this.baseUrl + "/api/services/app/SalesTracking/UploadDocumentKPR";
//   url_ = url_.replace(/[?&]$/, "");

//   const content_ = JSON.stringify(input);

//   let options_ : any = {
//       body: content_,
//       method: "post",
//       headers: new Headers({
//           "Content-Type": "application/json", 
//       })
//   };

//   return this.http.request(url_, options_).flatMap((response_ : any) => {
//       return this.processUploadDocumentKPR(response_);
//   }).catch((response_: any) => {
//       if (response_ instanceof Response) {
//           try {
//               return this.processUploadDocumentKPR(response_);
//           } catch (e) {
//               return <Observable<void>><any>Observable.throw(e);
//           }
//       } else
//           return <Observable<void>><any>Observable.throw(response_);
//   });
// }


async uploadDocumentKPR(InqueryLoginDataInfo: DocumentKPRInputDto): Promise<any> {
  const baseUrl= this.baseUrlss + `/api/services/app/CashierSystem/UploadDocumentKPR`;
  const headers = new HttpHeaders({
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
      });
    
      try {
        const response = await firstValueFrom(this.http.post<any>(baseUrl, InqueryLoginDataInfo, { headers }));
    
        if (response ) {
          // const accessToken = response.result.accessToken;
          // const expireInSeconds = response.result.expireInSeconds;
    
          // // Save token and expiration time in local storage
          // localStorage.setItem('Token', accessToken);
          // //localStorage.setItem('TokenExpiresAt', (Date.now() + expireInSeconds * 1000).toString());
          // //localStorage.setItem('TokenExpiresAt', (Date.now() + expireInSeconds * 1000).toString());
          // const expireInSecondss = 2 * 60;
          // const expirationTime = Date.now() + expireInSeconds * 1000;

          // // Set the expiration time in localStorage
          // localStorage.setItem('TokenExpiresAt', expirationTime.toString());
    
          return response;
        } else {
          console.error('Invalid response format during login:', response);
          throw new Error('Invalid response format during login.');
        }
      } catch (error) {
        console.error('An error occurred during login:', error);
        throw new Error('An error occurred during login. Please try again.');
      }
    }
   
  async getDetailTaskListMyCommisionBF(bookingHeaderID: any, bookCode: any, memberCode: any, memberCodeLogin: any): Promise<any> {
    const baseUrl= this.baseUrlss + `/api/services/app/CashierSystem/GetDetailTaskListMyCommisionBF`;
    const headers = new HttpHeaders({
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
    });
  
    try {
        // Assuming you are sending curentLoginDataInfo as query parameters
        const response = await firstValueFrom(this.http.get<any>(baseUrl, {
            headers,
            params: {
                // Add query parameters here based on curentLoginDataInfo properties
                // For example: param1: curentLoginDataInfo.param1,
                //              param2: curentLoginDataInfo.param2,
                
                bookingHeaderID: bookingHeaderID, 
                bookCode: bookCode,
                memberCode: memberCode,
                memberCodeLogin:memberCodeLogin
               
            },
        }));
  
        if (response) {
            // Set the expiration time in localStorage
            // localStorage.setItem('TokenExpiresAt', expirationTime.toString());
            console.log(response)
            
            return response;
        } else {
            console.error('Invalid response format during login:', response);
            throw new Error('Invalid response format during login.');
        }
    } catch (error) {
        console.error('An error occurred during login:', error);
        throw new Error('An error occurred during login. Please try again.');
    }
  }


  
//   aasync getOutstandingDownPayment(bookcode: string): Promise<any> {
//       let url_ = this.baseUrlss + "/api/services/app/SalesTracking/getOutstandingDownPayment?";
//       if (bookcode !== undefined)
//           url_ += "bookcode=" + encodeURIComponent("" + bookcode) + "&"; 
//       url_ = url_.replace(/[?&]$/, "");
  
//       let options_: any = {
//           method: "get",
//           headers: new Headers({
//               "Content-Type": "application/json", 
//               "Accept": "application/json"
//           })
//       };
  
//       try {
//           const response = await firstValueFrom(this.http.request(url_, options_));
//           return this.processGetOutstandingDownPayment(response);
//       } catch (error) {
//           throw error;
//       }
//   }
  
//   protected processGetOutstandingDownPayment(response:any) {
//     const status = response.status; 

//     if (status === 200) {
//         // You can directly return the response as any type
//         return response;
//     } else if (status !== 200 && status !== 204) {
//         // Or you can throw an error here
//         console.log(status)
//     }
//     return null;
// }
  
  
  
// async sendPaymentInstructions(input: SendPaymentInstructionsDto): Promise<any> {
//   let url_ = this.baseUrli + "/api/services/app/SalesTracking/SendPaymentInstructions";
//   url_ = url_.replace(/[?&]$/, "");

//   const content_ = JSON.stringify(input);

//   let options_ : any = {
//       body: content_,
//       method: "post",
//       headers: new Headers({
//           "Content-Type": "application/json", 
//       })
//   };
//   try {
//     const response = await firstValueFrom(this.http.request(url_, options_));
//     return this.processSendPaymentInstructions(response);
// } catch (error) {
//     throw error;
// }
 
// }
// async sendPaymentInstructions(input:any): Promise<any> {
//   const headers = new HttpHeaders({
//         "Content-Type": "application/json",
//         "Access-Control-Allow-Origin": "*",
//       });
    
//       try {
//         const response = await firstValueFrom(this.http.post<test>(this.baseUrli, input, { headers }));
    
//         if (response ) {
//           // const accessToken = response.result.accessToken;
//           // const expireInSeconds = response.result.expireInSeconds;
    
//           // // Save token and expiration time in local storage
//           // localStorage.setItem('Token', accessToken);
//           // //localStorage.setItem('TokenExpiresAt', (Date.now() + expireInSeconds * 1000).toString());
//           // //localStorage.setItem('TokenExpiresAt', (Date.now() + expireInSeconds * 1000).toString());
//           // const expireInSecondss = 2 * 60;
//           // const expirationTime = Date.now() + expireInSeconds * 1000;

//           // // Set the expiration time in localStorage
//           // localStorage.setItem('TokenExpiresAt', expirationTime.toString());
    
//           return response;
//         } else {
//           console.error('Invalid response format during login:', response);
//           throw new Error('Invalid response format during login.');
//         }
//       } catch (error) {
//         console.error('An error occurred during login:', error);
//         throw new Error('An error occurred during login. Please try again.');
//       }
//     }
    
// async sendPaymentInstructions(input:any): Promise<any> {

//   const baseUrl= this.baseUrlss + `/api/services/app/CashierSystem/SendPaymentInstructions`;
//   const headers = new HttpHeaders({
//         "Content-Type": "application/json",
//         "Access-Control-Allow-Origin": "*",
//       });
    
//      // try {
//         const response = await firstValueFrom(this.http.post<any>(baseUrl, input, { headers }));
    
//         if (response.result.status!='Failed'||response.result.status==null||response.result.status==undefined) {
//           Swal.fire(
//             'Success to send payment instructions',
//             '',
//             'success'
//           );
//           return response;
//         } if(response.result.status=='Failed') {
//         Swal.fire({
//                 title: `${response.result.status}`,
//                 text: `${response.result.errorMessage}`,
//                 icon: 'error',
//                 timer: 3000, 
                
//                 showConfirmButton: true, 
//         timerProgressBar: true 
//               })
        
//         }
     
//     }
async sendPaymentInstructions(input: any): Promise<any> {
  const baseUrl = this.baseUrlss + `/api/services/app/CashierSystem/SendPaymentInstructions`;
  const headers = new HttpHeaders({
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": "*",
  });

  try {
    return await firstValueFrom(this.http.post<any>(baseUrl, input, { headers }));
  } catch (error) {
    // Lebih baik untuk melemparkan kesalahan ke komponen yang memanggil layanan
    throw error;
  }
}

async sendEmailAccountStatementByBookCode(bookCode: string): Promise<any> {
  //const baseUrl= this.baseUrlss + "/api/services/app/OrderPP/ResendEmail?PPOrderID=${PPOrderID}async resendEmail(PPOrderID: number): Promise<any> {
    const baseUrl = this.baseUrlss + `/api/services/app/CashierSystem/SendEmailAccountStatementByBookCode?bookCode=${bookCode}`;
    const headers = new HttpHeaders({
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
    });

    try {
        const response = await this.http.post<test>(baseUrl, { bookCode }, { headers }).toPromise();

        if (response) {
            return response;
        } else {
            console.error('Invalid response format during login:', response);
            throw new Error('Invalid response format during login.');
        }
    } catch (error) {
        console.error('An error occurred during login:', error);
        throw new Error('An error occurred during login. Please try again.');
    }
}
// sendPaymentInstructions(input:any): Promise<any> {
//   const baseUrl= this.baseUrlss + `/api/services/app/CashierSystem/SendPaymentInstructions`;
//       let url_ = baseUrl;
//       url_ = url_.replace(/[?&]$/, "");

//       const content_ = JSON.stringify(input);

//       const options_ : any = {
//           body: content_,
//           method: "post",
//           headers: new HttpHeaders({
//               "Content-Type": "application/json", 
//           })
//       };

//       return new Promise<void>((resolve, reject) => {
//           this.http.request('post', url_, options_).toPromise()
//           .then((response: any) => {
//               resolve(this.processSendEmailConfirmation(response));
//           })
//           .catch((error: any) => {
//               reject(this.handleError(error));
//           });
//       });
//   }

  protected processSendEmailConfirmation(response: any): void {
      const status = response.status; 

      if (status === 200) {
          return;
      } else if (status !== 200 && status !== 204) {
          throw new Error("An unexpected server error occurred.");
      }
  }

  // private handleError(error: any): Error {
  //     let errorMessage: string;
  //     if (error instanceof HttpResponse) {
  //         errorMessage = `Error ${error.status}: ${error.statusText}`;
  //     } else {
  //         errorMessage = error.message ? error.message : error.toString();
  //     }
  //     return new Error(errorMessage);
  // }
  async sendEmailConfirmation(_bookCode:any,_email:any, _name:any): Promise<any> {
    
    const baseUrl = this.baseUrlss + `/api/services/app/CashierSystem/SendEmailConfirmation`;
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    });
  
    try {
      return   await firstValueFrom(this.http.post<test>(baseUrl, {bookcode:_bookCode,emailCustomer:_email,namaCustomer:_name}, { headers }));
    } catch {
      // Lebih baik untuk melemparkan kesalahan ke komponen yang memanggil layanan

        Swal.fire({
          title: 'Error',
          text:`An internal error occurred during your request!`,
          icon: 'error',
          timer: 3000,
          showConfirmButton: true,
          timerProgressBar: true
        });
      
  
      
    }
  }
    // async sendEmailConfirmation(input: SendEmailDpDto): Promise<any> {
    //   const baseUrl= this.baseUrlss + `/api/services/app/SalesTracking/SendEmailConfirmation`;
    //   const headers = new HttpHeaders({
    //         "Content-Type": "application/json",
    //         "Access-Control-Allow-Origin": "*",
    //       });
        
    //       try {
    //         const response = await firstValueFrom(this.http.post<test>(baseUrl, input, { headers }));
        
    //         if (response ) {
    //           // const accessToken = response.result.accessToken;
    //           // const expireInSeconds = response.result.expireInSeconds;
        
    //           // // Save token and expiration time in local storage
    //           // localStorage.setItem('Token', accessToken);
    //           // //localStorage.setItem('TokenExpiresAt', (Date.now() + expireInSeconds * 1000).toString());
    //           // //localStorage.setItem('TokenExpiresAt', (Date.now() + expireInSeconds * 1000).toString());
    //           // const expireInSecondss = 2 * 60;
    //           // const expirationTime = Date.now() + expireInSeconds * 1000;
    
    //           // // Set the expiration time in localStorage
    //           // localStorage.setItem('TokenExpiresAt', expirationTime.toString());
        
    //           return response;
    //         } else {
    //           console.error('Invalid response format during login:', response);
    //           throw new Error('Invalid response format during login.');
    //         }
    //       } catch (error) {
    //         console.error('An error occurred during login:', error);
    //         throw new Error('An error occurred during login. Please try again.');
    //       }
    //     }

// protected processSendPaymentInstructions(response:any) {
//   const status = response.status; 

//   if (status === 200) {
//       // You can directly return the response as any type
//       return response;
//   } else if (status !== 200 && status !== 204) {
//       // Or you can throw an error here
//       console.log(status)
//   }
//   return null;
// }
// async sendEmailAccountStatementByBookCode(bookCode
//   : any): Promise<any> {
//   const baseUrl = this.baseUrlss + `/api/services/app/CashierSystem/SendEmailAccountStatementByBookCode`;
//   const headers = new HttpHeaders({
//     "Content-Type": "application/json",
//     "Access-Control-Allow-Origin": "*",
//   });

//   try {
//     return await firstValueFrom(this.http.post<any>(baseUrl,bookCode , { headers }));
//   } catch (error) {
//     Swal.fire({
//       title: `Error`,
//       text: `the server responded with a status of 500 (Internal Server Error)`,
//       icon: 'error',
//       timer: 3000, 
      
//       showConfirmButton: true, 
// timerProgressBar: true 
//     })
     
//     throw error;
//   }
// }
// async sendEmailAccountStatementByBookCode(bookCode: string): Promise<any> {
//   const baseUrl= this.baseUrlss + `/api/services/app/PSASAccountStatement/SendEmailAccountStatementByBookCode?bookCode=${bookCode}`;
//   const headers = new HttpHeaders({
//         "Content-Type": "application/json",
//         "Access-Control-Allow-Origin": "*",
//       });
    
//       try {
//         const response = await this.http.post<test>(baseUrl, {bookCode}, { headers }).toPromise();
    
//         if (response ) {
         
    
//           return response;
//         } else {
//           console.error('Invalid response format during login:', response);
//           throw new Error('Invalid response format during login.');
//         }
//       } catch (error) {
//         console.error('An error occurred during login:', error);
//         throw new Error('An error occurred during login. Please try again.');
//       }
//     }
    async resendEmail(PPOrderID: number): Promise<any> {
      //const baseUrl= this.baseUrlss + "/api/services/app/OrderPP/ResendEmail?PPOrderID=${PPOrderID}async resendEmail(PPOrderID: number): Promise<any> {
        const baseUrl = this.baseUrlss + `/api/services/app/CashierSystem/ResendEmail?PPOrderID=${PPOrderID}`;
        const headers = new HttpHeaders({
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
        });
    
        try {
            const response = await this.http.post<test>(baseUrl, { PPOrderID }, { headers }).toPromise();
    
            if (response) {
                return response;
            } else {
                console.error('Invalid response format during login:', response);
                throw new Error('Invalid response format during login.');
            }
        } catch (error) {
            console.error('An error occurred during login:', error);
            throw new Error('An error occurred during login. Please try again.');
        }
    }
    // async resendEmail(PPOrderID: number): Promise<any> {
    //   const baseUrl = this.baseUrlss + `/api/services/app/CashierSystem/ResendEmail`;
    //   const headers = new HttpHeaders({
    //         "Content-Type": "application/json",
    //         "Access-Control-Allow-Origin": "*",
    //       });
        
    //       try {
    //         const response = await firstValueFrom(this.http.post<test>(baseUrl, { PPOrderID }, { headers }));
        
    //         if (response ) {
    //           // const accessToken = response.result.accessToken;
    //           // const expireInSeconds = response.result.expireInSeconds;
        
    //           // // Save token and expiration time in local storage
    //           // localStorage.setItem('Token', accessToken);
    //           // //localStorage.setItem('TokenExpiresAt', (Date.now() + expireInSeconds * 1000).toString());
    //           // //localStorage.setItem('TokenExpiresAt', (Date.now() + expireInSeconds * 1000).toString());
    //           // const expireInSecondss = 2 * 60;
    //           // const expirationTime = Date.now() + expireInSeconds * 1000;
    
    //           // // Set the expiration time in localStorage
    //           // localStorage.setItem('TokenExpiresAt', expirationTime.toString());
        
    //           return response;
    //         } else {
    //           console.error('Invalid response format during login:', response);
    //           throw new Error('Invalid response format during login.');
    //         }
    //       } catch (error) {
    //         console.error('An error occurred during login:', error);
    //         throw new Error('An error occurred during login. Please try again.');
    //       }
    //     }
    async resendKP(orderHeaderID: number): Promise<any> {
      
      //const baseUrl= this.baseUrlss + "/api/services/app/OrderPP/ResendEmail?PPOrderID=${PPOrderID}async resendEmail(PPOrderID: number): Promise<any> {
        const baseUrl = this.baseUrlss + `/api/services/app/CashierSystem/ResendKP?orderHeaderID=${orderHeaderID}`;
        const headers = new HttpHeaders({
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
        });
    
        try {
            const response = await this.http.post<test>(baseUrl, { orderHeaderID }, { headers }).toPromise();
    
            if (response) {
              console.log(response)
                return response;
            } else {
                console.error('Invalid response format during login:', response);
                throw new Error('Invalid response format during login.');
            }
        } catch (error) {
            console.error('An error occurred during login:', error);
            throw new Error('An error occurred during login. Please try again.');
        }
    }

    async resendLayoutPlan(input: ResendEmailLayoutPlanDto): Promise<any> {
      const baseUrl = this.baseUrlss + `/api/services/app/CashierSystem/ResendLayoutPlan`;
      const headers = new HttpHeaders({
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
          });
        
          try {
            const response = await firstValueFrom(this.http.post<test>(baseUrl, input, { headers }));
        
            if (response ) {
              // const accessToken = response.result.accessToken;
              // const expireInSeconds = response.result.expireInSeconds;
        
              // // Save token and expiration time in local storage
              // localStorage.setItem('Token', accessToken);
              // //localStorage.setItem('TokenExpiresAt', (Date.now() + expireInSeconds * 1000).toString());
              // //localStorage.setItem('TokenExpiresAt', (Date.now() + expireInSeconds * 1000).toString());
              // const expireInSecondss = 2 * 60;
              // const expirationTime = Date.now() + expireInSeconds * 1000;
    
              // // Set the expiration time in localStorage
              // localStorage.setItem('TokenExpiresAt', expirationTime.toString());
        
              return response;
            } else {
              console.error('Invalid response format during login:', response);
              throw new Error('Invalid response format during login.');
            }
          } catch (error) {
            console.error('An error occurred during login:', error);
            throw new Error('An error occurred during login. Please try again.');
          }
        }


        async resendEmailDoc(input: ResendEmailByBookCodeInputDto): Promise<any> {
          const baseUrl = this.baseUrlss + `/api/services/app/CashierSystem/ResendEmailDoc`;
          const headers = new HttpHeaders({
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*",
              });
            
              try {
                const response = await firstValueFrom(this.http.post<test>(baseUrl, input, { headers }));
            
                if (response ) {
                  // const accessToken = response.result.accessToken;
                  // const expireInSeconds = response.result.expireInSeconds;
            
                  // // Save token and expiration time in local storage
                  // localStorage.setItem('Token', accessToken);
                  // //localStorage.setItem('TokenExpiresAt', (Date.now() + expireInSeconds * 1000).toString());
                  // //localStorage.setItem('TokenExpiresAt', (Date.now() + expireInSeconds * 1000).toString());
                  // const expireInSecondss = 2 * 60;
                  // const expirationTime = Date.now() + expireInSeconds * 1000;
        
                  // // Set the expiration time in localStorage
                  // localStorage.setItem('TokenExpiresAt', expirationTime.toString());
            
                  return response;
                } else {
                  console.error('Invalid response format during login:', response);
                  throw new Error('Invalid response format during login.');
                }
              } catch (error) {
                console.error('An error occurred during login:', error);
                throw new Error('An error occurred during login. Please try again.');
              }
            }
        
            async sendEmailDP(input: SendEmailDpDto): Promise<any> {
              const baseUrl = this.baseUrlss + `/api/services/app/SalesTracking/SendEmailDP`;
              const headers = new HttpHeaders({
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*",
                  });
                
                  try {
                    const response = await firstValueFrom(this.http.post<test>(baseUrl, input, { headers }));
                
                    if (response ) {
                      // const accessToken = response.result.accessToken;
                      // const expireInSeconds = response.result.expireInSeconds;
                
                      // // Save token and expiration time in local storage
                      // localStorage.setItem('Token', accessToken);
                      // //localStorage.setItem('TokenExpiresAt', (Date.now() + expireInSeconds * 1000).toString());
                      // //localStorage.setItem('TokenExpiresAt', (Date.now() + expireInSeconds * 1000).toString());
                      // const expireInSecondss = 2 * 60;
                      // const expirationTime = Date.now() + expireInSeconds * 1000;
            
                      // // Set the expiration time in localStorage
                      // localStorage.setItem('TokenExpiresAt', expirationTime.toString());
                
                      return response;
                    } else {
                      console.error('Invalid response format during login:', response);
                      throw new Error('Invalid response format during login.');
                    }
                  } catch (error) {
                    console.error('An error occurred during login:', error);
                    throw new Error('An error occurred during login. Please try again.');
                  }
                }
                // async sendPaymentInstructions(input: SendPaymentInstructionsDto): Promise<any> {
                //   const baseUrl = this.baseUrlss + `/api/services/app/SalesTracking/SendPaymentInstructions`;
                //   const headers = new HttpHeaders({
                //         "Content-Type": "application/json",
                //         "Access-Control-Allow-Origin": "*",
                //       });
                    
                //       try {
                //         const response = await firstValueFrom(this.http.post<test>(baseUrl, input, { headers }));
                    
                //         if (response ) {
                //           // const accessToken = response.result.accessToken;
                //           // const expireInSeconds = response.result.expireInSeconds;
                    
                //           // // Save token and expiration time in local storage
                //           // localStorage.setItem('Token', accessToken);
                //           // //localStorage.setItem('TokenExpiresAt', (Date.now() + expireInSeconds * 1000).toString());
                //           // //localStorage.setItem('TokenExpiresAt', (Date.now() + expireInSeconds * 1000).toString());
                //           // const expireInSecondss = 2 * 60;
                //           // const expirationTime = Date.now() + expireInSeconds * 1000;
                
                //           // // Set the expiration time in localStorage
                //           // localStorage.setItem('TokenExpiresAt', expirationTime.toString());
                    
                //           return response;
                //         } else {
                //           console.error('Invalid response format during login:', response);
                //           throw new Error('Invalid response format during login.');
                //         }
                //       } catch (error) {
                //         console.error('An error occurred during login:', error);
                //         throw new Error('An error occurred during login. Please try again.');
                //       }
                //     }
                // async printSignedDoc(input: any): Promise<any> {
                //   const baseUrl = this.baseUrlss + `/api/services/app/CashierSystem/PrintSignedDoc`;
                //   const headers = new HttpHeaders({
                //         "Content-Type": "application/json",
                //         "Access-Control-Allow-Origin": "*",
                //       });
                    
                //       try {
                //         const response = await firstValueFrom(this.http.post<test>(baseUrl, input, { headers }));
                    
                //         if (response ) {
                //           // const accessToken = response.result.accessToken;
                //           // const expireInSeconds = response.result.expireInSeconds;
                    
                //           // // Save token and expiration time in local storage
                //           // localStorage.setItem('Token', accessToken);
                //           // //localStorage.setItem('TokenExpiresAt', (Date.now() + expireInSeconds * 1000).toString());
                //           // //localStorage.setItem('TokenExpiresAt', (Date.now() + expireInSeconds * 1000).toString());
                //           // const expireInSecondss = 2 * 60;
                //           // const expirationTime = Date.now() + expireInSeconds * 1000;
                
                //           // // Set the expiration time in localStorage
                //           // localStorage.setItem('TokenExpiresAt', expirationTime.toString());
                    
                //           return response;
                //         } else {
                //           console.error('Invalid response format during login:', response);
                //           throw new Error('Invalid response format during login.');
                //         }
                //       } catch (error) {
                //         console.error('An error occurred during login:', error);
                //         throw new Error('An error occurred during login. Please try again.');
                //       }
                //     }
                async printSignedDoc(bookCode: string,docCode:string): Promise<any> {
                  
                  //const baseUrl= this.baseUrlss + "/api/services/app/OrderPP/ResendEmail?PPOrderID=${PPOrderID}async resendEmail(PPOrderID: number): Promise<any> {
                    const baseUrl = this.baseUrlss + `/api/services/app/CashierSystem/PrintSignedDoc?bookCode=${bookCode}&docCode=${docCode}`;
                    const headers = new HttpHeaders({
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Origin": "*",
                    });
                
                    try {
                        const response = await this.http.post<test>(baseUrl, { bookCode, docCode}, { headers }).toPromise();
                
                        if (response) {
                          console.log(response)
                            return response;
                        } else {
                            console.error('Invalid response format during login:', response);
                            throw new Error('Invalid response format during login.');
                        }
                    } catch (error) {
                        console.error('An error occurred during login:', error);
                        throw new Error('An error occurred during login. Please try again.');
                    }
                }
                // async printSignedDoc(bookCode: string, docCode: string): Promise<any> {
                //   //const baseUrl= this.baseUrlss + "/api/services/app/OrderPP/ResendEmail?PPOrderID=${PPOrderID}async resendEmail(PPOrderID: number): Promise<any> {
                //     const baseUrl = this.baseUrlss + `/api/services/app/SalesTracking/PrintSignedDoc?bookCode=${bookCode}&docCode=${docCode}`;
                //     const headers = new HttpHeaders({
                //         "Content-Type": "application/json",
                //         "Access-Control-Allow-Origin": "*",
                //     });
                
                //     try {
                //         const response = await this.http.post<test>(baseUrl, { bookCode,docCode }, { headers }).toPromise();
                
                //         if (response) {
                //             return response;
                //         } else {
                //             console.error('Invalid response format during login:', response);
                //             throw new Error('Invalid response format during login.');
                //         }
                //     } catch (error) {
                //         console.error('An error occurred during login:', error);
                //         throw new Error('An error occurred during login. Please try again.');
                //     }
                // }
                //



                async printPPPU(bookCode: string): Promise<any> {
                  //const baseUrl= this.baseUrlss + "/api/services/app/OrderPP/ResendEmail?PPOrderID=${PPOrderID}async resendEmail(PPOrderID: number): Promise<any> {
                    const baseUrl = this.baseUrlss + `/api/services/app/CashierSystem/PrintPPPU?bookCode=${bookCode}`;
                    const headers = new HttpHeaders({
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Origin": "*",
                    });
                
                    try {
                        const response = await this.http.post<test>(baseUrl, { bookCode }, { headers }).toPromise();
                
                        if (response) {
                            return response;
                        } else {
                            console.error('Invalid response format during login:', response);
                            throw new Error('Invalid response format during login.');
                        }
                    } catch (error) {
                        console.error('An error occurred during login:', error);
                        throw new Error('An error occurred during login. Please try again.');
                    }
                }
                
                // getListProjectBySchema(memberCode: any, id:any): Observable<any> {
                //   const baseUrl = `${this.baseUrlss}/api/services/app/CashierSystem/GetListProjectBySchema`;
                //   const headers = new HttpHeaders({
                //     "Content-Type": "application/json",
                //     "Access-Control-Allow-Origin": "*",
                //   });
              
                //   const params = new HttpParams().set('memberCode', memberCode).set('id',id);
              
                //   return this.http.get<any>(baseUrl, { headers, params }).pipe(
                //     catchError(error => {
                //       console.error('An error occurred:', error);
                //       throw new Error('An error occurred. Please try again.');
                //     })
                //   );
                // }
                async getListProjectBySchema(memberCode: any, id:any): Promise<any> {
                  const baseUrl= `${this.baseUrlss}/api/services/app/CashierSystem/GetListProjectBySchema`;
                  const headers = new HttpHeaders({
                      "Content-Type": "application/json",
                      "Access-Control-Allow-Origin": "*",
                  });
                
                  try {
                      // Assuming you are sending curentLoginDataInfo as query parameters
                      const response = await firstValueFrom(this.http.get<any>(baseUrl, {
                          headers,
                          params: {
                              // Add query parameters here based on curentLoginDataInfo properties
                              // For example: param1: curentLoginDataInfo.param1,
                              //              param2: curentLoginDataInfo.param2,
                              
                              memberCode: memberCode, 
                              id: id,
                           
                             
                          },
                      }));
                
                      if (response) {
                          // Set the expiration time in localStorage
                          // localStorage.setItem('TokenExpiresAt', expirationTime.toString());
                          console.log(response)
                          
                          return response;
                      } else {
                          console.error('Invalid response format during login:', response);
                          throw new Error('Invalid response format during login.');
                      }
                  } catch (error) {
                      console.error('An error occurred during login:', error);
                      throw new Error('An error occurred during login. Please try again.');
                  }
                }
                // getTermBooking(bookCode: any): Observable<any> {
                //   const baseUrl = `${this.baseUrlss}/api/services/app/CashierSystem/GetTermBookingItemPriceDropdown`;
                //   const headers = new HttpHeaders({
                //     "Content-Type": "application/json",
                //     "Access-Control-Allow-Origin": "*",
                //   });
              
                //   const params = new HttpParams().set('bookCode', bookCode);
              
                //   return this.http.get<any>(baseUrl, { headers, params }).pipe(
                //     catchError(error => {
                //       console.error('An error occurred:', error);
                //       throw new Error('An error occurred. Please try again.');
                //     })
                //   );
                // }
                async getTermBooking(bookCode: any): Promise<any> {
                  const baseUrl= `${this.baseUrlss}/api/services/app/CashierSystem/GetTermBookingItemPriceDropdown`;
                  const headers = new HttpHeaders({
                      "Content-Type": "application/json",
                      "Access-Control-Allow-Origin": "*",
                  });
                
                  try {
                      // Assuming you are sending curentLoginDataInfo as query parameters
                      const response = await firstValueFrom(this.http.get<any>(baseUrl, {
                          headers,
                          params: {
                              // Add query parameters here based on curentLoginDataInfo properties
                              // For example: param1: curentLoginDataInfo.param1,
                              //              param2: curentLoginDataInfo.param2,
                              
                              bookCode: bookCode, 
                              // id: id,
                           
                             
                          },
                      }));
                
                      if (response) {
                          // Set the expiration time in localStorage
                          // localStorage.setItem('TokenExpiresAt', expirationTime.toString());
                          console.log(response)
                          
                          return response;
                      } else {
                          console.error('Invalid response format during login:', response);
                          throw new Error('Invalid response format during login.');
                      }
                  } catch (error) {
                      console.error('An error occurred during login:', error);
                      throw new Error('An error occurred during login. Please try again.');
                  }
                }
                // getTermBookingBank(projectID: any): Observable<any> {
                //   const baseUrl = `${this.baseUrlss}/api/services/app/CashierSystem/GetListBankKPA`;
                //   const headers = new HttpHeaders({
                //     "Content-Type": "application/json",
                //     "Access-Control-Allow-Origin": "*",
                //   });
              
                //   const params = new HttpParams().set('projectID', projectID);
              
                //   return this.http.get<any>(baseUrl, { headers, params }).pipe(
                //     catchError(error => {
                //       console.error('An error occurred:', error);
                //       throw new Error('An error occurred. Please try again.');
                //     })
                //   );
                // }

                //////
                async getTermBookingBank(projectID: any): Promise<any> {
                  const baseUrl= `${this.baseUrlss}/api/services/app/CashierSystem/GetListBankKPA`;
                  const headers = new HttpHeaders({
                      "Content-Type": "application/json",
                      "Access-Control-Allow-Origin": "*",
                  });
                
                  try {
                      // Assuming you are sending curentLoginDataInfo as query parameters
                      const response = await firstValueFrom(this.http.get<any>(baseUrl, {
                          headers,
                          params: {
                              // Add query parameters here based on curentLoginDataInfo properties
                              // For example: param1: curentLoginDataInfo.param1,
                              //              param2: curentLoginDataInfo.param2,
                              
                              projectID: projectID, 
                              // id: id,
                           
                             
                          },
                      }));
                
                      if (response) {
                          // Set the expiration time in localStorage
                          // localStorage.setItem('TokenExpiresAt', expirationTime.toString());
                          console.log(response)
                          
                          return response;
                      } else {
                          console.error('Invalid response format during login:', response);
                          throw new Error('Invalid response format during login.');
                      }
                  } catch (error) {
                      console.error('An error occurred during login:', error);
                      throw new Error('An error occurred during login. Please try again.');
                  }
                }
                updateTerm(payload: any): Observable<any> {
        
                  const baseUrl = `${this.baseUrlss}/api/services/app/CashierSystem/UpdateTerm`;
                  const headers = new HttpHeaders({
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*",
                  });
              
                  return this.http.put<any>(baseUrl, payload, { headers: headers });
                }
                RegenerateSchedule(payload: any): Observable<any> {
                  const baseUrl = `${this.baseUrlss}/api/services/app/CashierSystem/RegenerateSchedule`;
                  const headers = new HttpHeaders({
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*",
                  });
              
                  return this.http.post<any>(baseUrl, payload, { headers: headers });
                }
                // async getListProjectBySchema(memberCode: string): Promise<any> {
                //   const baseUrl = this.baseUrlss + `/api/services/app/Project/GetListProjectBySchema`;
                //   const headers = new HttpHeaders({
                //       "Content-Type": "application/json",
                //       "Access-Control-Allow-Origin": "*",
                //   });
                
                //   try {
                //       // Assuming you are sending curentLoginDataInfo as query parameters
                //       const response = await firstValueFrom(this.http.get<any>(baseUrl, {
                //           headers,
                //           params: {
                //               // Add query parameters here based on curentLoginDataInfo properties
                //               // For example: param1: curentLoginDataInfo.param1,
                //               //              param2: curentLoginDataInfo.param2,
                              
                            
                //               memberCode: memberCode,
                            
                             
                //           },
                //       }));
                
                //       if (response) {
                //           // Set the expiration time in localStorage
                //           // localStorage.setItem('TokenExpiresAt', expirationTime.toString());
                //           console.log(response)
                          
                //           return response;
                //       } else {
                //           console.error('Invalid response format during login:', response);
                //           throw new Error('Invalid response format during login.');
                //       }
                //   } catch (error) {
                //       console.error('An error occurred during login:', error);
                //       throw new Error('An error occurred during login. Please try again.');
                //   }
                // }
                // getDropdownTerm(projectID: number, isAdminBank: boolean): Observable<any> {
                //   const baseUrl = `${this.baseUrlss}/api/services/app/CashierSystem/GetListProjectBySchema`;
                //   const headers = new HttpHeaders({
                //     "Content-Type": "application/json"
                //   });
                  
                //   const params = new HttpParams().set('projectID', projectID).set('isAdminBank', isAdminBank);
                
                //   return this.http.get<any>(baseUrl, { headers, params }).pipe(
                //     catchError(error => {
                //       console.error('An error occurred:', error);
                //       throw new Error('An error occurred. Please try again.');
                //     })
                //   );
                // }
                
                async getDropdownTerm(projectID: number, isAdminBank: boolean): Promise<any> {
                  const baseUrl = this.baseUrlss + `/api/services/app/CashierSystem/GetDropdownTerm`;
                  const headers = new HttpHeaders({
                      "Content-Type": "application/json",
                      "Access-Control-Allow-Origin": "*",
                  });
                
                  try {
                      // Assuming you are sending curentLoginDataInfo as query parameters
                      const response = await firstValueFrom(this.http.get<any>(baseUrl, {
                          headers,
                          params: {
                              // Add query parameters here based on curentLoginDataInfo properties
                              // For example: param1: curentLoginDataInfo.param1,
                              //              param2: curentLoginDataInfo.param2,
                              
                            
                              projectID: projectID,

                              isAdminBank:isAdminBank
                             
                          },
                      }));
                
                      if (response) {
                          // Set the expiration time in localStorage
                          // localStorage.setItem('TokenExpiresAt', expirationTime.toString());
                          console.log(response)
                          
                          return response;
                      } else {
                          console.error('Invalid response format during login:', response);
                          throw new Error('Invalid response format during login.');
                      }
                  } catch (error) {
                      console.error('An error occurred during login:', error);
                      throw new Error('An error occurred during login. Please try again.');
                  }
                }
                async getHistoryViewDocument(bookingHeaderId: number, psCode: string, ppNo: string): Promise<any> {
                  const baseUrl = this.baseUrlss + `/api/services/app/CashierSystem/GetHistoryViewDocument`;
                  const headers = new HttpHeaders({
                      "Content-Type": "application/json",
                      "Access-Control-Allow-Origin": "*",
                  });
                
                  try {
                      // Assuming you are sending curentLoginDataInfo as query parameters
                      const response = await firstValueFrom(this.http.get<any>(baseUrl, {
                          headers,
                          params: {
                              // Add query parameters here based on curentLoginDataInfo properties
                              // For example: param1: curentLoginDataInfo.param1,
                              //              param2: curentLoginDataInfo.param2,
                              
                              bookingHeaderId:bookingHeaderId,
                              psCode: psCode,
                              ppNo:ppNo
                            
                             
                          },
                      }));
                
                      if (response) {
                          // Set the expiration time in localStorage
                          // localStorage.setItem('TokenExpiresAt', expirationTime.toString());
                          console.log(response)
                          
                          return response;
                      } else {
                          console.error('Invalid response format during login:', response);
                          throw new Error('Invalid response format during login.');
                      }
                  } catch (error) {
                      console.error('An error occurred during login:', error);
                      throw new Error('An error occurred during login. Please try again.');
                  }
                }

                async createHistoryViewDocument(input: any): Promise<any> {
                  const baseUrl = this.baseUrlss + `/api/services/app/CashierSystem/CreateHistoryViewDocument`;
                  const headers = new HttpHeaders({
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Origin": "*",
                      });
                    
                      try {
                        const response = await firstValueFrom(this.http.post<test>(baseUrl, input, { headers }));
                    
                        if (response ) {
                          const accessToken = response.result[0].result.accessToken;
                          const UserId = response.result[0].result.userId;
                          const expireInSeconds = response.result[0].result.expireInSeconds;
                    
                          // Save token and expiration time in local storage
                          localStorage.setItem('Token', accessToken);
                          //localStorage.setItem('TokenExpiresAt', (Date.now() + expireInSeconds * 1000).toString());
                          //localStorage.setItem('TokenExpiresAt', (Date.now() + expireInSeconds * 1000).toString());
                          //const expireInSecondss = 2 * 60;
                          const expirationTime = Date.now() + expireInSeconds * 1000;
                
                          // // Set the expiration time in localStorage
                           localStorage.setItem('TokenExpiresAt', expirationTime.toString());
                           
                           localStorage.setItem('UserId', UserId);
                          return response;
                        } else {
                          console.error('Invalid response format during login:', response);
                          throw new Error('Invalid response format during login.');
                        }
                      } catch (error) {
                        console.error('An error occurred during login:', error);
                        throw new Error('An error occurred during login. Please try again.');
                      }
                    }

                  //   async GetNameUser(id:any): Promise<any> {
                  //     const baseUrl= this.baseUrlss + `/api/services/app/CashierSystem/GetNameUser`;
                  //     const headers = new HttpHeaders({
                  //         "Content-Type": "application/json",
                  //         "Access-Control-Allow-Origin": "*",
                  //     });
                  
                  //     try {
                  //         // Assuming you are sending curentLoginDataInfo as query parameters
                  //         const response = await firstValueFrom(this.http.get<any>(baseUrl, {
                  //             headers,
                  //             params: {
                  //                 // Add query parameters here based on curentLoginDataInfo properties
                  //                 // For example: param1: curentLoginDataInfo.param1,
                  //                 //              param2: curentLoginDataInfo.param2,
                  //                 id: id
                  //             },
                  //         }));
                  
                  //         if (response) {
                  //             // Set the expiration time in localStorage
                  //             // localStorage.setItem('TokenExpiresAt', expirationTime.toString());
                  //             localStorage.setItem('name', response.result.name);
                  //             localStorage.setItem('userName', response.result.userName);
                  //             return response;
                  //         } else {
                  //             console.error('Invalid response format during login:', response);
                  //             throw new Error('Invalid response format during login.');
                  //         }
                  //     } catch (error) {
                  //         console.error('An error occurred during login:', error);
                  //         throw new Error('An error occurred during login. Please try again.');
                  //     }
                  // }
                  async GetNameUser(id: any): Promise<any> {
                    const baseUrl = this.baseUrlss + `/api/services/app/CashierSystem/GetNameUser`;
                    const headers = new HttpHeaders({
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Origin": "*",
                    });
                
                    try {
                        const response = await firstValueFrom(this.http.get<any>(baseUrl, {
                            headers,
                            params: {
                                id: id // Assuming 'id' is the correct parameter name for your API call
                            },
                        }));
                
                        if (response && response.result && response.result.name && response.result.userName) {
                            localStorage.setItem('name', response.result.name);
                            localStorage.setItem('userName', response.result.userName);
                            return response;
                        } else {
                            console.error('Invalid response format during GetNameUser:', response);
                            throw new Error('Invalid response format during GetNameUser.');
                        }
                    } catch (error) {
                        console.error('An error occurred during GetNameUser:', error);
                        throw new Error('An error occurred during GetNameUser. Please try again.');
                    }
                }
                async ChangeEmailSalesTracking(changeEmailSalesTracking:any): Promise<any> {
                  const baseUrl= this.baseUrlss + `/api/services/app/CashierSystem/ChangeEmailSalesTracking`;
                  const headers = new HttpHeaders({
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Origin": "*",
                      });
                    
                      try {
                        const response = await firstValueFrom(this.http.post<LoginResponse>(baseUrl, changeEmailSalesTracking, { headers }));
                    
                        if (response ) {
                       
                          console.log(response,'pppppppppppppppppppppppppppppppppppppppppppppppppppppp')
                          return response;
                        } else {
                          console.error('Invalid response format during login:', response);
                          throw new Error('Invalid response format during login.');
                        }
                      } catch (error) {
                        console.error('An error occurred during login:', error);
                        throw new Error('An error occurred during login. Please try again.');
                      }
                    }
                    async getDetailDocument(bookCode: any, docCode: any): Promise<any> {
                      const baseUrl = this.baseUrlss + `/api/services/app/CashierSystem/GetDetailDocument`;
                      const headers = new HttpHeaders({
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Origin": "*",
                      });
                    
                      try {
                        const response = await firstValueFrom(this.http.get<any>(baseUrl, {
                          headers,
                          params: {
                            bookCode: bookCode,
                            docCode: docCode
                          },
                        }));
                    
                        // Check if the response indicates an error
                        if (response && response.error) {
                          Swal.fire({
                            title: 'Error',
                            text: `Asem`,
                            icon: 'error',
                            timer: 3000, // Menutup pesan setelah 3 detik
                          
                            showConfirmButton: true, // Tidak menampilkan tombol OK
                                timerProgressBar: true // Menampilkan animasi timer
                          });
                          console.error('An error occurred:', response.error);
                          throw new Error('An error occurred. Please try again.');
                        }
                    
                        console.log(response);
                        return response;
                      } catch (error) {
                        Swal.fire({
                          title: 'Error',
                          text: `Asem`,
                          icon: 'error',
                          timer: 3000, // Menutup pesan setelah 3 detik
                        
                          showConfirmButton: true, // Tidak menampilkan tombol OK
                              timerProgressBar: true // Menampilkan animasi timer
                        });
                        console.error('An error occurred:', error);
                        throw new Error('An error occurred. Please try again.');
                      }
                    }

                    // async getDetailDocument(bookCode: any, docCode: any): Promise<any> {
                    //   const baseUrl= this.baseUrlss + `/api/services/app/CashierSystem/GetDetailDocument`;
                    //   const headers = new HttpHeaders({
                    //       "Content-Type": "application/json",
                    //       "Access-Control-Allow-Origin": "*",
                    //   });
                    
                    //   try {
                    //       // Assuming you are sending curentLoginDataInfo as query parameters
                    //       const response = await firstValueFrom(this.http.get<any>(baseUrl, {
                    //           headers,
                    //           params: {
                    //               // Add query parameters here based on curentLoginDataInfo properties
                    //               // For example: param1: curentLoginDataInfo.param1,
                    //               //              param2: curentLoginDataInfo.param2,
                                  
                                
                    //               bookCode: bookCode,
                    //               docCode:docCode
                    //           },
                    //       }));
                    
                         
                            
                    //           // Set the expiration time in localStorage
                    //           // localStorage.setItem('TokenExpiresAt', expirationTime.toString());
                    //           console.log(response)
                              
                    //           return response;
                    //       // } else {
                    //       //     console.error('Invalid response format during login:', response);
                    //       //     throw new Error('Invalid response format during login.');
                    //       // }
                    //   } catch (error) {
                    //       console.error('An error occurred during login:', error);
                    //       throw new Error('An error occurred during login. Please try again.');
                    //   }
                    // }
                    async getUnitcodeUnitno(bookCode: any): Promise<any> {
                      const baseUrl= this.baseUrlss + `/api/services/app/CashierSystem/GetUnitcodeUnitno`;
                      const headers = new HttpHeaders({
                          "Content-Type": "application/json",
                          "Access-Control-Allow-Origin": "*",
                      });
                    
                      try {
                          // Assuming you are sending curentLoginDataInfo as query parameters
                          const response = await firstValueFrom(this.http.get<any>(baseUrl, {
                              headers,
                              params: {
                              
                                  
                                
                                  bookCode: bookCode
                              },
                          }));
                    
                          if (response) {
                              // Set the expiration time in localStorage
                              // localStorage.setItem('TokenExpiresAt', expirationTime.toString());
                              console.log(response)
                              
                              return response;
                          } else {
                              console.error('Invalid response format during login:', response);
                              throw new Error('Invalid response format during login.');
                          }
                      } catch (error) {
                          console.error('An error occurred during login:', error);
                          throw new Error('An error occurred during login. Please try again.');
                      }
                    }
                    async getOutstandingDownPayment( bookCode:any): Promise<any> {
                      const baseUrl= this.baseUrlss + `/api/services/app/CashierSystem/getOutstandingDownPayment`;
                      const headers = new HttpHeaders({
                          "Content-Type": "application/json",
                          "Access-Control-Allow-Origin": "*",
                      });
                    
                      try {
                          // Assuming you are sending curentLoginDataInfo as query parameters
                          const response = await firstValueFrom(this.http.get<any>(baseUrl, {
                              headers,
                              params: {
                              
                                  
                                
                                  bookCode: bookCode
                              },
                          }));
                    
                          if (response) {
                              // Set the expiration time in localStorage
                              // localStorage.setItem('TokenExpiresAt', expirationTime.toString());
                              console.log(response)
                              
                              return response;
                          } else {
                              console.error('Invalid response format during login:', response);
                              throw new Error('Invalid response format during login.');
                          }
                      } catch (error) {
                          console.error('An error occurred during login:', error);
                          throw new Error('An error occurred during login. Please try again.');
                      }
                    }
                    // async getUnitcodeUnitnos( bookCode:any): Promise<any> {
                    //   const baseUrl= this.baseUrlss + `/api/services/app/CashierSystem/getOutstandingDownPayment`;
                    //   const headers = new HttpHeaders({
                    //       "Content-Type": "application/json",
                    //       "Access-Control-Allow-Origin": "*",
                    //   });
                    
                    //   try {
                    //       // Assuming you are sending curentLoginDataInfo as query parameters
                    //       const response = await firstValueFrom(this.http.get<any>(baseUrl, {
                    //           headers,
                    //           params: {
                              
                                  
                                
                    //               bookCode: bookCode
                    //           },
                    //       }));
                    
                    //       if (response) {
                    //           // Set the expiration time in localStorage
                    //           // localStorage.setItem('TokenExpiresAt', expirationTime.toString());
                    //           console.log(response)
                              
                    //           return response;
                    //       } else {
                    //           console.error('Invalid response format during login:', response);
                    //           throw new Error('Invalid response format during login.');
                    //       }
                    //   } catch (error) {
                    //       console.error('An error occurred during login:', error);
                    //       throw new Error('An error occurred during login. Please try again.');
                    //   }
                    // }
                    async getListPaymentTypeGateway( projectID: number): Promise<any> {
                      const baseUrl= this.baseUrlss + `/api/services/app/CashierSystem/GetListPaymentTypeGateway`;
                      const headers = new HttpHeaders({
                          "Content-Type": "application/json",
                          "Access-Control-Allow-Origin": "*",
                      });
                    
                      try {
                          // Assuming you are sending curentLoginDataInfo as query parameters
                          const response = await firstValueFrom(this.http.get<any>(baseUrl, {
                              headers,
                              params: {
                              
                                  
                                
                                projectID: projectID
                              },
                          }));
                    
                          if (response) {
                              // Set the expiration time in localStorage
                              // localStorage.setItem('TokenExpiresAt', expirationTime.toString());
                              console.log(response)
                              
                              return response;
                          } else {
                              console.error('Invalid response format during login:', response);
                              throw new Error('Invalid response format during login.');
                          }
                      } catch (error) {
                          console.error('An error occurred during login:', error);
                          throw new Error('An error occurred during login. Please try again.');
                      }
                    }
                    

                    async orderPaymentMidtrans(bookingHeaderID: any, payTypeID: any,amount:any): Promise<any> {
                      const baseUrl= this.baseUrlss + `/api/services/app/CashierSystem/OrderPaymentMidtrans`;
                      const headers = new HttpHeaders({
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Origin": "*",
                          });
                        
                          try {
                            const response = await firstValueFrom(this.http.post<LoginResponse>(baseUrl, {bookingHeaderID: bookingHeaderID,
                            payTypeID: payTypeID,
                            amount: amount}, { headers }));
                        
                            if (response ) {
                           
                              console.log(response,'pppppppppppppppppppppppppppppppppppppppppppppppppppppp')
                              return response;
                            } else {
                              console.error('Invalid response format during login:', response);
                              throw new Error('Invalid response format during login.');
                            }
                          } catch (error) {
                            console.error('An error occurred during login:', error);
                            throw new Error('An error occurred during login. Please try again.');
                          }
                        }
                   async GeneratePDFMyCommission(input: any): Promise<any> {
                          const baseUrl = this.baseUrlss + `/api/services/app/CashierSystem/GeneratePDFMyCommission`;
                          const headers = new HttpHeaders({
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Origin": "*",
                          });
                        
                          try {
                            const response= await firstValueFrom(this.http.post<any>(baseUrl, input, { headers }));
                            return response;
                          } catch (error) {
                            // Lebih baik untuk melemparkan kesalahan ke komponen yang memanggil layanan
                            throw error;
                          }
                      }    
                      async SaveDocument(input: any): Promise<any> {
                        const baseUrl = this.baseUrlss + `/api/services/app/CashierSystem/SaveDocument`;
                        const headers = new HttpHeaders({
                          "Content-Type": "application/json",
                          "Access-Control-Allow-Origin": "*",
                        });
                      
                        try {
                          const response= await firstValueFrom(this.http.post<any>(baseUrl, input, { headers }));
                          return response;
                        } catch (error) {
                          // Lebih baik untuk melemparkan kesalahan ke komponen yang memanggil layanan
                          throw error;
                        }
                    }   
                    
                    async UploadSignedDocument(input: any): Promise<any> {
                      const baseUrl = this.baseUrlss + `/api/services/app/CashierSystem/UploadSignedDocument`;
                      const headers = new HttpHeaders({
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Origin": "*",
                      });
                    
                      try {
                        const response= await firstValueFrom(this.http.post<any>(baseUrl, input, { headers }));
                        return response;
                      } catch (error) {
                        // Lebih baik untuk melemparkan kesalahan ke komponen yang memanggil layanan
                        throw error;
                      }
                  }  

                  async PrintConfirmationLetter(bookCode: string): Promise<any> {
                    //const baseUrl= this.baseUrlss + "/api/services/app/OrderPP/ResendEmail?PPOrderID=${PPOrderID}async resendEmail(PPOrderID: number): Promise<any> {
                      const baseUrl = this.baseUrlss + `/api/services/app/CashierSystem/PrintConfirmationLetter?bookCode=${bookCode}`;
                      const headers = new HttpHeaders({
                          "Content-Type": "application/json",
                          "Access-Control-Allow-Origin": "*",
                      });
                  
                      try {
                          const response = await this.http.post<test>(baseUrl, { bookCode }, { headers }).toPromise();
                  
                          if (response) {
                              return response;
                          } else {
                              console.error('Invalid response format during login:', response);
                              throw new Error('Invalid response format during login.');
                          }
                      } catch (error) {
                          console.error('An error occurred during login:', error);
                          throw new Error('An error occurred during login. Please try again.');
                      }
                  }
            
                  
}


