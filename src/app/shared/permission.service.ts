// permission.service.ts
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PermissionService {
  getPermission(): any {
    // Dapatkan izin dari localStorage atau sumber data lainnya
    const storedPermissions = localStorage.getItem('allPermissions');
    const permissions = storedPermissions ? JSON.parse(storedPermissions) : {};
    return permissions;
  }

  checkPermission(testPermission: string, permissions: any = this.getPermission()): boolean {
    if (permissions && Object.keys(permissions).length > 0) {
      const checking =
        permissions[testPermission] &&
        ['true', true].indexOf(permissions[testPermission]) !== -1;
      return checking;
    }

    return false;
  }
}
