
import { Injectable, inject, signal } from '@angular/core';
import { HttpClient ,HttpHeaders,HttpParams} from '@angular/common/http';
import { firstValueFrom } from 'rxjs';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import {environment} from '../../environments/environment.development'
import * as moment from 'moment';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { lastValueFrom } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SalestrackingService {
  private http = inject(HttpClient);
  private router = inject(Router)
  private baseUrlss=environment.baseUrl;
  constructor() { }
  // fetchData(userLoginData: any): Observable<any> {
  //   const baseUrl= this.baseUrlss + `/api/services/app/CashierSystem/ChangeEmailSalestracking`;
  //   return this.http.get<any>(baseUrl).pipe(
  //     catchError(error => {
  //       console.error('Error fetching data:', error);
  //       return throwError(() => new Error('Something went wrong. Please try again later.'));
  //     })
  //   );
  // }
  // postDataEmailSalestracking(data: any): Observable<any> {
  //   const baseUrl= this.baseUrlss + `/api/services/app/CashierSystem/ChangeEmailSalestracking`;
  //   const headers = new HttpHeaders({
  //     'Content-Type': 'application/json'
  //   });

  //   return this.http.post<any>(baseUrl, data, { headers }).pipe(
  //     catchError(this.handleError)
  //   );
  // }
  async postDataEmailSalestracking(data: any): Promise<any> {
    const baseUrl = this.baseUrlss + `/api/services/app/CashierSystem/ChangeEmailSalestracking`;
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });

    try {
      const response = await lastValueFrom(
        this.http.post<any>(baseUrl, data, { headers }).pipe(
          catchError(this.handleError2)
        )
      );
      return response;
    } catch (error) {
      // Tangani kesalahan di sini
      console.error('Error occurred:', error);
      throw error;
    }
  }

  private handleError2(error: any): Observable<never> {
    // Implementasi penanganan kesalahan
    console.error('An error occurred:', error);
    throw new Error('Error occurred while processing request');
  }

  private handleError(error: any) {
    console.error('An error occurred:', error);
    return throwError(() => new Error('Something went wrong. Please try again later.'));
  }
  async emailChange(userLoginData: any): Promise<any> {

    const baseUrl= this.baseUrlss + `/api/services/app/CashierSystem/ChangeEmailSalestracking`;
    const headers = new HttpHeaders({
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*",
        });
      
       // try {
          const response = await firstValueFrom(this.http.post<any>(baseUrl, userLoginData, { headers }));
      
          if (response && response?.result[0]?.result && response?.result[0]?.result.accessToken) {
            const accessToken = response?.result[0]?.result.accessToken;
            const expireInSeconds = response?.result[0]?.result.expireInSeconds;
            const userId = response?.result[0]?.result.userId;
     
            localStorage.setItem('Token', accessToken);
   
            const expirationTime = Date.now() + expireInSeconds * 1000;

            localStorage.setItem('TokenExpiresAt', expirationTime.toString());
            localStorage.setItem('UserId', userId);
         
            this.router.navigate(['/salestracking']);
            return response;
          } if(response?.result[0]?.errorMessage!=null) {
          Swal.fire({
                  title: `${response?.result[0]?.error.message}`,
                  text: `${response?.result[0]?.error.details}`,
                  icon: 'error',
                  timer: 3000, 
                  
                  showConfirmButton: true, 
          timerProgressBar: true 
                })
          
          }
       
      }
}
