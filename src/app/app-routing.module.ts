import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ComposeEmailComponent } from './components/apps/email/compose-email/compose-email.component';
import { EmailComponent } from './components/apps/email/email.component';
import { ReadEmailComponent } from './components/apps/email/read-email/read-email.component';
//import { FileManagerComponent } from './components/apps/file-manager/file-manager.component';

import { ConfirmMailComponent } from './components/authentication/confirm-mail/confirm-mail.component';
import { ForgotPasswordComponent } from './components/authentication/forgot-password/forgot-password.component';
import { LockScreenComponent } from './components/authentication/lock-screen/lock-screen.component';
import { LoginComponent } from './components/authentication/login/login.component';
import { LogoutComponent } from './components/authentication/logout/logout.component';
import { RegisterComponent } from './components/authentication/register/register.component';
import { ResetPasswordComponent } from './components/authentication/reset-password/reset-password.component';
import { SigninSignupComponent } from './components/authentication/signin-signup/signin-signup.component';

import { InternalErrorComponent } from './components/common/internal-error/internal-error.component';
import { NotFoundComponent } from './components/common/not-found/not-found.component';

import {tokenGuard,logOutGuard } from '../app/shared/auth.guard';
import { PermissionGuard } from '../app/shared/permission.guard';
import {HomeloginComponent} from '../app/components/authentication/homelogin/homelogin.component'
import {SelestrackingComponent} from '../app/components/pages/selestracking/selestracking.component'
import {DetaildocumentComponent} from '../app/components/pages/selestracking/detaildocument/detaildocument.component'
import {HomedashboardComponent} from '../app/components/authentication/homedashboard/homedashboard.component'
import {NotificationComponent} from '../../src/app/components/pages/notification/notification.component';
const routes: Routes = [
    { path: '',redirectTo: '/authentication/login', pathMatch: 'full' },
     {path: 'notification', component: NotificationComponent,canActivate: [logOutGuard,tokenGuard]},
    // {path: 'analytics', component: AnalyticsComponent,canActivate: [logOutGuard,tokenGuard,PermissionGuard],data: { permission: 'Pages' }},
    // {path: 'project-management', component: ProjectManagementComponent,canActivate: [logOutGuard,tokenGuard,PermissionGuard],data: { permission: 'P' }},
    // {path: 'lms-courses', component: LmsCoursesComponent,canActivate: [logOutGuard,tokenGuard]},
    // {path: 'crypto', component: CryptoComponent,canActivate: [logOutGuard,tokenGuard]},
    // {path: 'help-desk', component: HelpDeskComponent,canActivate: [logOutGuard,tokenGuard]},
    // {path: 'saas-app', component: SaasAppComponent,canActivate: [logOutGuard,tokenGuard]},
    // {path: 'chat', component: ChatComponent,canActivate: [logOutGuard,tokenGuard]},
    // {path: 'email', component: EmailComponent,canActivate: [logOutGuard,tokenGuard]},
    // {path: 'email/read', component: ReadEmailComponent,canActivate: [logOutGuard,tokenGuard]},
    // {path: 'email/compose', component: ComposeEmailComponent,canActivate: [logOutGuard,tokenGuard]},
    // {path: 'file-manager', component: FileManagerComponent,canActivate: [logOutGuard,tokenGuard]},
   
    {path: 'error-500', component: InternalErrorComponent},
    {path: 'authentication/forgot-password', component: ForgotPasswordComponent},
    {path: 'authentication/reset-password', component: ResetPasswordComponent},
    {path: 'authentication/login', component: LoginComponent},
    {path: 'authentication/homelogin', component: HomeloginComponent},
    {path: 'authentication/homedashboard', component: HomedashboardComponent,canActivate: [logOutGuard,tokenGuard]},
    {path: 'authentication/register', component: RegisterComponent},
    {path: 'authentication/signin-signup', component: SigninSignupComponent},
    {path: 'authentication/logout', component: LogoutComponent,canActivate: [logOutGuard,tokenGuard]},
    {path: 'authentication/confirm-mail', component: ConfirmMailComponent},
    {path: 'authentication/lock-screen', component: LockScreenComponent,},
    {path: 'salestracking', component:SelestrackingComponent,canActivate: [logOutGuard,tokenGuard]},
    { path: 'detail-document/:psCode/:bookingHeaderID/:documentType/:priorityPassID', component: DetaildocumentComponent }, 
    // Here add new pages component

    {path: '**', component: NotFoundComponent} // This line will remain down from the whole pages component list
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' })],
    exports: [RouterModule]
})
export class AppRoutingModule { }