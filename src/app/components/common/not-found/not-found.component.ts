import { Component } from '@angular/core';
import { AuthService } from '../../../shared/auth.service';
import { OnInit ,inject} from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent {
  public authService = inject(AuthService);
  constructor(private router: Router){
    
  }
}
