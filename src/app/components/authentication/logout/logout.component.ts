import { Component,OnInit ,inject} from '@angular/core';
import { CustomizerSettingsService } from '../../customizer-settings/customizer-settings.service';
import { Router } from '@angular/router';
@Component({
    selector: 'app-logout',
    templateUrl: './logout.component.html',
    styleUrls: ['./logout.component.scss']
})
export class LogoutComponent {
    showSpinner: boolean = false;
    constructor(
        public themeService: CustomizerSettingsService
    ) {}
    public router = inject(Router);

    toggleTheme() {
        this.themeService.toggleTheme();
    }
    logout() {
        this.showSpinner = true;
    
        setTimeout(() => {
            localStorage.clear();
            this.router.navigate(['/authentication/login']);
            this.showSpinner = false;
        }, 1000); // 5000 milliseconds = 5 seconds
    }
    
    toggleCardBorderTheme() {
        this.themeService.toggleCardBorderTheme();
    }

    toggleCardBorderRadiusTheme() {
        this.themeService.toggleCardBorderRadiusTheme();
    }

}