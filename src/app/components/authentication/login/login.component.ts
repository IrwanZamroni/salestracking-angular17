import { Component,OnInit ,inject,AfterViewInit} from '@angular/core';
import { CustomizerSettingsService } from '../../customizer-settings/customizer-settings.service';
import { Router } from '@angular/router';
import { AuthService } from '../../../shared/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit,AfterViewInit {
  backgroundImageUrl: string = 'url("../../../../assets/img/cards/bg-6.jpg")';
  showSpinner: boolean = false;
   inputInfo:any;
   accessTokens:any;
   token:any;
   dataReady: boolean = true;
    hide = true;
    showAlert5 = true;
    public router = inject(Router);
    public authService = inject(AuthService);
    loginForm: FormGroup;
    public fb = inject(FormBuilder);
    constructor(
        public themeService: CustomizerSettingsService
       
    ) {
        this.loginForm = this.fb.group({
            userNameOrEmailAddress: ['', [Validators.required]],
            password: ['', [Validators.required]]
          });
         
         
          
    }
    ngAfterViewInit() {
      // Setelah komponen selesai dirender, atur isLoading menjadi false
      setTimeout(() => {
        this.dataReady = false;
      }, 1000); // Timeout dalam milidetik (5 detik = 5000 milidetik)
    }
    
    async ngOnInit() {
        // const isUserAuthenticated = await this.authService.verifyToken();
    
        // if (isUserAuthenticated) {
        //   this.router.navigate(['/salestracking']);
        //   console.log('Already Logged In.');
        // } else {
        //   this.router.navigate(['/authentication/login']);
        // }
      }
    toggleTheme() {
        this.themeService.toggleTheme();
    }
    async buttonLogin() {
      this.showSpinner = true;
        if (this.loginForm.valid) {
          const userLoginData = this.loginForm.value;
         
            
         
        
            
              const token = await this.authService.login(this.loginForm.value);
             
              this.token=token;
             
             
             // this.dek(2);
           
      
            
           
            if (this.token) {
              
              this.inputInfo={
                accessToken:this.token?.result?.result?.accessToken,
                expireInSeconds:this.token?.result?.result?.expireInSeconds,
                userId:this.token?.result?.result?.userId
              }
             
              this.accessTokens={
                accessToken:this.token?.result?.result?.accessToken
              }
              const inputforpermission =this.accessTokens;
              // const res  = await this.authService.GetNameUser(this.token.result.result.userId); 

              // console.log(res,'klllllllllllllll')
              
               const response=await this.authService.gatewayOfUserPermissions(inputforpermission);
               localStorage.setItem('grantedPermissions', JSON.stringify(response.result.result.auth.grantedPermissions));
              localStorage.setItem('allPermissions', JSON.stringify(response.result.result.auth.allPermissions));
              const send = this.inputInfo;
            
              this.authService.getCurrentLoginInformationsGET(send);
             
              
         
            } else {
             
            }
         
        } else {
     
         }
        this.showSpinner = false;
      }
      async dek(id:any){
        const res  = await this.authService.GetNameUser(id); 

      }
      
    toggleCardBorderTheme() {
        this.themeService.toggleCardBorderTheme();
    }

    toggleCardBorderRadiusTheme() {
        this.themeService.toggleCardBorderRadiusTheme();
    }

    toggleRTLEnabledTheme() {
        this.themeService.toggleRTLEnabledTheme();
    }

}