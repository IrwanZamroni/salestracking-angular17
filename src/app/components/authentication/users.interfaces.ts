export interface UserRegistrationData {
    username: string;
    password: string;
    firstName: string;
    lastName: string;
    phoneNumber: string;
    sex: string;
  }

export interface RegistrationResponse {
    username: string;
  }


export interface LoginData {
    username: string;
    password: string;
  }
  
  export interface CurentLoginDataInfo {
    accessToken: string;
    expireInSeconds: number;
    userId: number;
  }
  export interface PermissionLoginDataInfo {
    accessToken: string;
 
 }
 export interface InqueryLoginDataInfo {
 // accessToken: string;
  
 ///memberCode: any;
 projectID: number[];
//  keyword: any;
//  bookCode: any;
  //termID: any;
 maxResultCount: number;
 skipCount: number;
  
}
export interface LoginResponse{
  //auth_token: string;
  result: {
    accessToken: string;
    encryptedAccessToken: string;
    expireInSeconds: number;
    userId: number;
    userName: string;
    profileImg: string | null;
  };
  targetUrl: string | null;
  success: boolean;
  error: string | null;
  unAuthorizedRequest: boolean;
  __abp: boolean;
}
export interface LoginResponse{
  //auth_token: string;
  result: {
    accessToken: string;
    encryptedAccessToken: string;
    expireInSeconds: number;
    userId: number;
    userName: string;
    profileImg: string | null;
  };
  targetUrl: string | null;
  success: boolean;
  error: string | null;
  unAuthorizedRequest: boolean;
  __abp: boolean;
}
export interface test{
  //auth_token: string;
  result: any;
  targetUrl: string | null;
  success: boolean;
  error: string | null;
  unAuthorizedRequest: boolean;
  __abp: boolean;
}

export interface LoginResponseInformation{
  //auth_token: string;
  result: {
    user: {
      name: string,
      surname: string,
      userName: string,
      emailAddress: string,
      id: number
  },
    tenant: {
        tenancyName: string,
        name: string,
        logoId: any,
        logoFileType: any,
        customCssId: any,
        subscriptionEndDateUtc: any,
        isInTrialPeriod: boolean,
        edition: {
            displayName: string,
            trialDayCount: any,
            monthlyPrice: any,
            annualPrice: any,
            isHighestEdition: true,
            isFree: true,
            id: number
        },
        creationTime: any,
        paymentPeriodType: number,
        subscriptionDateString: string,
        creationTimeString: string,
        id: number
    },
    
},
  
}
export interface UserProfile {
  first_name: string;
  last_name: string;
  phone_number: string;
  sex: string;
  is_farm_owner: boolean;
  is_farm_manager: boolean;
  is_assistant_farm_manager: boolean;
  is_farm_worker: boolean;
  is_team_leader: boolean;
  id: number;
  username: string;
}
