import { Component, inject, ElementRef ,HostListener,ViewChild,AfterViewInit} from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { Router } from '@angular/router';
import { AuthService } from '../../../shared/auth.service';

@Component({
  selector: 'app-homelogin',
  
  templateUrl: './homelogin.component.html',
  styleUrls: ['./homelogin.component.scss'],
  
})
export class HomeloginComponent implements AfterViewInit {
  @ViewChild('myDiv') myDiv: ElementRef;
  public router = inject(Router);
  public authService = inject(AuthService);
  showSpinner: boolean = false;
  images: any
  image: string[]=[]
  //['assets/img/meikarta.jpg', 'assets/img/lippohomeback.jpg', 'assets/img/m-projectInfoLogo_20211018130849.png'];
  //images: string[] = ['image1.jpg', 'image2.jpg', 'image3.jpg'];
  currentIndex: number = 0;
  carouselHeight: number = window.innerHeight;
  carouselWidth: number = window.innerWidth;
  isHovered = false;
  isHovered2 = false;
  displayBanner:boolean=true

  
  constructor() { 
    setTimeout(() => {
      this.displayBanner = false;
    }, 8000); 
  
  }
  ngAfterViewInit() {
    // Setelah tampilan diinisialisasi, Anda dapat melakukan tugas-tugas manipulasi DOM di sini.
    this.onLogin()
  }
  ngOnInit(): void {
    this.startCarousel();
  }
changeBackgroundColor(isHovered: boolean): void {
    this.isHovered = isHovered;
  }
  changeBackgroundColor2(isHovered2: boolean): void {
    this.isHovered2 = isHovered2;
  }
  @HostListener('window:resize', ['$event'])
  onResize(event: Event): void {
    this.carouselHeight = window.innerHeight;
    this.carouselWidth = window.innerWidth;
  }

  startCarousel() {
    setInterval(() => {
      this.nextImage();
    }, 5000);
  }

  nextImage() {
    this.currentIndex = (this.currentIndex + 1) % this.images.length;
  }
  leftImage() {
    this.currentIndex = this.currentIndex - 1 < 0 ? this.images.length - 1 : this.currentIndex - 1;
  }
  async onLogin() {
    this.showSpinner = true;
      // if (this.loginForm.valid) {
      //   const userLoginData = this.loginForm.value;
      try {
        const response = await this.authService.gateBanerFoto();
        this.images = response.result;

        for (const logo of this.images ) {
          this.image.push(logo.productLogo);
      }
      } catch (error) {
        console.error('Error fetching images', error);
      }
        // try {
        //   const response = await this.authService.gateBanerFoto();
       
        //   for (const logo of response.result.productLogo) {
        //     this.images.push(logo);
        // }
          
        //     //this.router.navigate(['dashboard']);
        //     //this.showSuccessToast('Logged in successfully');
        //   } 
        //   // else {
        //   //   //this.handleLoginError('Incorrect login credentials. Please try again.');
        //   // }
        //  catch (error) {
        //   //this.handleLoginError('Login failed. Please try again.');
        // }
      // } else {
      //   //this.showErrorToast('Please fill in all required fields.');
      // }
      this.showSpinner = false;
   
    }
// toggleRTLEnabledTheme() {
//     this.themeService.toggleRTLEnabledTheme();
// }


}
