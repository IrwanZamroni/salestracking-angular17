
import { CustomizerSettingsService } from '../../customizer-settings/customizer-settings.service';
import { MatTableDataSource } from '@angular/material/table';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Component, inject, ElementRef ,HostListener,ViewChild,AfterViewInit,Input, OnInit} from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatMenuTrigger } from '@angular/material/menu';
import { AuthService } from '../../../shared/auth.service';
import { DataService } from '../../../shared/data.service';
import { MatPaginator } from '@angular/material/paginator';
import {FormControl, FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ExampleDialogComponent } from './example-dialog/example-dialog.component';
import { of } from 'rxjs';
import {
  DocumentKPRListDto,
  DocumentKPRInputDto,
  ResendEmailByBookCodeInputDto,
  SendEmailDpDto,
  ResendEmailLayoutPlanDto,

  
} from '../../../shared/auth.service';
import { startWith, map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import Swal from 'sweetalert2';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';

import {ModaldialogcomponentComponent} from './modaldialogcomponent/modaldialogcomponent.component'//CreateHistoryViewDocumentDbo
import { MatSelect } from '@angular/material/select';
export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}
interface SendEmailConfirmationDto {
  bookcode: any,
  emailCustomer:any,
  namaCustomer: any
}
interface userNamedata {
  // Tentukan properti yang akan dikembalikan oleh promise resendKP di sini
  // Misalnya:
  name: any;
  userName: any;
}
interface Image {
  productName: string;
  projectID:number;
  // Jika ada properti lain, Anda bisa menambahkannya di sini
}
 interface CurentLoginDataInfo {
 
  projectID: number[];
   keyword?: any;
  bookCode?: any;
  memberCode?:any;
   termID?: any;
  maxResultCount: number;
  skipCount: number;
}
interface SendPaymentInstructionsDto {
  bookcode: any;
  orderCode: any;
  paymentInstructions: any;
  emailCustomer: any;
  customerName: any;
  memberCode: any;
  amountPay: any;
}
@Component({
  selector: 'app-settings-dialog',
  template: `
    <h2 mat-dialog-title>Settings</h2>
    <div mat-dialog-content>
      <!-- Isi pengaturan disini -->
    </div>
    <div mat-dialog-actions>
      <button mat-button (click)="close()">Close</button>
    </div>
  `,
})
export class SettingsDialogComponent {

  constructor(public dialogRef: MatDialogRef<SettingsDialogComponent>) {}

  close(): void {
    this.dialogRef.close();
  }
}
@Component({
  selector: 'app-modal-dialog',
  template: `
    <div class="p-30">
    <h3 mat-dialog-title>Dialog with elements</h3>
    <p mat-dialog-content>This dialog showcases the title, close, content and actions elements.</p>
    <div *ngFor="let document of documents">
        <!-- Display individual document data here -->
        <p>{{ document  }}</p>
      </div>
    <div mat-dialog-actions>
        <button mat-button mat-dialog-close (click)="closeDialog()" class="border-none fw-semibold ps-30 pe-30 pt-12 pb-12 cursor-pointer">Close</button>
    </div>
</div>
  `,
})
export class ModalDialogComponent {

  constructor(public dialogRef: MatDialogRef<ModalDialogComponent>) {}
  @Input() documents: any[];
  closeDialog(): void {
    this.dialogRef.close();
  }
}
const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
  {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
  {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
  {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
  {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
  {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
  {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
  {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
  {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
  {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
];

@Component({
  selector: 'app-selestracking',

  
  templateUrl: './selestracking.component.html',


  styleUrls: ['./selestracking.component.scss']
})
export class SelestrackingComponent implements OnInit {
  @ViewChild('memberCode') memberCode: ElementRef;
  @ViewChild('bookCode') bookCode: ElementRef;
  @ViewChild('keyword') keyword: ElementRef;
  // @ViewChild('term') term: MatSelect;
  @ViewChild('inputs', { static: false }) inputs!: ElementRef<HTMLInputElement>;
  @ViewChild('asic', { static: false }) asic!: ElementRef<HTMLInputElement>;
  ppno:any;
  isGlobalLoading: boolean = false;
  input = new DocumentKPRInputDto();
  parentMessage:any ;
  filteredOptions: any[]=[];
  //inputHistoryView = new CreateHistoryViewDocumentDbo();
  documents : any[]=[];
  document : any;
  bookingHeaderId: any;
  showSpinner: boolean = false;
  myControl = new FormControl('');
  showSpinners:boolean = true;
  displayedColumns: string[] = ['position', 'name', 'email', 'memberCode','paymentStatus','totalPayment','ppStatus','unitStatus','signP3U','signAkad','pctPaymentDp','document','confirmationlatter','eligibleCommPct'];
  //dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  dataSource: any;
 // displayedColumns: string[] = ['id', 'name', 'description'];
  pageSizeOptions: number[] = [5, 10, 25, 100]; // Pilihan jumlah item per halaman
  pageSize: number = 10; // Default jumlah item per halaman
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  images: any
  pageIndex: number = 0;
  showSpinners2:any=false;
  image: any[] = [];
  option: any[] = [];
  termdropdown:any[] = [];
  myForm: FormGroup;
  selectedData:any;
  selectedData2:any;
  salesId:any=["29"];
  max:any=10;
  responseuinit:any;
  skip:any=0;
  totalItems: number = 0;
  skipCount:any;
  asa:any;
  statecekCl:boolean;
  asa2?:any;
  membercode?:any = undefined;
  bookcode?:any;
  inputMember?:string;
  inputValue?: any;
  inputValue2?:any;
  inputValue3?:any;
  inputValue4?:any;
  hiden:boolean=false;
  displayBanner: boolean = true;
  isColumnLayout:boolean=false;
  existingData:any;
  userName:any;
  respond:any;
  searchTerm: string = '';
  showSearchInput: boolean = false;
  filteredOptionsforTerm:any[] = []; 
  term = new FormControl('');
    selectedProject: any; // Variabel untuk menyimpan proyek yang dipilih
    matSelectIsOpen: boolean = false; // Variabel untuk menunjukkan apakah MatSelect terbuka
  filteredProjects: any[] = []; // Daftar proyek yang difilter
  filteredTerms:any[] = [];
  filteredOptionss: Observable<any[]>;
  myControls = new FormControl('');
  options1: string[] = ['One', 'Two', 'Three'];
  filteredOptionsss: Observable<string[]>;
  isSmall: boolean;
  @ViewChild('select') select: MatSelect;
  @ViewChild('paymentInput') paymentInput: ElementRef;
  //filteredPaymentOptions:any[] = [];
  filteredPaymentOptions: Observable<any[]>;
  public router = inject(Router);
  public authService = inject(AuthService);
  public dataService = inject(DataService);
  bankCtrl = new FormControl();
  bankFilterCtrl = new FormControl();
  bankCtrl2 = new FormControl();
  numberCount:boolean;
  bankFilterCtrl2 = new FormControl();
  filteredBanks: Observable<any[]>;
  filteredBanks2: Observable<any[]>;
  showSpinners3:boolean;
  constructor(
   public themeService: CustomizerSettingsService,private fb: FormBuilder,public dialog: MatDialog,private breakpointObserver: BreakpointObserver
) {
  // this.myForm = this.fb.group({
  //   selectedImage: [''] // Anda dapat menentukan nilai awal di sini
  // });
  // const skipCount = this.paginator.pageIndex * this.paginator.pageSize??0;
  // const maxResults = this.paginator.pageSize;

//console.log(this.inputValue) 
this.filteredOptionsss = this.myControl.valueChanges.pipe(
  startWith(''),
  map(value => this._filter(value || '')),
);
;

(async () => {
  ;
  // try {
    this.showSpinners3=true
      const id = localStorage.getItem('UserId'); // Ganti dengan id yang sesuai
      const res = await this.authService.GetNameUser(id);
      const ad =this.getData(res.result.userName);
    
      console.log(ad,'asemmmmm')
      this.parentMessage=res.result.userName;
  
    
    //  this.userName = ;
  // } catch (error) {
  //     console.error("Terjadi kesalahan:", error);
  // }
})();


this.isSmall = window.innerWidth <= 700;


}
private _filter(value: string): string[] {
  const filterValue = value.toLowerCase();

  return this.options1.filter(option => option.toLowerCase().includes(filterValue));
}

// onOptionSelected(event: any): void {
//   const selectedProjectId = event.option.value;
//   console.log(selectedProjectId); // atau lakukan apa pun yang Anda butuhkan dengan selectedProjectId
// }

onOptionSelected(event: any): void {
  const selectedName = event.option.value;
  let selectedProjectId = null;

  // Misalkan this.image adalah array yang berisi objek-objek projek
  const projects = this.image; // Ubah sesuai dengan nama variabel yang sesuai di programmu

  // Iterasi melalui setiap objek projek
  for (let i = 0; i < projects.length; i++) {
      const project = projects[i];
      // Periksa apakah projectName proyek sama dengan selectedName
      if (project.projectName === selectedName) {
          // Jika iya, simpan ID proyek tersebut dan hentikan iterasi
          selectedProjectId = project.projectID;
          break;
      }
  }

  // Lakukan apa pun yang perlu dilakukan dengan selectedProjectId di sini
 
  // atau lakukan apa pun yang Anda butuhkan dengan selectedProjectId
}
ngOnInit(): void {

 
  

  this.onLogin(this.asa?.value);
}

filter1(): void {
  // Memastikan bahwa this.input telah diinisialisasi
  const filterValue = this.inputs.nativeElement.value.toLowerCase();
  this.filteredOptions = this.image.filter((o:any) => o.projectName.toLowerCase().includes(filterValue));
 
  
}
handleClick() {
  // Lakukan logika yang ingin Anda jalankan ketika bidang diklik di sini
  
 this.filteredOptions=this.image;
}


// filterPayments() {
//   let searchValue = this.paymentInput.nativeElement.value.toLowerCase();

//   this.filteredPaymentOptions = this.termdropdown.filter(option =>
//     option.termRemarks.toLowerCase().includes(searchValue)
//   );
// }
private _filterBanks(value: string): any[] {
  const filterValue = value.toLowerCase();
  return this.termdropdown.filter(bank => bank.termRemarks.toLowerCase().includes(filterValue));
}
private _filterBanks2(value: string): any[]{
  const filterValue = value.toLowerCase();
  return this.image.filter(bank => bank.projectName.toLowerCase().includes(filterValue));
}
// handleClick1(){
//   this.filteredPaymentOptions=this.termdropdown;
// }
filterTerms(): void {
 // const filterValue = value.toLowerCase();
 // this.filteredTerms = this.termdropdown.filter(term => term.termRemarks.toLowerCase().includes(filterValue));
//  const filterValue = value.toLowerCase();
//     if (filterValue.trim() === '') {
      // If search input is empty, display all terms
      // this.filteredTerms = this.termdropdown;
      const filterValue = this.asic.nativeElement.value.toLowerCase();
      this.filteredTerms = this.termdropdown.filter((s:any) => s.termRemarks.toLowerCase().includes(filterValue));
    console.log(this.filteredTerms,'hasillllllllllllll')
    // } else {
    //   // Otherwise, filter the terms based on the search input
    //   this.filteredTerms = this.termdropdown.filter(term => term.termRemarks.toLowerCase().includes(filterValue));
    // }
}
filter2(): void {
  // Memastikan bahwa this.input telah diinisialisasi
  const filterValue = this.asic.nativeElement.value.toLowerCase();
  this.filteredOptionsforTerm = this.termdropdown.filter((o:any) => o.termRemarks.toLowerCase().includes(filterValue));
console.log(this.filteredOptionsforTerm,'hasillllllllllllll')
}
filterProjects() {
  // Jika field pencarian kosong, tampilkan semua proyek
  if (!this.searchTerm.trim()||this.searchTerm==null|| this.searchTerm==undefined) {
    this.filteredProjects = this.image;
   
  }

  // Jika terdapat kata kunci pencarian, filter proyek sesuai dengan kata kunci tersebut
  this.filteredProjects = this.image.filter((project:any) =>
    project.projectName.toLowerCase().includes(this.searchTerm.toLowerCase())
  );
}
async dek(id:any){
  const res  = await this.authService.GetNameUser(id); 
 

 
}

@HostListener('window:resize', ['$event'])
  onResize(event:any) {
    this.isSmall = event.target.innerWidth <= 700;
  }
  isSmallScreen(): boolean {
    return this.isSmall;
  }
hide(){
  this.hiden=!this.hiden
}

//   onInputChange(value?: any) {
//     console.log('Input value changed:', value);
//     // Lakukan sesuatu dengan nilai yang berubah
//   }
// onMemberCodeChange(event: any) {
//   const inputValue = event.target.value;
//   console.log('Nilai Membercode yang baru:', inputValue);
//   // Lakukan sesuatu dengan nilai yang telah diubah
// }

onMemberCodeChange(event: Event) {
  const inputValue = (event.target as HTMLInputElement).value;
  this.membercode=inputValue
  console.log('Nilai Membercode yang baru:', inputValue);
  // Lakukan sesuatu dengan nilai yang telah diubah
}
onBokcodeChange(event:Event){
  const inputValue = (event.target as HTMLInputElement).value;
  this.bookcode=inputValue
  console.log('Nilai Membercode yang baru:', inputValue);
}
onMemberCodeChange2(event: Event) {
  const inputValue = (event.target as HTMLInputElement).value;
  this.inputValue2=inputValue
  console.log('Nilai Membercode yang baru:', inputValue);
  // Lakukan sesuatu dengan nilai yang telah diubah
}
onMemberCodeChange3(event: Event){
  const inputValue = (event.target as HTMLInputElement).value;
  this.inputValue4=inputValue
  console.log('Nilai Membercode yang baru:', inputValue);
}
ditekan(){

  // this.inputValue
  // this.inputValue2;
  // this.inputValue3;
  // this.inputValue4;
  this.membercode = this.memberCode ? (this.memberCode.nativeElement.value === '' ? undefined : this.memberCode.nativeElement.value) : undefined;
  this.bookcode = this.bookCode ? (this.bookCode.nativeElement.value === '' ? undefined : this.bookCode.nativeElement.value) : undefined;
  this.inputValue2 = this.keyword ? (this.keyword.nativeElement.value === '' ? undefined : this.keyword.nativeElement.value) : undefined;
   this.asa2;
  this.membercode;
  
  this.bookcode;
  this.inputValue2;



  this.skipCount=0;
  this.paginator.pageIndex=0;
  this.onLogin(this.asa)
}
openDialog(_psCode:any, _bookingHeaderID:any,_ppNO:any,_prioritypassId:any): void {
 
  console.log(_bookingHeaderID,'hhhhhhhhhh')

  if(_bookingHeaderID){
    this.ppno=_ppNO;
    this.input.psCode = _psCode;
    this.input.bookingHeaderID = _bookingHeaderID;
    this.bookingHeaderId = _bookingHeaderID;
   // this.getListDocucmentKPR(_psCode, _bookingHeaderID);
    //this.modal.show();
  }
    else{
      this.ppno=_ppNO;
      this.input.psCode = _psCode;
      this.input.bookingHeaderID = _bookingHeaderID!=null? _bookingHeaderID:0;
      this.bookingHeaderId = _bookingHeaderID!=null? _bookingHeaderID:0;
     // this.getListDocucmentKPR(_psCode, this.bookingHeaderId);
      //this.modal.show();
  }
  console.log( this.ppno,'ppnnooooooooo')
  localStorage.setItem("ppno", this.ppno);
  const dialogRef = this.dialog.open(ModaldialogcomponentComponent, {
    width: '550px', // atur lebar modal
    height:'500px',
    // data: this.documents,
    // bookingId:_bookingHeaderID
    data: {
      prioritypassId:_prioritypassId,
      doc: this.documents,
      bookingId:   this.bookingHeaderId ,
      SelestrackingComponent: this,
      psCode: this.input.psCode ,
      ppno:this.ppno,
      userId: localStorage.getItem('UserId'),
      name_button:'Uhuy'
    }
  });

  dialogRef.afterClosed().subscribe(result => {
    console.log('The dialog was closed');
  });

  
}
openBuyPP(_name:any,_ppOrderID:any,_projectCode:any){
//   this._router.navigate(["app/pponline/transactionpp"], {
//     queryParams: {
//         customerName: _name,
//         ppOrderID: _ppOrderID,
//         projectCode: _projectCode,
//     },
// });
}
async getListView(_bookingHeaderID: any, _psCode: any,_ppNO:any) {

  this.showSpinner=true
 
 
   try {
    
     const response = await this.authService.getHistoryViewDocument(_bookingHeaderID,_psCode, _ppNO);
     if(!response.success){
      Swal.fire({
      title: 'Error',
      text: 'An internal error occurred during your request! ',
      icon: 'error',
      timer: 3000, 
    
      showConfirmButton: true, 
          timerProgressBar: true 
    });
}
     console.log(response,'pppppp')
  
     console.log(this.documents)
     
   } catch (error) {
    Swal.fire(
      'Error!',
  'An internal error occurred during your request! ' ,
  'error'
    );
  }
   
   this.showSpinner=false
//    emailModal(email:any,psCode:any):any {
   
//   }
// }
 }
 emailModal(email:any,psCode:any){
  debugger;
  // const dialogRef = this.dialog.open(ExampleDialogComponent, {
  //   width: '250px',
  //   data: { name: 'Angular' }
  // });

  // dialogRef.afterClosed().subscribe(result => {
  //   console.log('The dialog was closed');
  // });
  const dialogRef = this.dialog.open(ExampleDialogComponent, {
    width: '550px', // atur lebar modal
    height:'500px',
   
    data: {
  
      email:email,
      psCode:psCode,
      name_tombol:'emailModal',
    }
  });

  // dialogRef.afterClosed().subscribe(result => {
  //   console.log('The dialog was closed');
  // });
  dialogRef.afterClosed().subscribe(result => {
  
    if (result && result.saved) {
      this.onLogin(this.asa);
    }
  });
 }
historyViewDocumentModal(_a:any,_b:any,_c:any){

  const dialogRef = this.dialog.open(ModaldialogcomponentComponent, {
    width: '550px', // atur lebar modal
    height:'300px',
   
    data: {
      doc: this.documents,
      bookingId: _a ,
      SelestrackingComponent: this,
      psCode:_b,
      ppno:_c,
      name_button:'historyViewDocumentModal'
    }
  });

  dialogRef.afterClosed().subscribe(result => {
    console.log('The dialog was closed');
  });
}
// resendPPEmail(_name:any,_ppOrderID:any){
//   this.showSpinner=true;
//   Swal.fire({
//     title: "Are you sure to Resend Confirmation PP/BF to Customer?",
//         text: "If agreed, the system will send the Confirmation PP/BF back to '" +
//         _name +
//         "' email as the related customer.",
  
//     icon: 'info',
//     showCancelButton: true,
//     confirmButtonColor: '#3085d6',
//     cancelButtonColor: '#d33',
//     reverseButtons: true,
//     confirmButtonText: 'OK',
//     cancelButtonText: 'Cancel'
//   }).then(async (result) => {
//     if (result.isConfirmed) {
    
   
//       this.authService.resendEmail(_ppOrderID)
//       .then((response) => {
//         if(response.result.status=="Failed"){
//           console.log('Email sent successfully!',response);
          
//                     Swal.fire(
//                       `${response.result.status}`,
//                   `${response.result.errorMessage}` ,
//                   'error'
//                     );
//                     this.showSpinner=false;
//         }else{
//           Swal.fire(
//             ' Resend Confirmation PP/BF',
//             '',
//             'success'
//           );
//           this.showSpinner=false;
//         }
//       })
//       .catch(error => {
//         console.log(error)
//         Swal.fire(
//           'Error!',
//       'An internal error occurred during your request! ' ,
//       'error'
//         );
//       });
      
//    }
//    else {
//     // Jika pengguna membatalkan dialog, atur showSpinner menjadi false di sini
//     this.showSpinner = false;
//   }
   
//   })
 
// }
resendPPEmail(_name: any, _ppOrderID: any) {
  this.showSpinner = true;
  Swal.fire({
    title: "Are you sure to Resend Confirmation PP/BF to Customer?",
    text: `If agreed, the system will send the Confirmation PP/BF back to '${_name}' email as the related customer.`,
    icon: 'info',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    reverseButtons: true,
    confirmButtonText: 'OK',
    cancelButtonText: 'Cancel'
  }).then(async (result) => {
    if (result.isConfirmed) {
      try {
        const response = await this.authService.resendEmail(_ppOrderID);

        if (response.result.status === "Failed") {
          console.log('Email sending failed!', response);
          Swal.fire(
            `${response.result.status}`,
            `${response.result.errorMessage}`,
            'error'
          );
        } else {
          Swal.fire(
            'Resend Confirmation PP/BF Successfully',
            '',
            'success'
          );
        }
      } catch (error) {
        console.error('An error occurred:', error);
        Swal.fire(
          'Error',
          'An internal error occurred during your request!',
          'error'
        );
      } finally {
        this.showSpinner = false;
      }
    } else {
      this.showSpinner = false;
    }
  });
}

// resendLayoutPlan(_projectID:any,_email:any,_name:any,_memberCode:any){
//   this.showSpinner=true;
//   Swal.fire({
//     title: "Are you sure to Resend Layout Plan to Customer?",
//         text: "If agreed, the system will send the Layout Plan back'" +
//         _name +
//         "' email as the related customer.",
  
//     icon: 'info',
//     showCancelButton: true,
//     confirmButtonColor: '#3085d6',
//     cancelButtonColor: '#d33',
//     reverseButtons: true,
//     confirmButtonText: 'OK',
//     cancelButtonText: 'Cancel'
//   }).then(async (result) => {
//     if (result.isConfirmed) {
//       this.showSpinner=true;
//       this.authService.resendLayoutPlan(
//         new ResendEmailLayoutPlanDto({
//             projectID: _projectID,
//             emailCustomer: _email,
//             customerName: _name,
//             memberCode:
//                 _memberCode != ""
//                     ? _memberCode
//                     : 'admin',
//         })
//     )
//     .then((response) => {
//       if(response.result.status=="Failed"){
//         console.log('Email sent successfully!',response);
        
//                   Swal.fire(
//                     `${response.result.status}`,
//                 `${response.result.errorMessage}` ,
//                 'error'
//                   );
//                   this.showSpinner=false;
//       }else{
//         Swal.fire(
//           'Resend Layout Plan Successfully',
//           '',
//           'success'
//         );
//         this.showSpinner=false;
//       }

     
//     })
//       .catch(error => {
//         console.log(error)
//         Swal.fire(
//           'Error!',
//       'An internal error occurred during your request! ' ,
//       'error'
//         );
        
//       });
//       this.showSpinner=false;
//    }else{
//     this.showSpinner=false;
//    }
//   })
// }
resendLayoutPlan(_projectID: any, _email: any, _name: any, _memberCode: any) {
  this.showSpinner = true;
  Swal.fire({
    title: "Are you sure to Resend Layout Plan to Customer?",
    text: `If agreed, the system will send the Layout Plan back to '${_name}' email as the related customer.`,
    icon: 'info',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    reverseButtons: true,
    confirmButtonText: 'OK',
    cancelButtonText: 'Cancel'
  }).then(async (result) => {
    if (result.isConfirmed) {
      try {
        const response = await this.authService.resendLayoutPlan(
          new ResendEmailLayoutPlanDto({
            projectID: _projectID,
            emailCustomer: _email,
            customerName: _name,
            memberCode: _memberCode !== "" ? _memberCode : 'admin',
          })
        );

        if (response.result.status === "Failed") {
          console.log('Email sending failed!', response);
          Swal.fire(
            `${response.result.status}`,
            `${response.result.errorMessage}`,
            'error'
          );
        } else {
          Swal.fire(
            'Resend Layout Plan Successfully',
            '',
            'success'
          );
        }
      } catch (error) {
        console.error('An error occurred:', error);
        Swal.fire(
          'Error',
          'An internal error occurred during your request!',
          'error'
        );
      } finally {
        this.showSpinner = false;
      }
    } else {
      this.showSpinner = false;
    }
  });
}

// resendKP(_name:any,_unitOrderHeaderId:any){
//   debugger;
//   this.showSpinner=true;
//   Swal.fire({
//     title: "Are you sure to Resend KP to Customer?",
//     text: "If agreed, the system will send the KP back to '" +
//     _name +
//     "' email as the related customer.",
  
//     icon: 'info',
//     showCancelButton: true,
//     confirmButtonColor: '#3085d6',
//     cancelButtonColor: '#d33',
//     reverseButtons: true,
//     confirmButtonText: 'OK',
//     cancelButtonText: 'Cancel'
//   }).then(async (result) => {
   
//     if (result.isConfirmed) {
//       this.showSpinner=true;
//       const response= await this.authService.resendKP(_unitOrderHeaderId)
//       // .then((response) => {
    
//         if(response.result.status=="Failed"){
//           console.log('Email sent successfully!',response);
          
//                     Swal.fire(
//                       `${response.result.status}`,
//                   `${response.result.errorMessage}` ,
//                   'error'
//                     );
//                     this.showSpinner=false;
//         }else{
//           Swal.fire(
//             'Resend KP Successfully',
//             '',
//             'success'
//           );
//           this.showSpinner=false;
//         }

       
//        })

// }
resendKP(_name: any, _unitOrderHeaderId: any) {
  this.showSpinner = true;
  Swal.fire({
    title: "Are you sure to Resend KP to Customer?",
    text: `If agreed, the system will send the KP back to '${_name}' email as the related customer.`,
    icon: 'info',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    reverseButtons: true,
    confirmButtonText: 'OK',
    cancelButtonText: 'Cancel'
  }).then(async (result) => {
    if (result.isConfirmed) {
      try {
        const response = await this.authService.resendKP(_unitOrderHeaderId);

        if (response.result.status === "Failed") {
          console.log('Email sending failed!', response);
          Swal.fire(
            `${response.result.status}`,
            `${response.result.errorMessage}`,
            'error'
          );
        } else {
          Swal.fire(
            'Resend KP Successfully',
            '',
            'success'
          );
        }
      } catch (error) {
        console.error('An error occurred:', error);
        Swal.fire(
          'Error',
          'An unexpected error occurred. Please try again later.',
          'error'
        );
      } finally {
        this.showSpinner = false;
      }
    } else {
      this.showSpinner = false;
    }
  });
}

async openModalUploadOL(_bookCode:any, _docCode:any, _docName:any, _bankName?:any){
debugger;
this.showSpinner=true;
const asd=await this.getDetailDocument(_bookCode,_docCode);
this.showSpinner=false;

console.log(asd,'lkjklll')
const dialogRef = this.dialog.open(ModaldialogcomponentComponent, {
width: '750px', // atur lebar modal
height:'500px',

data: {
doc: this.documents,
bookingId: this.bookingHeaderId ,
SelestrackingComponent: this,
psCode:this.input.psCode,
ppno:this.ppno,
docNo:asd.docNo,
bookingDocumentID:asd.bookingDocumentID,
name_button:'UploadOl'
}
});

dialogRef.afterClosed().subscribe(result => {
console.log('The dialog was closed');
});
}
openBookingUnit(_projectID:any,_clusterID:any){
  window.open('https://booking.lippo.homes/app/notifications', "_blank");
}
changePaymentTermModal(unitID:any,bankName:any,bankCode:any, _projectId:any,_bookCode:any,_bankCode:any,_unitId:any,_unitOrder:any,_termRemark:any,_bookingHeaderId:any){

 
  const dialogRef = this.dialog.open(ModaldialogcomponentComponent, {
    width: '750px', // atur lebar modal
    height:'500px',
  
    data: {
      unitID:unitID,
      bankName:bankName,

      projectId:_projectId,
      bankCode:_bankCode,
      bookCode:_bookCode,
      unitId: _unitId,
      unitOrder: _unitOrder,
      termRemark: _termRemark,
      
      bookingHeaderId:_bookingHeaderId,
      
      name_button:'Changepaymentterm'
    }
    });
    
    dialogRef.afterClosed().subscribe(result => {
     
    console.log('The dialog was closed');
    });
    // dialogRef.afterClosed().subscribe(result => {
  
    //   if (result && result.saved) {
    //      this.ditekan()
    //   }
    // });
}
async resendEmailDoc(_memberCode:any,_bookCode:any,_ppu:any,_pppu:any,_email:any,_name:any,_bol:any){
  debugger
  this.showSpinner=true;
  let ifSigned = _bol ? " Signed " : " ";
  Swal.fire({
    title: "Are you sure to Resend" + ifSigned + _pppu + " to Customer?",
    text: "If agreed, the system will send the" +
    ifSigned +
    _pppu +
    " back to '" +
    _name +
    "' email as the related customer.",
  
    icon: 'info',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    reverseButtons: true,
    confirmButtonText: 'OK',
    cancelButtonText: 'Cancel'
    
  }).then(async (result) => {
    if (result.isConfirmed) {
      // this.showSpinner=true;
      const response = await this.authService.resendEmailDoc(new ResendEmailByBookCodeInputDto({
        bookCode: _bookCode,
        emailCustomer: _email,
        customerName: _name,
        memberCode:
         _memberCode != ""
                ? _memberCode
                : 'admin',
        docCode: _ppu,
        isSigned: _bol,
    }
  
  
  ))
  if(response.result.status=="Failed"){
     
        
    Swal.fire(
      `${response.result.status}`,
  `${response.result.errorMessage}` ,
  'error'
    );
    this.showSpinner=false;

    }else{
    Swal.fire(
    'Resend Resend P3U Successfully',
    '',
    'success'
    );
    this.showSpinner=false;
    }
    // .then((response) => {
    //   if(response.result.status=="Failed"){
     
        
    //               Swal.fire(
    //                 `${response.result.status}`,
    //             `${response.result.errorMessage}` ,
    //             'error'
    //               );
    //               this.showSpinner=false;
              
    //   }else{
    //     Swal.fire(
    //       'Resend Resend P3U Successfully',
    //       '',
    //       'success'
    //     );
    //     this.showSpinner=false;
    //   }

     
    // })
      // .catch(error => {
      //   console.log(error)
      //   Swal.fire(
      //     'Error!',
      // 'An internal error occurred during your request! ' ,
      // 'error'
      //   );
      // });
      // this.showSpinner=false;
   }
  })
  // this.showSpinner=false;
}
uploadConfirmationLatter(){

}
printP3U(_bookCode:any){
  ;
  Swal.fire({
    title: "Are you sure to Print P3U/PPPU?",
    text: "If agreed, the system will print the P3U/PPPU Document.",
    icon: 'info',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    reverseButtons: true,
    confirmButtonText: 'OK',
    cancelButtonText: 'Cancel'
  }).then(async (result) => {
    if (result.isConfirmed) {
      ;
      try {
        this.showSpinner=true
        const response = await this.authService.printPPPU(_bookCode);
        
        if (response.result.status === "Failed") {
           
            
            Swal.fire(
                `${response.result.status}`,
                `${response.result.errorMessage}`,
                'error'
            );
            this.showSpinner=false;
        } else {
         
          Swal.fire(
            'Print P3U/PPPU Successfully',
            '',
            'success'
        );
            const filePDF = response.result.result.filePDF;
            this.onLogin(this.asa.value);
            window.open(filePDF, "_blank");
            this.showSpinner=false;
        }
    } catch (error) {
        console.log(error);
        
        Swal.fire(
          'Error!',
      'An internal error occurred during your request! ' ,
      'error'
        );
        this.showSpinner=false;
    }
    }
  });

}
resendDPReceipt(_bookCode:any,_email:any,_name:any){
  this.showSpinner=true;
  Swal.fire({
    title: "Are you sure to Resend DP Receipt to Customer?",
    text: "If agreed, the system will send the DP Receipt back to '" +
    _name +
    "' email as the related customer.",
  
    icon: 'info',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    reverseButtons: true,
    confirmButtonText: 'OK',
    cancelButtonText: 'Cancel'
  }).then(async (result) => {
    this.showSpinner=true;
    if (result.isConfirmed) {
      
      this.authService.sendEmailDP(new SendEmailDpDto({
        bookcode: _bookCode,
        emailCustomer: _email,
        namaCustomer: _name,
    }))
      .then(() => {
        console.log('Email sent successfully!');
        Swal.fire(
                    'Succes to send resend file',
                    '',
                    'success'
                  );
                  this.showSpinner=false;
      })
      .catch(error => {
        console.log(error)
        Swal.fire(
          'Error!',
      'An internal error occurred during your request! ' ,
      'error'
        );
        this.showSpinner=false;
      });
   }
  })
}
sendPaymentInstruction( _bookCode:any,
  _pmtInstruction:any,
  _pmtInstructionName:any,
  _email:any,
  _name:any,
  _memberCode:any,
  _orderCode?:any,
  _amountPay?:any){
    ;
   this.showSpinner=true;
    const as: SendPaymentInstructionsDto = {
      bookcode: _bookCode,
      orderCode: _orderCode,
      paymentInstructions: _pmtInstruction,
      emailCustomer: _email,
      customerName: _name,
      memberCode:
          _memberCode != undefined
              ? _memberCode
              : 'admin',
      amountPay: _amountPay,
    };
    // this.showSpinner=true;
    Swal.fire({
      title: "Are you sure to Send Payment Instruction?",
      text: "If agreed, the system will send the Payment Instruction" +
      _pmtInstructionName +
      " back to '" +
      _name +
      "' email as the related customer.",
    
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      reverseButtons: true,
      confirmButtonText: 'OK',
      cancelButtonText: 'Cancel'
    }).then(async (result) => {
      if (result.isConfirmed) {
        this.showSpinner=true;
        this.authService.sendPaymentInstructions(as)
        .then((response) => {
          if(response.result.status=="Failed"){
            console.log('Email sent successfully!',response);
            this.showSpinner=true;
                      Swal.fire(
                        `${response.result.status}`,
                    `${response.result.errorMessage}` ,
                    'error'
                      );
                      this.showSpinner=false;
          }else{
            this.showSpinner=true;
            Swal.fire(
              'Send Payment Instruction Successfully',
              '',
              'success'
            );
            this.showSpinner=false;
          }
        })
        .catch(error => {
          this.showSpinner=true;
          console.log(error)
          Swal.fire(
            'Error!',
        'An internal error occurred during your request! ' ,
        'error'
          );
        });
        this.showSpinner=false;
     }else{
      // this.showSpinner=false;
     }
     
    })
     this.showSpinner=true;
}


openSettings(): void {
  this.dialog.open(SettingsDialogComponent, {
    width: '250px',
  });
}
statusDP(_status:any, _statusDone:any):any {
  if (_status != undefined) {
      if (_status.includes("%")) {
          _status = _status.split(" ")[0];

          if (_status === _statusDone) return true;
      }
  }
}
bankTermValidation(_termRemarks?:any, _bankName?:any, _unit?:any) {
  if (_termRemarks != undefined && _bankName != undefined) {
      let checkKpNobuIfExist = !(
          _termRemarks.toLowerCase().includes("kp") &&
          _bankName.toLowerCase().includes("nobu")
      );
      return checkKpNobuIfExist;
  } else {
      return true;
  }
}

async uploadSignatureModal(
  _bookCode:any, _docCode:any, _docName:any){
    

    //  try {
    
      const response = await this.authService.getDetailDocument(_bookCode, _docCode);
      
      const responseUnit = await this.authService.getUnitcodeUnitno(_bookCode)
      this.responseuinit=responseUnit.result;
      this.respond= response.result;
      console.log( this.respond,'alkjhg')
      const dialogRef = this.dialog.open(ModaldialogcomponentComponent, {
        width: '750px', // atur lebar modal
        height:'500px',
     
        data: {
          unitCode:this.responseuinit.unitCode,
          unitNo:this.responseuinit.unitNo,
          bookCode:_bookCode,
          docNo: this.respond?.docNo,
          customerName: this.respond?.customerName ,
          bookingDocumentID: this.respond?.bookingDocumentID,
          docId:this.respond?.docId,
          docCode:this.respond?.docCode,
          docName:this.respond?.docName,
          psCode:this.respond?.psCode,
        
          isAmdt:this.respond?.isAmdt,
          apiSima:this.respond?.apiSima,
          name_button:'Upload'
        }
        });
        
        dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed');
        });

      console.log(response,'pppppp')
 
      console.log(this.documents)
     


}

async uploadSignatureModalConfirmlatter(
  _bookCode:any, _docCode:any, _docName:any){
    

    //  try {
      this.showSpinner=true;
      const response = await this.authService.getDetailDocument(_bookCode, _docCode);
      this.statecekCl= false;
      if(response.result.errorMessage!=null){
        this.statecekCl=true
        Swal.fire(
          `${response.result.status}`,
          `${response.result.errorMessage}`,
          'error'
        );
        
      }
    
      const responseUnit = await this.authService.getUnitcodeUnitno(_bookCode)
      this.responseuinit=responseUnit.result;
      this.respond= response.result;
      console.log( this.respond,'alkjhg')
      this.showSpinner=false;
      if(this.statecekCl===false){
        const dialogRef = this.dialog.open(ExampleDialogComponent, {
          width: '750px', // atur lebar modal
          height:'500px',
       
          data: {
            unitCode:this.responseuinit.unitCode,
            unitNo:this.responseuinit.unitNo,
            bookCode:_bookCode,
            docNo: this.respond?.docNo,
            customerName: this.respond?.customerName ,
            bookingDocumentID: this.respond?.bookingDocumentID,
            docId:this.respond?.docId,
            docCode:this.respond?.docCode,
            docName:this.respond?.docName,
            psCode:this.respond?.psCode,
            statecekCl:this.statecekCl,
            isAmdt:this.respond?.isAmdt,
            apiSima:this.respond?.apiSima,
         
            name_tombol:'UploadConfirm'
  
          }
          });
          
          dialogRef.afterClosed().subscribe(result => {
          console.log('The dialog was closed');
          });
          dialogRef.afterClosed().subscribe(result => {
    
            if (result && result.saved) {
              this.onLogin(this.asa);
            }
          });
      }
      

      console.log(response,'pppppp')
 
      console.log(this.documents)
     


}
async printConfirmationLatter(bookCode:any){
  Swal.fire({
    title: "Are you sure to Print Confirmation Letter?",
    text: "If agreed, the system will print the Confirmation Letter Document.",
  
    icon: 'info',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    reverseButtons: true,
    confirmButtonText: 'OK',
    cancelButtonText: 'Cancel'
  }).then(async (result) => {
    if (result.isConfirmed) {
      this.showSpinner=true;
      const response = await this.authService.PrintConfirmationLetter(bookCode);
      if (response.result.status === "Failed") {
           
            
        Swal.fire(
            `${response.result.status}`,
            `${response.result.errorMessage}`,
            'error'
        );
        this.showSpinner=false;
    } else {
     
      Swal.fire(
        'Print Confirmation Latter',
        '',
        'success'
    );
        const filePDF = response.result.result.filePDF;
        this.onLogin(this.asa.value);
        window.open(filePDF, "_blank");
        this.showSpinner=false;
    }
    //   if(response.result.status=="Failed"){
    //     Swal.fire({
    //     title: 'Error',
    //     text: `${response.result.errorMessage}`,
    //     icon: 'error',
    //     timer: 3000, 
      
    //     showConfirmButton: true, 
    //         timerProgressBar: true 
    //   });
    //   this.showSpinner=false;
    // }else{
    //   Swal.fire(
    //     'Print Confirmation Letter Successfully',
    //     '',
    //     'success'
    //   );
    //   this.showSpinner=false;
    // }
     
   }
  })
 
}
printOLAkad(bookCode:any){
  
 
  const dialogRef = this.dialog.open(ModaldialogcomponentComponent, {
    width: '750px', // atur lebar modal
    height:'500px',
 
    data: {
      bookCode:bookCode,
     
      name_button:'printol'
    }
    });

    dialogRef.afterClosed().subscribe(result => {

      if (result && result.saved) {
         this.ditekan()
      }
    });
}
printSignedDoc(_bookCode:any, _pppu:any, _ptigau:any) {
  Swal.fire({
    title: "Are you sure to Print Signed Document?",
    text: "If agreed, the system will print the Signed  Document.",
    icon: 'info',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    reverseButtons: true,
    confirmButtonText: 'OK',
    cancelButtonText: 'Cancel'
  }).then(async (result) => {
    if (result.isConfirmed) {
      try {
        this.showSpinner=true;
       
        const response = await this.authService.printSignedDoc(_bookCode,_pppu);
      
        if(response.result.errorMessage){
          Swal.fire({
                            title: `${response.result.status}`,
                            text: `${response.result.errorMessage}`,
                            icon: 'error',
                            timer: 3000, 
                            
                            showConfirmButton: true, 
                    timerProgressBar: true 
                          })
                          this.showSpinner=false;

        }else{
          const filePDF = response.result.result.filePDF;;
          this.onLogin(this.asa.value);
          window.open(filePDF, "_blank");
          Swal.fire(
                        'Success to  Print Signed Document',
                        '',
                        'success'
                      );
                      this.showSpinner=false;
        }
  
 
   
      } catch (error) {
        console.log(error);
        Swal.fire(
          'Error!',
      'An internal error occurred during your request! ' ,
      'error'
        );
        this.showSpinner=false;
      }
    }
  });

}


openDialog2(_bookingHeaderID:any,_bookCode:any,_memberCode:any,_memberCodeLogin:any='admin'): void {
  if(_bookingHeaderID==null){


  Swal.fire({
    icon: 'warning',
    title: 'Booking Header ID is null, Are you sure to Continue?',
    text: 'If agreed, the page will be directed to the dashboard page of your commission.',
    showCancelButton: true,
    confirmButtonColor: '#007bff',
    cancelButtonColor: '#aaa',
    reverseButtons: true,
    confirmButtonText: 'OK',
    cancelButtonText: 'Cancel'
  }).then((result) => {
    if (result.isConfirmed) {
   
      console.log('Ok');
      window.open('https://booking.lippo.homes/app/main/my-commision', '_blank', 'menubar=yes,location=yes,resizable=yes,scrollbars=yes,status=yes');
    } else {
    
     
    }
    
  });
}
 else{
  const dialogRef = this.dialog.open(ModaldialogcomponentComponent, {
    width: '2550px', 
    height:'500px',
  
    data: {
      _bookingHeaderID:_bookingHeaderID,
      _bookCode:_bookCode,
      _memberCode:_memberCode,
      _memberCodeLogin:_memberCodeLogin,
      name_button:'Comm'
      
    }
  });
  dialogRef.afterClosed().subscribe(result => {
  
  });
 }

  
 
 
}

showModal(_projectID:any, _bookCode:any, _bookingHeaderID:any, _type?:any, _typeName?:any, _amount?:any){
  console.log( this.ppno,'ppnnooooooooo')
  localStorage.setItem("ppno", this.ppno);
  const dialogRef = this.dialog.open(ModaldialogcomponentComponent, {
    width: '550px', 
    height:'500px',

    data: {
      projectID: _projectID,
      bookCode:  _bookCode ,
      bookingHeaderID: _bookingHeaderID,
      type:_type,
      typeName:_typeName,
      amount:_amount,
      name_button:'Pay'
    }
  });

  dialogRef.afterClosed().subscribe(result => {
  
    if (result && result.saved) {
      //  this.ditekan()
    }
  });
}
async showModalDp(_projectID:any, _bookCode:any, _bookingHeaderID:any, _type?:any, _typeName?:any, _amount?:any){
  this.showSpinner=true;
  
  console.log( this.ppno,'ppnnooooooooo')
  ;
  localStorage.setItem("ppno", this.ppno);
  const response = await this.authService.getOutstandingDownPayment(_bookCode);
  const documentdp=response.result.dp;
  if(documentdp!=0.0){
   
    const dialogRef = this.dialog.open(ModaldialogcomponentComponent, {
      width: '550px', 
      height:'500px',
  
      data: {
        projectID: _projectID,
        bookCode:  _bookCode ,
        bookingHeaderID: _bookingHeaderID,
        type:_type,
        typeName:_typeName,
        amount:response.result.dp,
        name_button:'Asem'
      }
    });
    dialogRef.afterClosed().subscribe(result => {

      if (result && result.saved) {
      
        //  this.ditekan()
      }
      this.showSpinner=false;
    });
  }else{
    Swal.fire({
        
      title: 'No Pending Payments',
      icon: 'info',
    // Menutup pesan setelah 3 detik
    
      showConfirmButton: true, // Tidak menampilkan tombol OK
       // Menampilkan animasi timer
    });
    this.showSpinner=false;
  }
 

 
  
}
loadData(): void {
  const skipCount = this.paginator.pageIndex * this.paginator.pageSize;
  const maxResults = this.paginator.pageSize;
  
}
trackByFn(index: any, item: any): any {

}
clearInput() {


  this.asa2=undefined;
  if (this.memberCode && this.memberCode.nativeElement) {
    this.memberCode.nativeElement.value = '';
  }
  if (this.bookCode && this.bookCode.nativeElement) {
    this.bookCode.nativeElement.value = '';
  }
  if (this.keyword && this.keyword.nativeElement) {
    this.keyword.nativeElement.value = '';
  }
  


        

 
}
async getDetailDocument(_bookCode:any, _docCode:any) {


 
 this.showSpinner=true;
   try {
    
     const response = await this.authService.getDetailDocument(_bookCode, _docCode);
     return response;
     this.respond= response.result;
     console.log( this.respond,'alkjhg')
     if(!response.success){
      Swal.fire({
      title: 'Error',
      text: 'An internal error occurred during your request! ',
      icon: 'error',
      timer: 3000, 
    
      showConfirmButton: true, 
          timerProgressBar: true 
    });
    this.showSpinner=false;
}
    
    
   } catch (error) {
    Swal.fire(
      'Error!',
  'An internal error occurred during your request! ' ,
  'error'
    );
    this.showSpinner=false;
  }
}
selectedValue(event:any) {
 
  const selectedName = event.option.value;
  let selectedProjectId = null;

 
  const projects = this.image; 


  for (let i = 0; i < projects.length; i++) {
      const project = projects[i];
    
      if (project.projectName === selectedName) {
      
          selectedProjectId = project.projectID;
          break;
      }
  }
  
   
  this.getDropdownTerm(selectedProjectId);
  this.onLogin(selectedProjectId);
  this.clearInput();
  this.membercode = this.memberCode ? (this.memberCode.nativeElement.value === '' ? undefined : this.memberCode.nativeElement.value) : undefined;
  this.bookcode = this.bookCode ? (this.bookCode.nativeElement.value === '' ? undefined : this.bookCode.nativeElement.value) : undefined;
  this.inputValue2 = this.keyword ? (this.keyword.nativeElement.value === '' ? undefined : this.keyword.nativeElement.value) : undefined;
  this.asa=  selectedProjectId;
  this.existingData =  this.asa;
  
 
  // this.filteredBanks = this.bankFilterCtrl.valueChanges.pipe(
  //   startWith(''),
  //   map(value => this._filterBanks(value))
  // );
  this.paymentInput.nativeElement.value = ''; 
 //this.filteredBanks = [];

  
 

  this.skipCount=0;
  this.paginator.pageIndex=0
  this.selectedData = {
    value: event.value,
    text: event.source.triggerValue
  };
  
  
  this.asa2= undefined;


  console.log(this.asa.value);
}

selectedValue2(event:any) {


  const selectedName = event.option.value;
  let selectedtermId = null;
  

 
  const termdropdown = this.termdropdown; 

  for (let i = 0; i < termdropdown.length; i++) {
      const project = termdropdown[i];

      if (project.termRemarks === selectedName) {
        
          selectedtermId = project.termID;
          break;
      }
  }
  

  this.clearInput();
  this.skipCount=0;
  
  this.membercode = this.memberCode ? (this.memberCode.nativeElement.value === '' ? undefined : this.memberCode.nativeElement.value) : undefined;
  this.bookcode = this.bookCode ? (this.bookCode.nativeElement.value === '' ? undefined : this.bookCode.nativeElement.value) : undefined;
  this.inputValue2 = this.keyword ? (this.keyword.nativeElement.value === '' ? undefined : this.keyword.nativeElement.value) : undefined;
  this.paginator.pageIndex=0
  this.selectedData2 = {
    value: event.value,
    text: event.source.triggerValue
  };
  
  this.asa2=  selectedtermId;

  this.onLogin(this.asa)
  
 
}
toggleTheme() {
    this.themeService.toggleTheme();
}

toggleRTLEnabledTheme() {
    this.themeService.toggleRTLEnabledTheme();
}


onPageChange(event: any) {

  this.pageSize = event.pageSize;
  this.onLogin(this.asa);
 
}

async onLogin(_a:any) {



  this.showSpinner = true;
  try {
    if(_a!= undefined){
  

    const skipCounts = this.paginator?.pageIndex * this.paginator?.pageSize;
   
     this.skipCount = skipCounts ?? 0;
  //this.numberCount?0: 
     const skip= this.skipCount
    const maxResultCount = this.paginator?.pageSize??10;
    const loginData: CurentLoginDataInfo = {
      
      
      memberCode:this.membercode===undefined?undefined:this.membercode,
      projectID: [_a],
      keyword:this.inputValue2===undefined?undefined:this.inputValue2,
      bookCode:this.bookcode===undefined?undefined:this.bookcode,
        
       termID:this.asa2===undefined?undefined:this.asa2,
      maxResultCount: maxResultCount,
      skipCount: skip
    };
 
    const response = await this.authService.gateInquerySelesTracking(loginData);
   
    this.dataSource = response.result.items;
 
    this.totalItems = response.result.totalCount;

    if(response.error===null){
   
  
  
    }
  
  }
  this.showSpinner = false;
  } catch (error) {
    Swal.fire(
      'Error!',
  'An internal error occurred during your request! ' ,
  'error'
    );

   
  }
 

}
async getDropdownTerm(_:any) {

  this.showSpinners2 = true;

 




  try {
   const as=false
    const response = await this.authService.getDropdownTerm(_,as);
    const arrayDistinc = response.result.filter((value:any,index:any,self:any)=>{
      return self.indexOf(value) ===index;
    })
    this.termdropdown = response.result;

    // this.filteredBanks = this.bankFilterCtrl.valueChanges.pipe(
    //   startWith(''),
    //   map(value => this._filterBanks(value))
    // );
    this.filteredBanks = of(this.termdropdown);

  

    this.bankFilterCtrl.setValue('');
    this.bankFilterCtrl.valueChanges.pipe(
      startWith(''),
      map(value => this._filterBanks(value))
    ).subscribe(filteredBanks => {
      this.filteredBanks = of(filteredBanks);
    });
  } catch (error) {
    Swal.fire(
      'Error!',
  'An internal error occurred during your request! ' ,
  'error'
    );
  }

  finally {

  }
  this.showSpinners2 = false;
}
onChange(selectedBank: any) {
  
  if (this.memberCode && this.memberCode.nativeElement) {
    this.memberCode.nativeElement.value = '';
  }
  if (this.bookCode && this.bookCode.nativeElement) {
    this.bookCode.nativeElement.value = '';
  }
  if (this.keyword && this.keyword.nativeElement) {
    this.keyword.nativeElement.value = '';
  }
 
  this.membercode = this.memberCode ? (this.memberCode.nativeElement.value === '' ? undefined : this.memberCode.nativeElement.value) : undefined;
  this.bookcode = this.bookCode ? (this.bookCode.nativeElement.value === '' ? undefined : this.bookCode.nativeElement.value) : undefined;
  this.inputValue2 = this.keyword ? (this.keyword.nativeElement.value === '' ? undefined : this.keyword.nativeElement.value) : undefined;
  // const selectedName = selectedBank;
  // let selectedtermId = null;
  

 
  // const termdropdown = this.termdropdown; 

  // for (let i = 0; i < termdropdown.length; i++) {
  //     const project = termdropdown[i];

  //     if (project.termRemarks === selectedName) {
        
  //         selectedtermId = project.termID;
  //         break;
  //     }
  // }
  console.log('Selected bank:', selectedBank);
  this.asa2=  selectedBank.termID;
  this.onLogin(this.asa);
  // Lakukan operasi lain dengan bank yang dipilih, seperti menyimpan ke server atau memanipulasi data lainnya.
}

onChange2(selectedBank: any) {
  
  
  if (this.memberCode && this.memberCode.nativeElement) {
    this.memberCode.nativeElement.value = '';
  }
  if (this.bookCode && this.bookCode.nativeElement) {
    this.bookCode.nativeElement.value = '';
  }
  if (this.keyword && this.keyword.nativeElement) {
    this.keyword.nativeElement.value = '';
  }
 
  this.membercode = this.memberCode ? (this.memberCode.nativeElement.value === '' ? undefined : this.memberCode.nativeElement.value) : undefined;
  this.bookcode = this.bookCode ? (this.bookCode.nativeElement.value === '' ? undefined : this.bookCode.nativeElement.value) : undefined;
  this.inputValue2 = this.keyword ? (this.keyword.nativeElement.value === '' ? undefined : this.keyword.nativeElement.value) : undefined;
  // const selectedName = selectedBank;
  // let selectedtermId = null;
  
  this.asa2=undefined;
 
  // const termdropdown = this.termdropdown; 

  // for (let i = 0; i < termdropdown.length; i++) {
  //     const project = termdropdown[i];

  //     if (project.termRemarks === selectedName) {
        
  //         selectedtermId = project.termID;
  //         break;
  //     }
  // }
  console.log('Selected bank:', selectedBank);
  this.asa=  selectedBank.projectID;
  this.existingData =  this.asa;
  this.getDropdownTerm(this.asa);
  this.skipCount=0;
  this.paginator.pageIndex=0;
  this.numberCount=true;
  this.onLogin(this.asa)
 
  // Lakukan operasi lain dengan bank yang dipilih, seperti menyimpan ke server atau memanipulasi data lainnya.
}
//  getData(as:any) {
//   
//   this.showSpinners = true;

//   const id = localStorage?.getItem("UserId")
//   this.authService.getListProjectBySchema(as,id).subscribe(
    
//     (data) => {
   

//       this.image = data.result.items;
      
//       // this.filteredBanks2=data.result.items;
//       this.filteredProjects = this.image;
//       this.filteredOptions = this.image;
//       this.showSpinners = false;
//     },
//     (error) => {
    
//       this.showSpinners = false;
//     }
   
//   );

//   // this.filteredBanks2 = of(this.image);

  

//   // this.bankFilterCtrl2.setValue('');
//   // this.bankFilterCtrl2.valueChanges.pipe(
//   //   startWith(''),
//   //   map(value => this._filterBanks2(value))
//   // ).subscribe(filteredBanks => {
//   //   this.filteredBanks2 = of(filteredBanks);
//   // });

 
// }
async getData(as:any){
  
  this.showSpinners3=true
 
  const id = localStorage?.getItem("UserId")
  try {
    const response = await this.authService.getListProjectBySchema(as, id);

    // Check if response and response.result.items are defined
    if (response && response.result && response.result.items) {
      const image = response.result.items;
      this.image = image;
      this.filteredBanks2 = of(image);
      //this.filteredBanks2 = of(this.termdropdown);

  

      this.bankFilterCtrl2.setValue('');
      this.bankFilterCtrl2.valueChanges.pipe(
        startWith(''),
        map(value => this._filterBanks2(value))
      ).subscribe(filteredBanks => {
        this.filteredBanks2 = of(filteredBanks);
      });
    } else {
      // Handle the case where response or items are undefined
      console.error('API response or items are undefined');
      this.filteredBanks2 = of([]); // or handle as needed
    }
  } catch (error) {
    // Handle error appropriately
    console.error('Error fetching project list', error);
    this.filteredBanks2 = of([]); // or handle as needed
  }
  this.showSpinners3=false
 }
 
async resendAccountStatement(bookCode:any,_name:any){
  
  Swal.fire({
    title: "Are you sure to Resend Account Statement to Customer?",
    text: "If agreed, the system will send the Account Statement back to '" +
    _name +
    "' email as the related customer.",

  
  
    icon: 'info',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    reverseButtons: true,
    confirmButtonText: 'OK',
    cancelButtonText: 'Cancel'
  }).then(async (result) => {
    if (result.isConfirmed) {
   
  
      try {
        this.showSpinner=true;
        const response = await this.authService.sendEmailAccountStatementByBookCode(bookCode);
        if(response.result.status!="Failed"){
          Swal.fire({
                            title: `Success`,
                            text: `Success to Resend Account Statement Successfully`,
                            icon: 'success',
                            timer: 3000, 
                            
                            showConfirmButton: true, 
                    timerProgressBar: true 
                          })
                          this.showSpinner=false;

        }else{
          Swal.fire({
            title: `${response.result.status}`,
            text: `${response.result.errorMessage}`,
            icon: 'error',
            timer: 3000, 
            
            showConfirmButton: true, 
    timerProgressBar: true 
          })
          this.showSpinner=false;
        }
     
      } catch (error) {
        Swal.fire(
          'Error!',
      'An internal error occurred during your request! ' ,
      'error'
        );
        this.showSpinner=false; 
      }

    }
  })
}
async resendEmailConfirmation(_bookCode:any,_email:any, _name:any){

  const inputw :  SendEmailConfirmationDto ={
    bookcode: _bookCode,
    emailCustomer:_email,
  namaCustomer: _name
  }
  Swal.fire({
    title: "Are you sure to Resend Confirmation to Customer?",
    text: "If agreed, the system will send the Email Confirmation back to " +
    _name +
    " email as the related customer.",

  
    reverseButtons: true,
    icon: 'info',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'OK',
    cancelButtonText: 'Cancel'
  }).then(async (result) => {

    if (result.isConfirmed) {
    try{
      this.showSpinner=true;
      const response = await this.authService.sendEmailConfirmation(_bookCode,_email, _name)


        if(response.result.errorMessage){
          Swal.fire({
                            title: `${response.result.status}`,
                            text: `${response.result.errorMessage}`,
                            icon: 'error',
                            timer: 3000, 
                            
                            showConfirmButton: true, 
                    timerProgressBar: true 
                          })
                          this.showSpinner=false;

        }else{
          Swal.fire(
                        'Success to Resend Confirmation',
                        '',
                        'success'
                      );
                      this.showSpinner=false;
        }
      } catch (error) {

        Swal.fire(
          'Error!',
      'An internal error occurred during your request! ' ,
      'error'
        );
        
        this.showSpinner=false;
      }
  
         
     }
  })
}
async showModalsendPaymentInstruction( _bookCode: any,
  _pmtInstruction: any,
  _pmtInstructionName: any,
  _email: any,
  _name: any,
  _memberCode: any,
  _orderCode?: any,
  _amountPay?: any) {
    const as: SendPaymentInstructionsDto = {
      bookcode: _bookCode,
      orderCode: _orderCode,
      paymentInstructions: _pmtInstruction,
      emailCustomer: _email,
      customerName: _name,
      memberCode:
          _memberCode != undefined
              ? _memberCode
              : 'admin',
      amountPay: _amountPay,
    };
  Swal.fire({
    title: "Are you sure to Send Payment Instruction " + _pmtInstructionName + " to Customer?",
        text: "If agreed, the system will send the Payment Instruction " + _pmtInstructionName + " back to '" + _name + "' email as the related customer.",
  
    icon: 'info',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    reverseButtons: true,
    confirmButtonText: 'OK',
    cancelButtonText: 'Cancel'
  }).then(async (result) => {
    if (result.isConfirmed) {
      this.showSpinner=true;
      
      try {
        
        const response = await this.authService.sendPaymentInstructions(as);
        if(response.result.status=='Failed'){
          Swal.fire({
                            title: `${response.result.status}`,
                            text: `${response.result.errorMessage}`,
                            icon: 'error',
                            timer: 3000, 
                            
                            // showConfirmButton: true, 
                    timerProgressBar: true 
                          })
                          this.showSpinner=false;

        }else{
          Swal.fire(
                        'Success to send payment instructions',
                        '',
                        'success'
                      );
                      this.showSpinner=false;
        }
      } catch (error) {
        Swal.fire(
          'Error!',
      'An internal error occurred during your request! ' ,
      'error'
        );
        this.showSpinner=false;
      }
   }
  })
 
}

  getTotalPages(): number {
    return Math.ceil(this.totalItems / this.paginator.pageSize);
  }


 

}
