

import { MatTableDataSource } from '@angular/material/table';
import { FormBuilder, FormGroup } from '@angular/forms';
import { FormControl, Validators } from '@angular/forms';
import { EditorChangeContent, EditorChangeSelection } from 'ngx-quill';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {provideNativeDateAdapter} from '@angular/material/core';
import { Component, inject, ElementRef ,HostListener,ViewChild,AfterViewInit,Input} from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { of } from 'rxjs';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatMenuTrigger } from '@angular/material/menu';
import {  Inject } from '@angular/core';
import {  MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { AuthService } from '../../../../shared/auth.service';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import Quill from 'quill';
import {  OnInit } from '@angular/core';
import { SalestrackingService } from '../../../../shared/salestracking.service';
import {SelestrackingComponent} from '../selestracking.component'
import {
  DocumentKPRListDto,
  DocumentKPRInputDto,
  
} from '../../../../shared/auth.service';
import Swal from 'sweetalert2';
import { EventEmitter, Output } from '@angular/core';
interface CurentLoginDataInfo {
 
  userName: any;
  // keyword: any;
  // bookCode: any;
  bookingHeaderId: any;
  priorityPassId: any;
  creationUserId:any

}
@Component({
  selector: 'app-modaldialogcomponent',
  templateUrl: './modaldialogcomponent.component.html',


  styleUrls: ['./modaldialogcomponent.component.scss']
})

export class ModaldialogcomponentComponent  {
  @Output() modalResponse: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('inputs', { static: false }) inputs!: ElementRef<HTMLInputElement>;
  @ViewChild('inputs2', { static: false }) inputs2!: ElementRef<HTMLInputElement>;
   documents: any;
  document : any;
  ppno:any;
  isGlobalLoading: boolean = false;
  input = new DocumentKPRInputDto();
  optionsFileAMDT = {
    max_file_size: 100000,
    type: 'jpg|jpeg|png|pdf',
    url: 'UploadAddendumDocsLegal',
    pictureUrl: 'LegalDocument/UploadAddendumDocsLegal'
  };
  //inputHistoryView = new CreateHistoryViewDocumentDbo();
  listComission:any;
  bookingHeaderId: any;
  showSpinner: boolean = false;
  filteredPaymentOptions:any[] = [];
  favoriteSeason: any;
  displayedColumns: string[] = ['position', 'name', 'email', 'memberCode','paymentStatus','totalPayment','document'];
  //dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  dataSource: any[] = [];
 // displayedColumns: string[] = ['id', 'name', 'description'];
  pageSizeOptions: number[] = [5, 10, 25, 100]; // Pilihan jumlah item per halaman
  pageSize: number = 10; // Default jumlah item per halaman
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  images: any;
  listComissiondataTotal:any[];
  listComissiondataHeader:any[];
  listComissiondataBody:any[];
  listComissiondataBookingFee:any[];
  filteredProjects: any[] = [];
  pageIndex: number = 0;
  @ViewChild('oldEmail') oldEmail: ElementRef;
  myForm: FormGroup;
  documentPicType:any;
  form: FormGroup;
  forme: FormGroup;
  selectedData:any;
  salesId:any=["29"];
  @ViewChild('paymentInput') paymentInput: ElementRef;
  max:any=10;
  ppNo:any=10;
  skip:any=0;
  areFieldsFilledd:boolean=true;
  totalItems: number = 0;
  skipCount:any;
  documento:any;
  generatePdfUrl:any;
  signaturdate:any;
  documentdp:any;
  blurred = false;
  image: any[] = [];
    focused = false;
    isPaymentSelected: boolean = false;
    areFieldsFilled: boolean = true;
  userName:any;
  showSpinner2:boolean;
  showSpinners:boolean = true;
  newemail: string = '';
  fileSizeBinary:any=5242880;
  public router = inject(Router);
  newEmailForm: FormGroup;
  public authService = inject(AuthService);
  public salestrackingService = inject(SalestrackingService);
  oldRemarksTerm:any;
  filteredOptions: any[]=[];
  filteredOption:any[]=[];
  changedata:any=true;
  changedatabank:any=true;
  respondemail:any;
  unitcodeunitno:any;
  termId:number;
  image2 :any;
  bankCode:any;
  respond:any;
  showSpinners3:boolean;
  showSpinners4 : boolean;
 filteredProjects2:any; 
  filteredOptions2:any;
  termdropdown:any[]=[];
  isActiveBank:boolean;
  bankId:any;
  dateForm: FormGroup;
  selectedProject:any;
  dataprintol: any = {};
  dataprintolrupiah: any = {};
  bankCtrl2 = new FormControl();
  bankFilterCtrl2 = new FormControl();
  filteredBanks2: Observable<any[]>;
  bankCtrl = new FormControl();
  bankFilterCtrl = new FormControl();
  filteredBanks: Observable<any[]>;
  datalocalstorage:any;
  
  // public salestracking = inject(SelestrackingComponent);
  constructor(private fb: FormBuilder,public dialogRef: MatDialogRef<ModaldialogcomponentComponent>,@Inject(MAT_DIALOG_DATA) public data: any) {
    // if(this.data.statecekCl==true){
    //   this.dialogRef.close();
    // }
    this.datalocalstorage=localStorage.getItem('test');
    this.newEmailForm = new FormGroup({
      email: new FormControl(null, Validators.required),
    
    });
    this.form = new FormGroup({
      file: new FormControl(null, Validators.required),
      date: new FormControl(null, Validators.required)
    });
  
    this.oldRemarksTerm = this.data.termRemark;
    this.getListDocucmentKPR(this.data.psCode,this.data.bookingId,this.data.ppno)
    this.getListView(this.data.bookingId,this.data.psCode,this.data.ppno)
    this.getListCommision()
    this.GetDetailTaskListMyCommisionBF(this.data._bookingHeaderID,this.data._bookCode,this.data._memberCode,this.data._memberCodeLogin)
    this.getListPaymentTypeGateway(this.data.projectID)
    this.getOutstandingDownPayment(this.data.bookCode);
     this.getData(this.data.bookCode)
    this.getUnitCoceUnitno(this.data.bookCode)
    this.getDataBank(this.data.projectId)
    this.onSubmits(this.data.bookCode)
    this.getDataRuoiah(this.data.bookCode)
  
  }
  onFileSelected(event: any) {
    const file: File = event.target.files[0];
    
  }
  get fileControl() {
    return this.form.get('file');
  }
  get emailControl() {
    return this.forme.get('email');
  }
  get dateControl() {
    return this.form.get('date');
  }
  onPaymentMethodChange(event:any) {
    this.isPaymentSelected = true;
    const valueconvert = event.value;
    this.favoriteSeason= parseInt(valueconvert, 10); // menyimpan nilai radio yang dipilih
    console.log( this.favoriteSeason)
  }

  filter1(): void {
    // Memastikan bahwa this.input telah diinisialisasi
    const filterValue = this.inputs.nativeElement.value.toLowerCase();
    this.filteredOptions = this.image.filter((o:any) => o.remarks.toLowerCase().includes(filterValue));
  
    const term = filterValue.trim() === ''
    console.log(term,'termmmmmmmmmmmmmmmmmm')
    if(term===true){
      this.changedata=true
    }else{
      this.changedata=false
    }
  }
  handleClick() {
    // Lakukan logika yang ingin Anda jalankan ketika bidang diklik di sini
    
   this.filteredOptions=this.image;
  }
  handleClick1(){
    this.filteredPaymentOptions = this.termdropdown;
  }
  // untuk bank----------------------------------------------------
  filter2(): void {
    // Memastikan bahwa this.input telah diinisialisasi
    const filterValue = this.inputs.nativeElement.value.toLowerCase();
    this.filteredOption = this.image2.filter((o:any) => o.bankName.toLowerCase().includes(filterValue));
  
    
  }
  filterPayments() {
    let searchValue = this.paymentInput.nativeElement.value.toLowerCase();
  
    this.filteredPaymentOptions = this.termdropdown.filter((option:any) =>
      option.bankName.toLowerCase().includes(searchValue)
    );
    const term = searchValue.trim() === ''
    console.log(term,'termmmmmmmmmmmmmmmmmm')
    if(term===true && this.isActiveBank==true){
      this.changedatabank=true
    }else{
      this.changedatabank=false
    }
  }
  displayFn2(option: any): string {
    return option ? option.bankName : '';
  }
  selectedValue2(event:any) {

    
    const selectedName = event.option.value;
    let selectedtermId = null;
    
    let bankId = null;
    // Misalkan this.image adalah array yang berisi objek-objek projek
    const termdropdown = this.termdropdown; 
    // Iterasi melalui setiap objek projek
    for (let i = 0; i < termdropdown.length; i++) {
        const project = termdropdown[i];
        // Periksa apakah projectName proyek sama dengan selectedName
        if (project.bankName === selectedName) {
            // Jika iya, simpan ID proyek tersebut dan hentikan iterasi
            selectedtermId = project.bankCode;
            bankId= project.bankID
            break;
        }
    }
    
  
   
    
    this.bankCode=  selectedtermId;
    this.bankId=bankId;
  
console.log( this.bankCode,'bankId')
    
   
  }
  // untuk bank----------------------------------------------------
  displayFn(option: any): string {
    return option ? option.remarks : '';
  }
  selectedValue(event:any) {
    const selectedOption = event.option.value; 
    const selectedTermId = selectedOption.termID; 
    const selectedRemarks = selectedOption.remarks; 
    if(selectedRemarks.toLowerCase().includes('kp')){
      this.isActiveBank=true;
    }else{
      this.isActiveBank=false;
    }
    this.termId= selectedTermId;
    console.log('Term ID yang dipilih:', selectedTermId);
    console.log('Remarks yang dipilih:', selectedRemarks);

    console.log('Term ID yang dipilih:', selectedTermId);
    const selectedName = event.option.value;
    let selectedProjectId = null;
  
    // Misalkan this.image adalah array yang berisi objek-objek projek
    const projects = this.image; // Ubah sesuai dengan nama variabel yang sesuai di programmu
  
    // Iterasi melalui setiap objek projek
    for (let i = 0; i < projects.length; i++) {
        const project = projects[i];
        // Periksa apakah projectName proyek sama dengan selectedName
        if (project.remarks === selectedName) {
            // Jika iya, simpan ID proyek tersebut dan hentikan iterasi
            selectedProjectId = project.projectID;
            break;
        }
    }
   
  }
  trackByFn(index: any, item: any): any {
    console.log( item.projectID,'oppppppppppppppppp'); // Menggunakan ID sebagai kunci untuk menandai ulang
  }
  async getDataBank(projectId:any){
    
    // this.showSpinners3=true
    this.showSpinners4 = true;
    const id = localStorage?.getItem("UserId")
    try {
      const response = await this.authService.getTermBookingBank(projectId);
  
      // Check if response and response.result.items are defined
      if (response && response.result ) {
        
        // Lakukan sesuatu dengan data yang diterima di sini
        const term = response.result
        this.termdropdown = term;
        // this.filteredProjects2 = this.image2;
        // this.filteredOption= this.image2;
        
     
        this.filteredBanks = of( term );
        //this.filteredBanks2 = of(this.termdropdown);
  
    
  
        this.bankFilterCtrl.setValue('');
        this.bankFilterCtrl.valueChanges.pipe(
          startWith(''),
          map(value => this._filterBanks(value))
        ).subscribe(filteredBanks => {
          this.filteredBanks = of(filteredBanks);
        });
      } else {
        // Handle the case where response or items are undefined
        console.error('API response or items are undefined');
        this.filteredBanks2 = of([]); // or handle as needed
        this.showSpinners = false;
      }
    } catch (error) {
      // Handle error appropriately
      console.error('Error fetching project list', error);
      this.filteredBanks2 = of([]); // or handle as needed
    }
    // this.showSpinners3=false
    this.showSpinners4 = false;
   }
  // getDataBank(projectId:any) {
  

 
  //   //console.log(skipCount)
  //   // const skipCount = pageIndex * pageSize;
  //   // const maxResultCount = pageSize;

  //   //const as=this.userName;
  //   const id = localStorage?.getItem("UserId")
  //   this.authService.getTermBookingBank(projectId).subscribe(
      
  //     (data) => {
  //       console.log('Response from API:', data);
  //       // Lakukan sesuatu dengan data yang diterima di sini
  //       this.termdropdown = data.result;
  //       this.filteredProjects2 = this.image2;
  //       this.filteredOption= this.image2;
  //       this.showSpinners = false;
  //     },
  //     (error) => {
  //       console.error('Error fetching project list:', error);
  //       // Lakukan penanganan kesalahan di sini
  //       this.showSpinners = false;
  //     }
     
  //   );
  
  //   // try {
  //   //  const as='admin'
  //   //   const response = await this.authService.getListProjectBySchema(as);
  //   //   this.image = response.result.items;
  
  //   //   console.log(this.image);
  //   //   console.log(response)
  //   // } catch (error) {
  //   //   console.error('Error fetching data', error);
  //   // }
  
   
  // }

  
  //batas------------------------------------------
  // getData(bookCode:any) {
  

 
  //   //console.log(skipCount)
  //   // const skipCount = pageIndex * pageSize;
  //   // const maxResultCount = pageSize;

  //   //const as=this.userName;
  //   const id = localStorage?.getItem("UserId")
  //   this.authService.getTermBooking(bookCode).subscribe(
      
  //     (data) => {
  //       console.log('Response from API:', data);
  //       // Lakukan sesuatu dengan data yang diterima di sini
  //       this.image = data.result;
  //       this.filteredProjects = this.image;
  //       this.filteredOptions = this.image;
  //       this.showSpinners = false;
  //     },
  //     (error) => {
  //       console.error('Error fetching project list:', error);
  //       // Lakukan penanganan kesalahan di sini
  //       this.showSpinners = false;
  //     }
     
  //   );
  
  //   // try {
  //   //  const as='admin'
  //   //   const response = await this.authService.getListProjectBySchema(as);
  //   //   this.image = response.result.items;
  
  //   //   console.log(this.image);
  //   //   console.log(response)
  //   // } catch (error) {
  //   //   console.error('Error fetching data', error);
  //   // }
  
   
  // }
  onChange2(selectedBank: any) {
    
   
    const term2 = selectedBank === undefined;
    const term3 = selectedBank === null;
   
    if(term2===true ||term3===true){
      this.changedata=true
    }else{
      this.changedata=false
    }
    const selectedRemarks= selectedBank.remarks
    if(selectedRemarks.toLowerCase().includes('kp')){
      this.isActiveBank=true;
    }else{
      this.isActiveBank=false;
    }
  
    this.termId= selectedBank.termID;
  
    // Lakukan operasi lain dengan bank yang dipilih, seperti menyimpan ke server atau memanipulasi data lainnya.
  }


  onChange(selectedBank: any) {
    
    const term2 = selectedBank === undefined;
    const term3 = selectedBank === null;
    if((term2===true||term3===true) && this.isActiveBank==true){
      this.changedatabank=true
    }else{
      this.changedatabank=false
    }
    this.bankCode=  selectedBank.bankCode;
    this.bankId=selectedBank.bankId;
  
    // Lakukan operasi lain dengan bank yang dipilih, seperti menyimpan ke server atau memanipulasi data lainnya.
  }
  private _filterBanks2(value: string): any[]{
    const filterValue = value.toLowerCase();
    return this.image.filter(bank => bank.remarks.toLowerCase().includes(filterValue));
  }
  private _filterBanks(value: string): any[]{
    const filterValue = value.toLowerCase();
    return this.termdropdown.filter(bank => bank.bankName.toLowerCase().includes(filterValue));
  }
  async getData(bookCode:any){
    
    // this.showSpinners3=true
    this.showSpinners3 = true;
    const id = localStorage?.getItem("UserId")
    try {
      const response = await this.authService.getTermBooking(bookCode);
  
      // Check if response and response.result.items are defined
      if (response && response.result ) {
        this.showSpinners = true;
        const image = response.result;
        this.image = image;
        this.filteredBanks2 = of(image);
        //this.filteredBanks2 = of(this.termdropdown);
  
    
  
        this.bankFilterCtrl2.setValue('');
        this.bankFilterCtrl2.valueChanges.pipe(
          startWith(''),
          map(value => this._filterBanks2(value))
        ).subscribe(filteredBanks => {
          this.filteredBanks2 = of(filteredBanks);
        });
      } else {
        // Handle the case where response or items are undefined
        console.error('API response or items are undefined');
        this.filteredBanks2 = of([]); // or handle as needed
      }
    } catch (error) {
      // Handle error appropriately
      console.error('Error fetching project list', error);
      this.filteredBanks2 = of([]); // or handle as needed
      
    }
    this.showSpinners3 =  false;
   }
   
  onDateChange(dateValue: any) {
    
    this.areFieldsFilledd = false;
    const dates= dateValue.value
    const date = new Date(dates);
    const isoDateString = date.toISOString();
    this.signaturdate=isoDateString;
    // Lakukan sesuatu dengan nilai tanggal di sini
  }
  //-------------------------------------------------------------------------------------
  async onSubmits(bookCode:any) {

    // if (this.form.valid) {
      // const input:any={
       
      //   trBookingDocID: this.data.bookingDocumentID,
      //   docNo: this.data.docNo,
      //   signedDocumentFile:  this.document.fileName,
      //   signatureDate: this.signaturdate
      // }
       const response = await this.authService.GetMappingOfferingLetterNobu(bookCode)
       this.dataprintol=response.result;
       console.log(this.dataprintol,'test...................')
    // } else {
    //   // Tampilkan pesan kesalahan atau lakukan tindakan yang sesuai
    // if(response.result.errorMessage!==""||response.result.errorMessage!==null||response.result.errorMessage!==undefined){
    //   // Swal.fire({
    //   // title: `${response.result.status}`,
    //   // text: `${response.result.errorMessage}`,
    //   // icon: 'error',
    //   // timer: 3000, // Menutup pesan setelah 3 detik
    
    //   // showConfirmButton: true, // Tidak menampilkan tombol OK
    //   //     timerProgressBar: true 
    // });
      // console.log("Form tidak valid:",response.result.errorMessage);
    //   // Dapat mengakses status dirty dan touched dari form control
    
    //}
  }
  async getDataRuoiah(bookCode:any) {

    // if (this.form.valid) {
      // const input:any={
       
      //   trBookingDocID: this.data.bookingDocumentID,
      //   docNo: this.data.docNo,
      //   signedDocumentFile:  this.document.fileName,
      //   signatureDate: this.signaturdate
      // }
       const response = await this.authService.GetPlafondBookingDocument(bookCode)
       this.dataprintolrupiah=response.result;
       
    // } else {
    //   // Tampilkan pesan kesalahan atau lakukan tindakan yang sesuai
    // if(response.result.errorMessage!==""||response.result.errorMessage!==null||response.result.errorMessage!==undefined){
    //   // Swal.fire({
    //   // title: `${response.result.status}`,
    //   // text: `${response.result.errorMessage}`,
    //   // icon: 'error',
    //   // timer: 3000, // Menutup pesan setelah 3 detik
    
    //   // showConfirmButton: true, // Tidak menampilkan tombol OK
    //   //     timerProgressBar: true 
    // });
      // console.log("Form tidak valid:",response.result.errorMessage);
    //   // Dapat mengakses status dirty dan touched dari form control
    
    //}
  }
  //-------------------------------------------------------------------------------------
  async onSubmit() {
    this.showSpinner=true
 
    // if (this.form.valid) {
      const input:any={
       
        trBookingDocID: this.data.bookingDocumentID,
        docNo: this.data.docNo,
        signedDocumentFile:  this.document.fileName,
        signatureDate: this.signaturdate
      }
       const response = await this.authService.UploadSignedDocument(input)
       this.showSpinner=true
    // } else {
    //   // Tampilkan pesan kesalahan atau lakukan tindakan yang sesuai
    if(response.result.errorMessage!=null){
      Swal.fire({
      title: `${response.result.status}`,
      text: `${response.result.errorMessage}`,
      icon: 'error',
      timer: 3000, // Menutup pesan setelah 3 detik
    
      showConfirmButton: true, // Tidak menampilkan tombol OK
          timerProgressBar: true 
    });
      // console.log("Form tidak valid:",response.result.errorMessage);
    //   // Dapat mengakses status dirty dan touched dari form control
    this.showSpinner=false
    }else{
      if(this.data.docCode=='CL'){
        Swal.fire(
          'Upload Confirmation Letter Successfully',
          '',
          'success'
        );
        this.dialogRef.close({ saved: true });
        this.showSpinner=false
      }
    
      if(this.data.docCode=='PPPU'){
        Swal.fire(
          'Upload P3U Successfully',
          '',
          'success'
        );
        this.dialogRef.close({ saved: true });
        this.showSpinner=false
      }
      if(this.data.docCode=='OLN2'){
        Swal.fire(
          'Upload OL & Akad Successfully',
          '',
          'success'
        );
        this.dialogRef.close({ saved: true });
        this.showSpinner=false
      }
    }
  }
  async closeDialog() {
    // debugger
    // const input2:any={
       
    //   trBookingDocID: this.data?.bookingDocumentID,
    //   docNo: this.data?.docNo,
    //   signedDocumentFile:  this.document?.fileName,
    //   signatureDate: this.signaturdate
    // }
    // const response = await this.authService.UploadSignedDocument(input2)
    if(this.data.statecekCl==true){
      this.dialogRef.close();
    }
    
    this.dialogRef.close();
    
  
  }
 async save2(){
 debugger;
 
  const input:any={
    bookingHeaderID: this.data.bookingHeaderID,
    payTypeID:this.favoriteSeason,
    amount: this.data.amount,
  }
 const result = await  Swal.fire({
  title: 'Are you sure to Pay?',
  text: 'If agreed, you will be directed according to the terms of the selected payment method.',
  icon: 'warning',
  showCancelButton: true,
  reverseButtons: true,
  cancelButtonText: 'Cancel',
  confirmButtonText: 'Yes',
  
});
//  Swal.fire({
//     title: 'Are you sure to Pay?',
//     text: 'If agreed, you will be directed according to the terms of the selected payment method.',
//     icon: 'warning',
//     showCancelButton: true,
//     cancelButtonText: 'Cancel',
//     confirmButtonText: 'Yes',
//     customClass: {
//       confirmButton: 'btn btn-success mr-2', // tombol OK dengan margin kanan
//       cancelButton: 'btn btn-secondary' // tombol Cancel
//     }
//   });
 

  if (result.isConfirmed) {
    // Panggil layanan updateTerm jika pengguna menekan tombol "Yes"

    const result = await Swal.fire({
      title: `Redirecting you to Midtrans, please don't refresh this page`,
 
      icon: 'warning',
      showCancelButton: false,
      confirmButtonText: 'OK',
     
    });
  
    if (result.isConfirmed) {
      // Panggil layanan updateTerm jika pengguna menekan tombol "Yes"
      const respond = await this.authService.OrderPaymentMidtrans(input);
      // if(respond.error.message!=null){
      //   Swal.fire({
      //     title: `${respond.error.message}`,
        
      //     icon: 'error',
      //     showConfirmButton: true, // Menampilkan tombol OK
        
      //   });
      // }else{
      //   Swal.fire({
      //     title: `Success to Payment `,
        
      //     icon: 'success',
      //     showConfirmButton: true, // Menampilkan tombol OK
        
      //   });
      // }
    this.generatePdfUrl = respond.result.redirect_url;
    this.respond =respond.result.permata_va_number;
    this.respondemail =respond.result.biller_code;
    if(this.generatePdfUrl != null){
      window.open(this.generatePdfUrl , '_blank', 'menubar=yes,location=yes,resizable=yes,scrollbars=yes,status=yes');
     
    }
   
      this.dialogRef.close({ saved: true });
      if (this.generatePdfUrl == null) {
        Swal.fire({
          title: `Your virtual account number is (Permata Bank) ${this.respond ? this.respond : ''}`,
          text: `Payment method has been sent to email:${this.respondemail?this.respondemail:''}`,
          icon: 'warning',
          showConfirmButton: true, // Menampilkan tombol OK
        
        });
      }

    }
    
  }
      // Buka URL dalam jendela baru
      
    // const data =this.dataprintolrupiah.plafond;
    // var stringTanpaTitik = data.replace(/\./g, ''); // Menghapus semua titik
// console.log(stringTanpaTitik); // Output: '729550552,00'
//   const
// const response = await this.authService.postPrintOLN(this.listComission);
  }
  // modalSave(){
    
  //   this.postData();

  // }
  async confirmUpdate() {
    const result = await Swal.fire({
      title: 'Are you sure?',
      text: 'You are about to update the data.',
      icon: 'warning',
      showCancelButton: true,
      reverseButtons: true,
      confirmButtonText: 'Yes',
      cancelButtonText: 'Cancel'
    });
  
    if (result.isConfirmed) {
      // Panggil layanan updateTerm jika pengguna menekan tombol "Yes"
      this.modalSave2();
    }
  }
  modalSave2(){
    ;
    this.showSpinner= true;
    const payload = {
      // projectId:_projectId,
      // bankCode:_bankCode,
      // bookCode:_bookCode,
      // unitId: _unitId,
      // unitOrder: _unitOrder,
      // termRemark: _termRemark,
      
      // bookingHeaderId:_bookingHeaderId,
      termID: this.termId,
      unitID: this.data.unitID,
      bankCode: this.bankCode,
      bankDetailID: this.bankId,
      bookingHeaderID: this.data.bookingHeaderId,
   
    };

    this.authService.updateTerm(payload).subscribe(
      (response) => {
         const data =this.regenarate()
        console.log('Pembaruan berhasil:', response);
        Swal.fire({
          icon: 'success',
          title: 'Success!',
          text: 'Data has been updated successfully.'
        });
        this.closeDialog()
        this.dialogRef.close({ saved: true });
      },
      (error) => {
        console.error('Pembaruan gagal:', error);
        Swal.fire({
          icon: 'error',
          title: 'Error!',
          text: 'Failed to update data. Please try again later.'
        });
      }
    );
    this.showSpinner= false;
  }
  regenarate(){
    const payload = {
      // projectId:_projectId,
      // bankCode:_bankCode,
       bookCode:this.data.bookCode
      // unitId: _unitId,
      // unitOrder: _unitOrder,
      // termRemark: _termRemark,
      
      
    };

    // Memanggil layanan untuk melakukan pembaruan
    this.authService.RegenerateSchedule(payload).subscribe(
      (response) => {
       
      },
      (error) => {
       
      }
    );
  }
  async print() {
    this.showSpinner=true
    try {
      // Panggil metode GeneratePDFMyCommission dan tunggu responsenya
      const response = await this.authService.GeneratePDFMyCommission(this.listComission);
      
      // Dapatkan URL dari response yang diterima
      this.generatePdfUrl = response.result.urls;
  
      // Buka URL dalam jendela baru
      window.open(this.generatePdfUrl , '_blank', 'menubar=yes,location=yes,resizable=yes,scrollbars=yes,status=yes');
    } catch (error) {
      // Tangani kesalahan jika terjadi
      console.error('Error:', error);
    }
    this.showSpinner=false
  }
  // postData(): void {
  
  //  this.showSpinner=true;
  //   // const data = { 
  //   //   psCode: this.data.psCode,
  //   //   oldEmail: this.data.email,
  //   //   newEmail: this.newemail,
  //   //   creationUserId: 457501
  //   // };
  //   // this.salestrackingService.postDataEmailSalestracking(data).subscribe({
  //   //   next: (response) => {
  //   //     this.dialogRef.close({ saved: true });
  //   //     console.log('Post successful:', response);
  //   //     Swal.fire(
  //   //       'Success Change Email',
  //   //       '',
  //   //       'success'
  //   //     );
  //   //     this.showSpinner=false;
  //   //   },
  //   //   error: (error) => {
  //   //     console.error('Error posting data:', error);
  //   //     Swal.fire(
  //   //       'Error Change Email',
  //   //       '',
  //   //       'error'
  //   //     );
  //   //     this.showSpinner=false;
  //   //   }
  //   // });

  // }
  focus($event:any) {
    this.focused = true
    this.blurred = false
}
changedEditor(event: EditorChangeContent | EditorChangeSelection) {}
blur($event:any) {
  this.focused = false
  this.blurred = true
}

  async createHistoryViewDocument() {

    this.showSpinner=true
    this.userName=localStorage.getItem('userName')
    const loginData: CurentLoginDataInfo = {
      userName: localStorage.getItem('userName'),
      
      bookingHeaderId:this.data.bookingId,
      priorityPassId: this.data.priorityPassID,
      creationUserId: localStorage.getItem('UserId')
   
    };
     try {
      
       const response = await this.authService.createHistoryViewDocument(loginData);
       this.document=response.result;
       if(!response.success){
        Swal.fire({
        title: 'Error',
        text: 'Internal Bad Request',
        icon: 'error',
        timer: 3000, // Menutup pesan setelah 3 detik
      
        showConfirmButton: true, // Tidak menampilkan tombol OK
            timerProgressBar: true 
      });
  }
    
       console.log(this.documents)
       
     } catch (error) {
       console.error('Error fetching data', error);
     
     }
     this.showSpinner=false
     //this.showSpinner = false;
   }
   async GetDetailTaskListMyCommisionBF(_bookingHeaderID:any,_bookCode:any,_memberCode:any,_memberCodeLogin:any){
    this.showSpinner2=true
   
   
     try {
      
      
       const response = await this.authService.getDetailTaskListMyCommisionBF(_bookingHeaderID,_bookCode,_memberCode,_memberCodeLogin);
       this.document=response.result;
       if(!response.success){
        Swal.fire({
        title: 'Error',
        text: 'Internal Bad Request',
        icon: 'error',
        timer: 3000, // Menutup pesan setelah 3 detik
      
        showConfirmButton: true, // Tidak menampilkan tombol OK
            timerProgressBar: true 
      });
  }
    
       console.log(this.documents)
       
     } catch (error) {
       console.error('Error fetching data', error);
     
     }
     this.showSpinner2=false
   }
   
   async getOutstandingDownPayment(bookCode:any){

    this.showSpinner=true
   
   
    try {
      const response = await this.authService.getOutstandingDownPayment(bookCode);
      this.documentdp=response.result.dp;
     if(this.data.name_button=='Asem'){
    
      if(this.documentdp==0.0&&this.data.name_button==='Asem'){
         this.dialogRef.close({ saved: true });
        Swal.fire({
        
          title: 'No Pending Payments',
          icon: 'info',
        // Menutup pesan setelah 3 detik
        
          showConfirmButton: true, // Tidak menampilkan tombol OK
           
        });
      
      }
      if(!response.success){
       Swal.fire({
       title: 'Error',
       text: 'Internal Bad Request',
       icon: 'error',
       timer: 3000, 
     
       showConfirmButton: true, 
           timerProgressBar: true 
     });
 }
     }
      
   
      console.log(this.documents)
      const response2 = await this.authService.getOutstandingDownPayment(bookCode);
      this.documentdp=response2.result.dp;
    } catch (error) {
      console.error('Error fetching data', error);
    
    }
    this.showSpinner=false
   }
   async getUnitCoceUnitno(bookCode:any){
    this.showSpinner=true
   
   
    try {
     
     
      const response = await this.authService.getUnitcodeUnitno(bookCode);
      this.unitcodeunitno=response.result
      this.documentdp=response.result.dp;
      if(!response.success){
       Swal.fire({
       title: 'Error',
       text: 'Internal Bad Request',
       icon: 'error',
       timer: 3000, // Menutup pesan setelah 3 detik
     
       showConfirmButton: true, // Tidak menampilkan tombol OK
           timerProgressBar: true 
     });
 }
   
      console.log(this.documents)
      
    } catch (error) {
      console.error('Error fetching data', error);
    
    }
    this.showSpinner=false
   }
  async getListDocucmentKPR(_psCode: any, _bookingHeaderID: any,_ppNO:any) {

    
    this.showSpinner=true
   
   
     try {
      
      
       const response = await this.authService.getListDocumentKPR(_psCode, _bookingHeaderID, _ppNO);
       this.document=response.result;
       if(!response.success){
        Swal.fire({
        title: 'Error',
        text: 'Internal Bad Request',
        icon: 'error',
        timer: 3000, // Menutup pesan setelah 3 detik
      
        showConfirmButton: true, // Tidak menampilkan tombol OK
            timerProgressBar: true 
      });
  }
    
       console.log(this.documents)
       
     } catch (error) {
       console.error('Error fetching data', error);
     
     }
     this.showSpinner=false
     //this.showSpinner = false;
   }
   async getListView(_bookingHeaderID: any, _psCode: any,_ppNO:any) {

    this.showSpinner=true
   
   
     try {
      
       const response = await this.authService.getHistoryViewDocument(_bookingHeaderID,_psCode, _ppNO);
       if(!response.success){
        Swal.fire({
        title: 'Error',
        text: 'Internal Bad Request',
        icon: 'error',
        timer: 3000, // Menutup pesan setelah 3 detik
      
        showConfirmButton: true, // Tidak menampilkan tombol OK
            timerProgressBar: true 
      });
  }
       console.log(response,'pppppp')
       this.documento=response.result;
   
    
       console.log(this.documents)
       // this.dataSource = response.result.items;
       // this.totalItems = response.result.totalCount;
     } catch (error) {
       console.error('Error fetching data', error);
      
     }
     this.showSpinner=false
     //this.showSpinner = false;
   }
   getDocumentKPRFile(_psCode:any,_bookingHeaderID:any,_documentType:any, _priorityPassId:any) {
    console.log(_priorityPassId)
    this.createHistoryViewDocument()
    localStorage.setItem('psCode',_psCode)
      localStorage.setItem('bookingHeaderID',_bookingHeaderID)
      localStorage.setItem('documentType',_documentType)
      localStorage.setItem('priorityPassID',_priorityPassId)
    window.open('detail-document/' + _psCode + '/' + _bookingHeaderID + '/' + _documentType+ '/' +_priorityPassId,'_blank');
}


 

async uploadDocumentKPR(_document:any, _documentFile:any) {
  
   //this.showSpinner = true;
 console.log(_document)
  this.isGlobalLoading = true;
  this.input.bookingHeaderID=this.data.bookingId;
  this.input.documentType = _document.documentType;
  this.input.documentFile = _documentFile;
  this.input.priorityPassId=_document.priorityPassId == null ? undefined:_document.priorityPassId;
  this.input.psCode= _document.psCode;
  this.input.userId= localStorage.getItem('UserId');

 
 
 
   try {
    this.showSpinner = true;
     const response = await this.authService.uploadDocumentKPR(this.input);
    
     if(!response.success){
      Swal.fire({
      title: 'Error',
      text: 'Internal Bad Request',
      icon: 'error',
      timer: 3000, // Menutup pesan setelah 3 detik
    
      showConfirmButton: true, // Tidak menampilkan tombol OK
          timerProgressBar: true 
    });
}
     this.ppno=localStorage.getItem("ppno");
     console.log(response)
    this.getListDocucmentKPR(this.data.psCode,this.data.bookingId,this.ppno);

    
   } catch (error) {
     console.error('Error fetching data', error);
  
   }
   this.showSpinner = false;
   //this.showSpinner = false;
 }
 created(event: Quill) {}
 async getListCommision() {

   
   
   
  try {
  
    const response = await this.authService.getDetailTaskListMyCommisionBF(this.data._bookingHeaderID, this.data._bookCode, this.data._memberCode, this.data._memberCodeLogin);
    if(!response.success){
      Swal.fire({
      title: 'Error',
      text: 'Internal Bad Request',
      icon: 'error',
      timer: 3000, 
    
      showConfirmButton: true, 
          timerProgressBar: true 
    });
}
    this.listComission=response.result;
    this.listComissiondataTotal=response.result.dataTotal;
    this.listComissiondataHeader=response.result.dataHeader;
    this.listComissiondataBody=response.result.dataBody;
    this.listComissiondataBookingFee=response.result.dataBookingFee;
    console.log(this.listComissiondataTotal);
    console.log(this.listComissiondataHeader)

    console.log(this.documents)
    // this.dataSource = response.result.items;
    // this.totalItems = response.result.totalCount;
  } catch (error) {
    console.error('Error fetching data', error);
   
  }

  //this.showSpinner = false;
}

async SaveDocuments(fileName:any, documentPicType:any,fileExtension:any,base64String:any) {

    const input:any={
      documentType: fileExtension,
      documentPicType: documentPicType,
      fileName: fileName,
      documentFile: base64String
    }
  this.showSpinner=true
 
 
   try {
    
    
     const response = await this.authService.SaveDocument(input);
     this.document=response.result;
     if(!response.success){
      Swal.fire({
      title: 'Error',
      text: 'Internal Bad Request',
      icon: 'error',
      timer: 3000, // Menutup pesan setelah 3 detik
    
      showConfirmButton: true, // Tidak menampilkan tombol OK
          timerProgressBar: true 
    });
}
  
     console.log(this.documents)
     
   } catch (error) {
     console.error('Error fetching data', error);
   
   }
   this.showSpinner=false
   //this.showSpinner = false;
 }
uploadFile(event: any, document: any, index: any): void {
  
  
  console.log(event);
  console.log(document);
  if (event.target.files[0].size > 5048576) {
  Swal.fire({
      title: 'Error',
      text: 'Maximum file 5 MB',
      icon: 'error',
      timer: 3000, // Menutup pesan setelah 3 detik
    
      showConfirmButton: true, // Tidak menampilkan tombol OK
          timerProgressBar: true 
    });
    // abp.notify.warn('Only upload file with extension .jpg/.jpeg/.pdf with exceed file size 5 MB.', 'File size is too large');
    setTimeout(() => {
      const fileInput = document.getElementById('file-' + index) as HTMLInputElement;
      if (fileInput) {
        fileInput.value = '';
      }
    }, 0);
  } else {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.onload = () => {
      if (reader.result) {
        if (file.type.toLowerCase().includes('image')) {
          this.input.documentPicType = 'image';
        }
        if (file.type.toLowerCase().includes('pdf')) {
          this.input.documentPicType = 'pdf';
        }
        const base64String = reader.result.toString().replace('data:' + file.type + ';base64,', '');
        console.log(base64String)
        if (base64String !== undefined) {
        
          this.uploadDocumentKPR(document, base64String);
        
        }
      } else {
        console.error('Error: FileReader result is null.');
      }
    };
    reader.readAsDataURL(file);
    reader.onerror = (error) => {
      console.error('Error occurred while reading file:', error);
    };
  }
  
}
onFileSelecteds(event: any,fileInput: HTMLInputElement) {
  this.areFieldsFilled = false;
  const file: File = event.target.files[0];
  const fileName: string = file.name;
  const fileExtension: string = this.getFileType(fileName);
  if (fileExtension.indexOf('jpg') === -1 && fileExtension.indexOf('png') === -1 && fileExtension.indexOf('pdf') === -1) {

    Swal.fire({
      icon: 'warning',
      title: 'File must .jpg,.png,.pdf',
      
      showCancelButton: true,
      confirmButtonColor: '#007bff',
      cancelButtonColor: '#aaa',
      reverseButtons: true,
      confirmButtonText: 'OK',
      cancelButtonText: 'Cancel'
    }).then((result) => {
      if (result.isConfirmed) {
        // Tindakan jika tombol OK diklik
        console.log('Ok');
        fileInput.value = '';
      } else {
        // Tindakan jika tombol Batal diklik
       
      }
      
    });
  }else{
    console.log('File extension:', fileExtension);
  console.log(event);
  console.log(document);
  if (event.target.files[0].size > 5048576) {
  Swal.fire({
      title: 'Error',
      text: 'Maximum file 5 MB',
      icon: 'error',
      timer: 3000, // Menutup pesan setelah 3 detik
    
      showConfirmButton: true, // Tidak menampilkan tombol OK
          timerProgressBar: true 
    });
    // abp.notify.warn('Only upload file with extension .jpg/.jpeg/.pdf with exceed file size 5 MB.', 'File size is too large');
    // setTimeout(() => {
    //   const fileInput = document.getElementById('file-' + index) as HTMLInputElement;
    //   if (fileInput) {
    //     fileInput.value = '';
    //   }
    // }, 0);
  } else {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.onload = () => {
      if (reader.result) {
        if (file.type.toLowerCase().includes('image')) {
          this.documentPicType = 'image';
        }
        if (file.type.toLowerCase().includes('pdf')) {
          this.documentPicType = 'pdf';
        }
        const base64String = reader.result.toString().replace('data:' + file.type + ';base64,', '');
        console.log(base64String)
        if (base64String !== undefined) {
        
          this.SaveDocuments(fileName, this.documentPicType,fileExtension,base64String);
        
        }
      } else {
        console.error('Error: FileReader result is null.');
      }
    };
    reader.readAsDataURL(file);
    reader.onerror = (error) => {
      console.error('Error occurred while reading file:', error);
    };
  }
  }
  // if(fileExtension!="jpg"||"png"||"pdf"){
  //   Swal.fire({
  //     icon: 'warning',
  //     title: 'File must .jpg,.png,.pdf',
      
  //     showCancelButton: true,
  //     confirmButtonColor: '#007bff',
  //     cancelButtonColor: '#aaa',
  //     confirmButtonText: 'OK',
  //     cancelButtonText: 'Cancel'
  //   }).then((result) => {
  //     if (result.isConfirmed) {
  //       // Tindakan jika tombol OK diklik
  //       console.log('Ok');
        
  //     } else {
  //       // Tindakan jika tombol Batal diklik
       
  //     }
      
  //   });
  // }
  
}

getFileType(fileName: string): string {
  ;
  const dotIndex = fileName.lastIndexOf('.');
  if (dotIndex === -1) {
    return ''; // Tidak ada ekstensi
  }
  const hasil= fileName.substr(dotIndex + 1).toLowerCase();
  

  return hasil
}
async save(){
  
  Swal.fire({
    title: "Are you sure to Print OL & Akad?",
    text: "If agreed, the system will print the OL & Akad Document.",
  
    icon: 'info',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    reverseButtons: true,
    confirmButtonText: 'Yes',
    cancelButtonText: 'Cancel'
  }).then(async (result) => {
    if (result.isConfirmed) {
      console.log(this.dataprintol.tenor,'assssssssssssssssss')
      console.log(this.dataprintol.sukuBunga,'sukuuuuuuuuuuuuuuu')
      console.log(this.dataprintol.tahun,'sukuuuuuuuuuuuuuuu')
      // const input:any={
      //   bookCode:this.data.bookCode,
      //   plafond:this.dataprintolrupiah.plafond,
        
      //   tenor: this.dataprintol.tenor,
      //   sukuBunga: this.dataprintol.sukuBunga,
      //   sukuBungaTahun: this.dataprintol.tahun,
      
      // }
      const data =this.dataprintolrupiah.plafond;
      var stringTanpaTitik = data.replace(/\./g, ''); // Menghapus semua titik
      const sukuBungaString = this.dataprintol.sukuBunga.toString();
      const tanor =  this.dataprintol.tenor.toString();
      const tahun =  this.dataprintol.tahun.toString();
      const input = JSON.stringify({
        bookCode: this.data.bookCode,
        plafond: stringTanpaTitik,
        tenor: tanor,
        sukuBunga: sukuBungaString,
        sukuBungaTahun: tahun
    });
      // let mount = this.data.amount?this.data.amount:this.documentdp;
      const response = await this.authService.postPrintOLN(input);
      if(response.success==true){
        window.open(response.result.filePDF, '_blank');
        Swal.fire({
          icon: 'success',
          title: 'Success!',
          text: 'Data has been print successfully.'
        });
        this.dialogRef.close({ saved: true });
      }if(response.error!=null){
        Swal.fire({
          title: `${response.result.status}`,
          text: `${response.result.errorMessage}`,
          icon: 'error',
          timer: 3000, // Menutup pesan setelah 3 detik
        
          showConfirmButton: true, // Tidak menampilkan tombol OK
              timerProgressBar: true 
        });
    
      }
    //  if(response.result.redirect_url){
    //   window.open(response.result.redirect_url, '_blank', 'menubar=yes,location=yes,resizable=yes,scrollbars=yes,status=yes');
    //   // window.open(response.result.redirect_url, 'nama_jendela');
    //  }
     
   }
  })
  
}

async getListPaymentTypeGateway(projectID:any) {

    
  this.showSpinner=true
 
 
   try {
    
    
     const response = await this.authService.getListPaymentTypeGateway(projectID);
     this.document=response.result;
     if(!response.success){
      Swal.fire({
      title: 'Error',
      text: 'Internal Bad Request',
      icon: 'error',
      timer: 3000, // Menutup pesan setelah 3 detik
    
      showConfirmButton: true, // Tidak menampilkan tombol OK
          timerProgressBar: true 
    });
}
  //    for (const logo of this.document ) {
  //      this.documents=logo;
  //  }
     console.log(this.documents)
     // this.dataSource = response.result.items;
     // this.totalItems = response.result.totalCount;
   } catch (error) {
     console.error('Error fetching data', error);
    //  Swal.fire({
    //   title: 'Error',
    //   text: 'Internal Bad Request',
    //   icon: 'error',
    //   timer: 3000, // Menutup pesan setelah 3 detik
    
    //   showConfirmButton: true, // Tidak menampilkan tombol OK
    //       timerProgressBar: true 
    // });
   }
   this.showSpinner=false
   //this.showSpinner = false;
 }
}