import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModaldialogcomponentComponent } from './modaldialogcomponent.component';

describe('ModaldialogcomponentComponent', () => {
  let component: ModaldialogcomponentComponent;
  let fixture: ComponentFixture<ModaldialogcomponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ModaldialogcomponentComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ModaldialogcomponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
