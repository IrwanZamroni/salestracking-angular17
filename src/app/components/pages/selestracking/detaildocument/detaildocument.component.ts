import { Component, OnInit, ViewChild, Injector, ElementRef, Output, EventEmitter } from '@angular/core';
import { AuthService } from '../../../../shared/auth.service';
import { ActivatedRoute, Router } from "@angular/router";
import { OnDestroy } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { firstValueFrom, Subscription } from 'rxjs';
interface RouteParams {
  psCode: any;
  bookingHeaderID: any;
  documentType: any;
  priorityPassID: any;
}
@Component({
  selector: 'app-detaildocument',
 
 


  template: ``,



})
export class DetaildocumentComponent {
  params: RouteParams;
  base64String:any;
  fileType:any;
  href:any;

  constructor(
    injector: Injector,
    private _activeroute: ActivatedRoute,
    private _salesTrackingService: AuthService,public router: Router,
) {
  this._activeroute.params.subscribe((params: any) => {
    if (params != undefined) {
      const routeParams = params as RouteParams; // Casting params to RouteParams
      
      this.params = routeParams;
      this.getDocumentKPRFile(routeParams.psCode, routeParams.bookingHeaderID, routeParams.documentType, routeParams.priorityPassID);
      localStorage.setItem('psCode',routeParams.psCode)
      localStorage.setItem('bookingHeaderID',routeParams.bookingHeaderID)
      localStorage.setItem('documentType',routeParams.documentType)
      localStorage.setItem('priorityPassID',routeParams.priorityPassID)
    }
  });
  this.href = this.router.url;
  const parts = this.href.split('/')[1]; 
  localStorage.setItem('-', parts);
}
ngOnInit() {

 
       // Casting params to RouteParams
      localStorage.setItem('psCode', this.params.psCode)
      localStorage.setItem('bookingHeaderID', this.params.bookingHeaderID)
      localStorage.setItem('documentType', this.params.documentType)
      localStorage.setItem('priorityPassID', this.params.priorityPassID)
   
   
   
  
}

async getDocumentKPRFile(_psCode:any, _bookingHeaderID:any, _documentType:any, _priorityPassID:any) {

  const headers = new HttpHeaders({
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": "*",
  });

  // const params = new HttpParams()
  //   .set('psCode', psCode)
  //   .set('bookingHeaderID', bookingHeaderID.toString())
  //   .set('documentType', documentType)
  //   .set('priorityPassId', priorityPassId.toString());

  try {

    const response = await this._salesTrackingService.getDocumentKPRFile(_psCode, _bookingHeaderID, _documentType,_priorityPassID);
    if (response.result.documentType.toLowerCase().includes("image")) {
      this.base64String = ("data:image/png;base64," + response.result.documentFile);

      let obj = document.createElement('img');
      obj.src = this.base64String;
      obj.title = this.params.documentType;
      obj.alt = this.params.documentType;
      obj.style.width = 'fit-content';
      obj.style.height = '100vh';
      obj.style.position = 'absolute';
      obj.style.marginTop='-46rem'
      document.body.appendChild(obj);
  }
  if (response.result.documentType.toLowerCase().includes("pdf")) {
      this.base64String = ("data:application/pdf;base64," + response.result.documentFile);

      // Convert base64 to binary
      const binaryData = atob(response.result.documentFile);

      // Create a Uint8Array from the binary data
      const arrayBuffer = new Uint8Array(binaryData.length);
      for (let i = 0; i < binaryData.length; i++) {
          arrayBuffer[i] = binaryData.charCodeAt(i);
      }

      // Create a Blob from the Uint8Array
      const blob = new Blob([arrayBuffer], { type: 'application/pdf' });

      // Create an object element and set its properties
      let obj = document.createElement('object');
      obj.style.width = '100%';
      obj.style.height = '100vh';
      obj.style.marginTop='-46rem'
      obj.type = 'application/pdf';

      // Set the object element's data to the Blob
      obj.data = URL.createObjectURL(blob);

      // Append the object element to the body
      document.body.appendChild(obj);
  }
  this.fileType = response.result.documentType;
  //     this.base64String = ("data:application/pdf;base64," + result.documentFile);

  //     let obj = document.createElement('object');
  //     obj.style.width = '100%';
  //     obj.style.height = '100%';
  //     obj.type = 'application/pdf';
  //     obj.data = this.base64String;
  //     document.body.appendChild(obj);
  // }
  // this.fileType = result.documentType;
}catch (err) {
  console.error(err);
}
}
}
