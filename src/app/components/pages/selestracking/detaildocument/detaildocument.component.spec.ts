import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetaildocumentComponent } from './detaildocument.component';

describe('DetaildocumentComponent', () => {
  let component: DetaildocumentComponent;
  let fixture: ComponentFixture<DetaildocumentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DetaildocumentComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DetaildocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
