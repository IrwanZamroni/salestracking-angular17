import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelestrackingComponent } from './selestracking.component';

describe('SelestrackingComponent', () => {
  let component: SelestrackingComponent;
  let fixture: ComponentFixture<SelestrackingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SelestrackingComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(SelestrackingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
