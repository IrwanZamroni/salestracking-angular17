import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { FormBuilder, FormGroup } from '@angular/forms';
import { FormControl, Validators } from '@angular/forms';
import {  Inject } from '@angular/core';
import { AuthService } from '../../../../shared/auth.service';
import {  inject, ElementRef ,HostListener,ViewChild,AfterViewInit,Input} from '@angular/core';
import { SalestrackingService } from '../../../../shared/salestracking.service';
import {  MAT_DIALOG_DATA } from '@angular/material/dialog';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-example-dialog',
  templateUrl: './example-dialog.component.html',

  styleUrls: ['./example-dialog.component.scss']
})
export class ExampleDialogComponent {
  newemail: string = '';
  newEmailForm: FormGroup;
  form: FormGroup;
  datalocalstorage:any;
  documentPicType:any;
  document:any;
  documents: any;
  areFieldsFilled:boolean;
  areFieldsFilledd:boolean=true;
  signaturdate:any;
  public authService = inject(AuthService);
  showSpinner: boolean = false;
  public salestrackingService = inject(SalestrackingService);
  constructor(public dialogRef: MatDialogRef<ExampleDialogComponent>,@Inject(MAT_DIALOG_DATA) public data: any) {
    this.datalocalstorage=localStorage.getItem('UserId');
    this.newEmailForm = new FormGroup({
      email: new FormControl(null, Validators.required),
    
    });
    this.form = new FormGroup({
      file: new FormControl(null, Validators.required),
      date: new FormControl(null, Validators.required)
    });
  
  }
  onDateChange(dateValue: any) {
    
    this.areFieldsFilledd = false;
    const dates= dateValue.value
    const date = new Date(dates);
    const isoDateString = date.toISOString();
    this.signaturdate=isoDateString;
    // Lakukan sesuatu dengan nilai tanggal di sini
  }
  async onSubmit() {
    this.showSpinner=true
 
    // if (this.form.valid) {
      const input:any={
       
        trBookingDocID: this.data.bookingDocumentID,
        docNo: this.data.docNo,
        signedDocumentFile:  this.document.fileName,
        signatureDate: this.signaturdate
      }
       const response = await this.authService.UploadSignedDocument(input)
       this.showSpinner=true
    // } else {
    //   // Tampilkan pesan kesalahan atau lakukan tindakan yang sesuai
    if(response.result.errorMessage!=null){
      Swal.fire({
      title: `${response.result.status}`,
      text: `${response.result.errorMessage}`,
      icon: 'error',
      timer: 3000, // Menutup pesan setelah 3 detik
    
      showConfirmButton: true, // Tidak menampilkan tombol OK
          timerProgressBar: true 
    });
      // console.log("Form tidak valid:",response.result.errorMessage);
    //   // Dapat mengakses status dirty dan touched dari form control
    this.showSpinner=false
    }else{
      if(this.data.docCode=='CL'){
        Swal.fire(
          'Upload Confirmation Letter Successfully',
          '',
          'success'
        );
        this.dialogRef.close({ saved: true });
        this.showSpinner=false
      }
    
      if(this.data.docCode=='PPPU'){
        Swal.fire(
          'Upload P3U Successfully',
          '',
          'success'
        );
        this.dialogRef.close({ saved: true });
        this.showSpinner=false
      }
      if(this.data.docCode=='OLN2'){
        Swal.fire(
          'Upload OL & Akad Successfully',
          '',
          'success'
        );
        this.dialogRef.close({ saved: true });
        this.showSpinner=false
      }
    }
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  get fileControl() {
    return this.form.get('file');
  }
  get dateControl() {
    return this.form.get('date');
  }
  getFileType(fileName: string): string {
    ;
    const dotIndex = fileName.lastIndexOf('.');
    if (dotIndex === -1) {
      return ''; // Tidak ada ekstensi
    }
    const hasil= fileName.substr(dotIndex + 1).toLowerCase();
    
  
    return hasil
  }
  async SaveDocuments(fileName:any, documentPicType:any,fileExtension:any,base64String:any) {

    const input:any={
      documentType: fileExtension,
      documentPicType: documentPicType,
      fileName: fileName,
      documentFile: base64String
    }
  this.showSpinner=true
 
 
   try {
    
    
     const response = await this.authService.SaveDocument(input);
     this.document=response.result;
     if(!response.success){
      Swal.fire({
      title: 'Error',
      text: 'Internal Bad Request',
      icon: 'error',
      timer: 3000, // Menutup pesan setelah 3 detik
    
      showConfirmButton: true, // Tidak menampilkan tombol OK
          timerProgressBar: true 
    });
}
  
     console.log(this.documents)
     
   } catch (error) {
     console.error('Error fetching data', error);
   
   }
   this.showSpinner=false
   //this.showSpinner = false;
 }
  onFileSelecteds(event: any,fileInput: HTMLInputElement) {
    this.areFieldsFilled = false;
    const file: File = event.target.files[0];
    const fileName: string = file.name;
    const fileExtension: string = this.getFileType(fileName);
    if (fileExtension.indexOf('jpg') === -1 && fileExtension.indexOf('png') === -1 && fileExtension.indexOf('pdf') === -1) {
  
      Swal.fire({
        icon: 'warning',
        title: 'File must .jpg,.png,.pdf',
        
        showCancelButton: true,
        confirmButtonColor: '#007bff',
        cancelButtonColor: '#aaa',
        reverseButtons: true,
        confirmButtonText: 'OK',
        cancelButtonText: 'Cancel'
      }).then((result) => {
        if (result.isConfirmed) {
          // Tindakan jika tombol OK diklik
          console.log('Ok');
          fileInput.value = '';
        } else {
          // Tindakan jika tombol Batal diklik
         
        }
        
      });
    }else{
      console.log('File extension:', fileExtension);
    console.log(event);
    console.log(document);
    if (event.target.files[0].size > 5048576) {
    Swal.fire({
        title: 'Error',
        text: 'Maximum file 5 MB',
        icon: 'error',
        timer: 3000, // Menutup pesan setelah 3 detik
      
        showConfirmButton: true, // Tidak menampilkan tombol OK
            timerProgressBar: true 
      });
      // abp.notify.warn('Only upload file with extension .jpg/.jpeg/.pdf with exceed file size 5 MB.', 'File size is too large');
      // setTimeout(() => {
      //   const fileInput = document.getElementById('file-' + index) as HTMLInputElement;
      //   if (fileInput) {
      //     fileInput.value = '';
      //   }
      // }, 0);
    } else {
      const file = event.target.files[0];
      const reader = new FileReader();
      reader.onload = () => {
        if (reader.result) {
          if (file.type.toLowerCase().includes('image')) {
            this.documentPicType = 'image';
          }
          if (file.type.toLowerCase().includes('pdf')) {
            this.documentPicType = 'pdf';
          }
          const base64String = reader.result.toString().replace('data:' + file.type + ';base64,', '');
          console.log(base64String)
          if (base64String !== undefined) {
          
            this.SaveDocuments(fileName, this.documentPicType,fileExtension,base64String);
          
          }
        } else {
          console.error('Error: FileReader result is null.');
        }
      };
      reader.readAsDataURL(file);
      reader.onerror = (error) => {
        console.error('Error occurred while reading file:', error);
      };
    }
    }
    // if(fileExtension!="jpg"||"png"||"pdf"){
    //   Swal.fire({
    //     icon: 'warning',
    //     title: 'File must .jpg,.png,.pdf',
        
    //     showCancelButton: true,
    //     confirmButtonColor: '#007bff',
    //     cancelButtonColor: '#aaa',
    //     confirmButtonText: 'OK',
    //     cancelButtonText: 'Cancel'
    //   }).then((result) => {
    //     if (result.isConfirmed) {
    //       // Tindakan jika tombol OK diklik
    //       console.log('Ok');
          
    //     } else {
    //       // Tindakan jika tombol Batal diklik
         
    //     }
        
    //   });
    // }
    
  }
  async closeDialog() {
    // debugger
    // const input2:any={
       
    //   trBookingDocID: this.data?.bookingDocumentID,
    //   docNo: this.data?.docNo,
    //   signedDocumentFile:  this.document?.fileName,
    //   signatureDate: this.signaturdate
    // }
    // const response = await this.authService.UploadSignedDocument(input2)
    // if(this.data.statecekCl==true){
    //   this.dialogRef.close();
    // }
    
    this.dialogRef.close();
    
  
  }
  // postData(): void {
  
  //   this.showSpinner=true;
  //    const data = { 
  //      psCode: this.data.psCode,
  //      oldEmail: this.data.email,
  //      newEmail: this.newemail,
  //      creationUserId: this.datalocalstorage
  //    };
  //    this.salestrackingService.postDataEmailSalestracking(data).subscribe({
  //      next: (response) => {
  //        this.dialogRef.close({ saved: true });
  //        console.log('Post successful:', response);
  //        Swal.fire(
  //          'Success Change Email',
  //          '',
  //          'success'
  //        );
  //        this.showSpinner=false;
  //      },
  //      error: (error) => {
  //        console.error('Error posting data:', error);
  //        Swal.fire(
  //          'Error Change Email',
  //          '',
  //          'error'
  //        );
  //        this.showSpinner=false;
  //      }
  //    });
 
  //  }
  async postData(): Promise<void> {
    this.showSpinner = true;
    const converttoint=parseInt(this.datalocalstorage, 10);  
    const data = {
      psCode: this.data.psCode,
      oldEmail: this.data.email,
      newEmail: this.newemail,
      creationUserId: converttoint
    };

    try {
      const response = await this.salestrackingService.postDataEmailSalestracking(data);
      this.dialogRef.close({ saved: true });
      console.log('Post successful:', response);
      Swal.fire(
        'Success Change Email',
        '',
        'success'
      );
    } catch (error) {
      console.error('Error posting data:', error);
      Swal.fire(
        'Error Change Email',
        '',
        'error'
      );
    } finally {
      this.showSpinner = false;
    }
  }
  modalSave(){
   
this.postData()
  }
}
