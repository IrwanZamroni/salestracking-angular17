import { Directive } from '@angular/core';

@Directive({
  selector: '[appBackgroundColorOnClick]',
  standalone: true
})
export class BackgroundColorOnClickDirective {

  constructor() { }

}
